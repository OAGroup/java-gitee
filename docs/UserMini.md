
# UserMini

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**login** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**avatarUrl** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
**htmlUrl** | **String** |  |  [optional]



