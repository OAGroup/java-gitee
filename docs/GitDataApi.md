# GitDataApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getV5ReposOwnerRepoGitBlobsSha**](GitDataApi.md#getV5ReposOwnerRepoGitBlobsSha) | **GET** /v5/repos/{owner}/{repo}/git/blobs/{sha} | 获取文件Blob
[**getV5ReposOwnerRepoGitTreesSha**](GitDataApi.md#getV5ReposOwnerRepoGitTreesSha) | **GET** /v5/repos/{owner}/{repo}/git/trees/{sha} | 获取目录Tree


<a name="getV5ReposOwnerRepoGitBlobsSha"></a>
# **getV5ReposOwnerRepoGitBlobsSha**
> Blob getV5ReposOwnerRepoGitBlobsSha(owner, repo, sha, accessToken)

获取文件Blob

获取文件Blob

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.GitDataApi;


GitDataApi apiInstance = new GitDataApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String sha = "sha_example"; // String | 文件的 Blob SHA，可通过 [获取仓库具体路径下的内容] API 获取
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    Blob result = apiInstance.getV5ReposOwnerRepoGitBlobsSha(owner, repo, sha, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GitDataApi#getV5ReposOwnerRepoGitBlobsSha");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **sha** | **String**| 文件的 Blob SHA，可通过 [获取仓库具体路径下的内容] API 获取 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Blob**](Blob.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoGitTreesSha"></a>
# **getV5ReposOwnerRepoGitTreesSha**
> Tree getV5ReposOwnerRepoGitTreesSha(owner, repo, sha, accessToken, recursive)

获取目录Tree

获取目录Tree

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.GitDataApi;


GitDataApi apiInstance = new GitDataApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String sha = "sha_example"; // String | 可以是分支名(如master)、Commit或者目录Tree的SHA值
String accessToken = "accessToken_example"; // String | 用户授权码
Integer recursive = 56; // Integer | 赋值为1递归获取目录
try {
    Tree result = apiInstance.getV5ReposOwnerRepoGitTreesSha(owner, repo, sha, accessToken, recursive);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GitDataApi#getV5ReposOwnerRepoGitTreesSha");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **sha** | **String**| 可以是分支名(如master)、Commit或者目录Tree的SHA值 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **recursive** | **Integer**| 赋值为1递归获取目录 | [optional]

### Return type

[**Tree**](Tree.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

