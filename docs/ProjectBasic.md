
# ProjectBasic

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**fullName** | **String** |  |  [optional]
**humanName** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
**namespace** | **Object** |  |  [optional]
**path** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**owner** | [**UserBasic**](UserBasic.md) |  |  [optional]
**description** | **String** |  |  [optional]
**_private** | **Boolean** |  |  [optional]
**_public** | **Boolean** |  |  [optional]
**internal** | **Boolean** |  |  [optional]
**fork** | **Boolean** |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**sshUrl** | **String** |  |  [optional]



