# MiscellaneousApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getV5Emojis**](MiscellaneousApi.md#getV5Emojis) | **GET** /v5/emojis | 列出可使用的 Emoji
[**getV5GitignoreTemplates**](MiscellaneousApi.md#getV5GitignoreTemplates) | **GET** /v5/gitignore/templates | 列出可使用的 .gitignore 模板
[**getV5GitignoreTemplatesName**](MiscellaneousApi.md#getV5GitignoreTemplatesName) | **GET** /v5/gitignore/templates/{name} | 获取一个 .gitignore 模板
[**getV5GitignoreTemplatesNameRaw**](MiscellaneousApi.md#getV5GitignoreTemplatesNameRaw) | **GET** /v5/gitignore/templates/{name}/raw | 获取一个 .gitignore 模板原始文件
[**getV5Licenses**](MiscellaneousApi.md#getV5Licenses) | **GET** /v5/licenses | 列出可使用的开源许可协议
[**getV5LicensesLicense**](MiscellaneousApi.md#getV5LicensesLicense) | **GET** /v5/licenses/{license} | 获取一个开源许可协议
[**getV5LicensesLicenseRaw**](MiscellaneousApi.md#getV5LicensesLicenseRaw) | **GET** /v5/licenses/{license}/raw | 获取一个开源许可协议原始文件
[**getV5ReposOwnerRepoLicense**](MiscellaneousApi.md#getV5ReposOwnerRepoLicense) | **GET** /v5/repos/{owner}/{repo}/license | 获取一个仓库使用的开源许可协议
[**postV5Markdown**](MiscellaneousApi.md#postV5Markdown) | **POST** /v5/markdown | 渲染 Markdown 文本


<a name="getV5Emojis"></a>
# **getV5Emojis**
> getV5Emojis(accessToken)

列出可使用的 Emoji

列出可使用的 Emoji

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.MiscellaneousApi;


MiscellaneousApi apiInstance = new MiscellaneousApi();
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.getV5Emojis(accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling MiscellaneousApi#getV5Emojis");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5GitignoreTemplates"></a>
# **getV5GitignoreTemplates**
> getV5GitignoreTemplates(accessToken)

列出可使用的 .gitignore 模板

列出可使用的 .gitignore 模板

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.MiscellaneousApi;


MiscellaneousApi apiInstance = new MiscellaneousApi();
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.getV5GitignoreTemplates(accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling MiscellaneousApi#getV5GitignoreTemplates");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5GitignoreTemplatesName"></a>
# **getV5GitignoreTemplatesName**
> getV5GitignoreTemplatesName(name, accessToken)

获取一个 .gitignore 模板

获取一个 .gitignore 模板

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.MiscellaneousApi;


MiscellaneousApi apiInstance = new MiscellaneousApi();
String name = "name_example"; // String | .gitignore 模板名
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.getV5GitignoreTemplatesName(name, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling MiscellaneousApi#getV5GitignoreTemplatesName");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| .gitignore 模板名 | [enum: Actionscript, Ada, Agda, Android, AppEngine, AppceleratorTitanium, ArchLinuxPackages, Autotools, C, C++, CFWheels, CMake, CUDA, CakePHP, ChefCookbook, Clojure, CodeIgniter, CommonLisp, Composer, Concrete5, Coq, CraftCMS, D, DM, Dart, Delphi, Drupal, EPiServer, Eagle, Elisp, Elixir, Elm, Erlang, ExpressionEngine, ExtJs, Fancy, Finale, Flutter, ForceDotCom, Fortran, FuelPHP, GWT, Gcov, GitBook, Anjuta, Ansible, Archives, Backup, Bazaar, BricxCC, CVS, Calabash, Cloud9, CodeKit, DartEditor, Diff, Dreamweaver, Dropbox, Eclipse, EiffelStudio, Emacs, Ensime, Espresso, FlexBuilder, GPG, Images, JDeveloper, JEnv, JetBrains, KDevelop4, Kate, Lazarus, LibreOffice, Linux, LyX, MATLAB, Mercurial, MicrosoftOffice, ModelSim, Momentics, MonoDevelop, NetBeans, Ninja, NotepadPP, Octave, Otto, PSoCCreator, Patch, PuTTY, Redcar, Redis, SBT, SVN, SlickEdit, Stata, SublimeText, SynopsysVCS, Tags, TextMate, TortoiseGit, Vagrant, Vim, VirtualEnv, Virtuoso, VisualStudioCode, WebMethods, Windows, Xcode, XilinxISE, macOS, Go, Godot, Gradle, Grails, Haskell, IGORPro, Idris, JBoss, Java, Jekyll, Joomla, Julia, KiCad, Kohana, Kotlin, LabVIEW, Laravel, Leiningen, LemonStand, Lilypond, Lithium, Lua, Magento, Maven, Mercury, MetaProgrammingSystem, MiniProgram, Nanoc, Nim, Node, OCaml, Objective-C, Opa, OpenCart, OracleForms, Packer, Perl, Perl6, Phalcon, PlayFramework, Plone, Prestashop, Processing, PureScript, Python, Qooxdoo, Qt, R, ROS, Rails, RhodesRhomobile, Ruby, Rust, SCons, Sass, Scala, Scheme, Scrivener, Sdcc, SeamGen, SketchUp, Smalltalk, Stella, SugarCRM, Swift, Symfony, SymphonyCMS, TeX, Terraform, Textpattern, TurboGears2, Typo3, Umbraco, Unity, UnrealEngine, VVVV, VisualStudio, Waf, WordPress, Xojo, Yeoman, Yii, ZendFramework, Zephir]
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5GitignoreTemplatesNameRaw"></a>
# **getV5GitignoreTemplatesNameRaw**
> getV5GitignoreTemplatesNameRaw(name, accessToken)

获取一个 .gitignore 模板原始文件

获取一个 .gitignore 模板原始文件

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.MiscellaneousApi;


MiscellaneousApi apiInstance = new MiscellaneousApi();
String name = "name_example"; // String | .gitignore 模板名
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.getV5GitignoreTemplatesNameRaw(name, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling MiscellaneousApi#getV5GitignoreTemplatesNameRaw");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| .gitignore 模板名 | [enum: Actionscript, Ada, Agda, Android, AppEngine, AppceleratorTitanium, ArchLinuxPackages, Autotools, C, C++, CFWheels, CMake, CUDA, CakePHP, ChefCookbook, Clojure, CodeIgniter, CommonLisp, Composer, Concrete5, Coq, CraftCMS, D, DM, Dart, Delphi, Drupal, EPiServer, Eagle, Elisp, Elixir, Elm, Erlang, ExpressionEngine, ExtJs, Fancy, Finale, Flutter, ForceDotCom, Fortran, FuelPHP, GWT, Gcov, GitBook, Anjuta, Ansible, Archives, Backup, Bazaar, BricxCC, CVS, Calabash, Cloud9, CodeKit, DartEditor, Diff, Dreamweaver, Dropbox, Eclipse, EiffelStudio, Emacs, Ensime, Espresso, FlexBuilder, GPG, Images, JDeveloper, JEnv, JetBrains, KDevelop4, Kate, Lazarus, LibreOffice, Linux, LyX, MATLAB, Mercurial, MicrosoftOffice, ModelSim, Momentics, MonoDevelop, NetBeans, Ninja, NotepadPP, Octave, Otto, PSoCCreator, Patch, PuTTY, Redcar, Redis, SBT, SVN, SlickEdit, Stata, SublimeText, SynopsysVCS, Tags, TextMate, TortoiseGit, Vagrant, Vim, VirtualEnv, Virtuoso, VisualStudioCode, WebMethods, Windows, Xcode, XilinxISE, macOS, Go, Godot, Gradle, Grails, Haskell, IGORPro, Idris, JBoss, Java, Jekyll, Joomla, Julia, KiCad, Kohana, Kotlin, LabVIEW, Laravel, Leiningen, LemonStand, Lilypond, Lithium, Lua, Magento, Maven, Mercury, MetaProgrammingSystem, MiniProgram, Nanoc, Nim, Node, OCaml, Objective-C, Opa, OpenCart, OracleForms, Packer, Perl, Perl6, Phalcon, PlayFramework, Plone, Prestashop, Processing, PureScript, Python, Qooxdoo, Qt, R, ROS, Rails, RhodesRhomobile, Ruby, Rust, SCons, Sass, Scala, Scheme, Scrivener, Sdcc, SeamGen, SketchUp, Smalltalk, Stella, SugarCRM, Swift, Symfony, SymphonyCMS, TeX, Terraform, Textpattern, TurboGears2, Typo3, Umbraco, Unity, UnrealEngine, VVVV, VisualStudio, Waf, WordPress, Xojo, Yeoman, Yii, ZendFramework, Zephir]
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5Licenses"></a>
# **getV5Licenses**
> getV5Licenses(accessToken)

列出可使用的开源许可协议

列出可使用的开源许可协议

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.MiscellaneousApi;


MiscellaneousApi apiInstance = new MiscellaneousApi();
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.getV5Licenses(accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling MiscellaneousApi#getV5Licenses");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5LicensesLicense"></a>
# **getV5LicensesLicense**
> getV5LicensesLicense(license, accessToken)

获取一个开源许可协议

获取一个开源许可协议

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.MiscellaneousApi;


MiscellaneousApi apiInstance = new MiscellaneousApi();
String license = "license_example"; // String | 协议名称
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.getV5LicensesLicense(license, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling MiscellaneousApi#getV5LicensesLicense");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **license** | **String**| 协议名称 | [enum: MulanPSL-1.0, AFL-3.0, AGPL-3.0, Apache-2.0, Artistic-2.0, BSD-2-Clause, BSD-3-Clause, BSD-3-Clause-Clear, BSL-1.0, CC-BY-4.0, CC-BY-SA-4.0, CC0-1.0, ECL-2.0, EPL-1.0, EUPL-1.1, GPL-2.0, GPL-3.0, ISC, LGPL-2.1, LGPL-3.0, LPPL-1.3c, MIT, MPL-2.0, MS-PL, MS-RL, NCSA, OFL-1.1, OSL-3.0, PostgreSQL, Unlicense, WTFPL, Zlib]
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5LicensesLicenseRaw"></a>
# **getV5LicensesLicenseRaw**
> getV5LicensesLicenseRaw(license, accessToken)

获取一个开源许可协议原始文件

获取一个开源许可协议原始文件

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.MiscellaneousApi;


MiscellaneousApi apiInstance = new MiscellaneousApi();
String license = "license_example"; // String | 协议名称
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.getV5LicensesLicenseRaw(license, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling MiscellaneousApi#getV5LicensesLicenseRaw");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **license** | **String**| 协议名称 | [enum: MulanPSL-1.0, AFL-3.0, AGPL-3.0, Apache-2.0, Artistic-2.0, BSD-2-Clause, BSD-3-Clause, BSD-3-Clause-Clear, BSL-1.0, CC-BY-4.0, CC-BY-SA-4.0, CC0-1.0, ECL-2.0, EPL-1.0, EUPL-1.1, GPL-2.0, GPL-3.0, ISC, LGPL-2.1, LGPL-3.0, LPPL-1.3c, MIT, MPL-2.0, MS-PL, MS-RL, NCSA, OFL-1.1, OSL-3.0, PostgreSQL, Unlicense, WTFPL, Zlib]
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoLicense"></a>
# **getV5ReposOwnerRepoLicense**
> getV5ReposOwnerRepoLicense(owner, repo, accessToken)

获取一个仓库使用的开源许可协议

获取一个仓库使用的开源许可协议

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.MiscellaneousApi;


MiscellaneousApi apiInstance = new MiscellaneousApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.getV5ReposOwnerRepoLicense(owner, repo, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling MiscellaneousApi#getV5ReposOwnerRepoLicense");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="postV5Markdown"></a>
# **postV5Markdown**
> postV5Markdown(text, accessToken)

渲染 Markdown 文本

渲染 Markdown 文本

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.MiscellaneousApi;


MiscellaneousApi apiInstance = new MiscellaneousApi();
String text = "text_example"; // String | Markdown 文本
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.postV5Markdown(text, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling MiscellaneousApi#postV5Markdown");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **text** | **String**| Markdown 文本 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

