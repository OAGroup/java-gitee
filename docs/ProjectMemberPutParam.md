
# ProjectMemberPutParam

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accessToken** | **String** | 用户授权码 |  [optional]
**permission** | [**PermissionEnum**](#PermissionEnum) | 成员权限: 拉代码(pull)，推代码(push)，管理员(admin)。默认: push |  [optional]


<a name="PermissionEnum"></a>
## Enum: PermissionEnum
Name | Value
---- | -----
PULL | &quot;pull&quot;
PUSH | &quot;push&quot;
ADMIN | &quot;admin&quot;



