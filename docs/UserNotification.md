
# UserNotification

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**content** | **String** |  |  [optional]
**type** | **String** |  |  [optional]
**unread** | **String** |  |  [optional]
**mute** | **String** |  |  [optional]
**updatedAt** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**actor** | [**UserBasic**](UserBasic.md) | 通知发送者 |  [optional]
**repository** | [**ProjectBasic**](ProjectBasic.md) |  |  [optional]
**subject** | [**UserNotificationSubject**](UserNotificationSubject.md) | 通知直接关联对象 |  [optional]
**namespaces** | [**List&lt;UserNotificationNamespace&gt;**](UserNotificationNamespace.md) | 通知次级关联对象 |  [optional]



