
# Commit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**treeId** | **String** |  |  [optional]
**parentIds** | **List&lt;String&gt;** |  |  [optional]
**message** | **String** |  |  [optional]
**timestamp** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**url** | **String** |  |  [optional]
**author** | [**UserBasic**](UserBasic.md) |  |  [optional]
**committer** | [**UserBasic**](UserBasic.md) |  |  [optional]
**distinct** | **Boolean** |  |  [optional]
**added** | **List&lt;String&gt;** |  |  [optional]
**removed** | **List&lt;String&gt;** |  |  [optional]
**modified** | **List&lt;String&gt;** |  |  [optional]



