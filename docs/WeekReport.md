
# WeekReport

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**content** | **String** |  |  [optional]
**contentHtml** | **String** |  |  [optional]
**year** | **String** |  |  [optional]
**month** | **String** |  |  [optional]
**weekIndex** | **String** |  |  [optional]
**weekBegin** | **String** |  |  [optional]
**weekEnd** | **String** |  |  [optional]
**createdAt** | **String** |  |  [optional]
**updatedAt** | **String** |  |  [optional]
**user** | [**UserMini**](UserMini.md) |  |  [optional]



