# SearchApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getV5SearchGists**](SearchApi.md#getV5SearchGists) | **GET** /v5/search/gists | 搜索代码片段
[**getV5SearchIssues**](SearchApi.md#getV5SearchIssues) | **GET** /v5/search/issues | 搜索 Issues
[**getV5SearchRepositories**](SearchApi.md#getV5SearchRepositories) | **GET** /v5/search/repositories | 搜索仓库
[**getV5SearchUsers**](SearchApi.md#getV5SearchUsers) | **GET** /v5/search/users | 搜索用户


<a name="getV5SearchGists"></a>
# **getV5SearchGists**
> List&lt;Code&gt; getV5SearchGists(q, accessToken, page, perPage, language, owner, sort, order)

搜索代码片段

搜索代码片段

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.SearchApi;


SearchApi apiInstance = new SearchApi();
String q = "q_example"; // String | 搜索关键字
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
String language = "language_example"; // String | 筛选指定语言的代码片段
String owner = "owner_example"; // String | 筛选所属用户 (username/login) 的代码片段
String sort = "sort_example"; // String | 排序字段，created_at(创建时间)、updated_at(更新时间)、notes_count(评论数)、stars_count(收藏数)、forks_count(Fork 数)，默认为最佳匹配
String order = "desc"; // String | 排序顺序: desc(default)、asc
try {
    List<Code> result = apiInstance.getV5SearchGists(q, accessToken, page, perPage, language, owner, sort, order);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SearchApi#getV5SearchGists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **q** | **String**| 搜索关键字 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]
 **language** | **String**| 筛选指定语言的代码片段 | [optional] [enum: Java, Zephir, Pawn, PHP, JavaScript, C#, C/C++, Python, Ruby, Go, Perl, ASP, Objective-C, HTML, CSS, TypeScript, Shell, Erlang, Kotlin, Swift, Rust, Lua, Assembly, Pascal, Scala, ActionScript, Groovy, Clojure, Matlab, VimL, R, Lisp, SQL, XML, 易语言, Smalltalk, 其他, C, CoffeeScript, C++, AutoHotkey, Arduino, Common Lisp, Scheme, FORTRAN, Verilog, Puppet, XSLT, Delphi, eC, D, Haskell, Visual Basic, PowerShell, Emacs Lisp, Racket, OCaml, Vala, Ada, Dart, M, ColdFusion, Awk, LiveScript, Slash, Julia, Nemerle, Elixir, VHDL, TeX/LaTeX, Standard ML, Haxe, Eiffel, DOT, Scilab, Prolog, NodeJS, Android, Coq, Logos, QML, Docker, 微信, Crystal, HTML/CSS, 汇编]
 **owner** | **String**| 筛选所属用户 (username/login) 的代码片段 | [optional]
 **sort** | **String**| 排序字段，created_at(创建时间)、updated_at(更新时间)、notes_count(评论数)、stars_count(收藏数)、forks_count(Fork 数)，默认为最佳匹配 | [optional] [enum: created_at, updated_at, notes_count, stars_count, forks_count]
 **order** | **String**| 排序顺序: desc(default)、asc | [optional] [default to desc] [enum: asc, desc]

### Return type

[**List&lt;Code&gt;**](Code.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5SearchIssues"></a>
# **getV5SearchIssues**
> List&lt;Issue&gt; getV5SearchIssues(q, accessToken, page, perPage, repo, language, label, state, author, assignee, sort, order)

搜索 Issues

搜索 Issues

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.SearchApi;


SearchApi apiInstance = new SearchApi();
String q = "q_example"; // String | 搜索关键字
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
String repo = "repo_example"; // String | 筛选指定仓库 (path, e.g. oschina/git-osc) 的 issues
String language = "language_example"; // String | 筛选指定语言的 issues
String label = "label_example"; // String | 筛选指定标签的 issues
String state = "state_example"; // String | 筛选指定状态的 issues, open(开启)、closed(完成)、rejected(拒绝)
String author = "author_example"; // String | 筛选指定创建者 (username/login) 的 issues
String assignee = "assignee_example"; // String | 筛选指定负责人 (username/login) 的 issues
String sort = "sort_example"; // String | 排序字段，created_at(创建时间)、last_push_at(更新时间)、notes_count(评论数)，默认为最佳匹配
String order = "desc"; // String | 排序顺序: desc(default)、asc
try {
    List<Issue> result = apiInstance.getV5SearchIssues(q, accessToken, page, perPage, repo, language, label, state, author, assignee, sort, order);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SearchApi#getV5SearchIssues");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **q** | **String**| 搜索关键字 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]
 **repo** | **String**| 筛选指定仓库 (path, e.g. oschina/git-osc) 的 issues | [optional]
 **language** | **String**| 筛选指定语言的 issues | [optional] [enum: Java, JavaScript, PHP, Python, Android, Objective-C, C#, Go, C++, HTML, C, NodeJS, Swift, Ruby, TypeScript, Shell, CSS, Docker, Lua, Dart, Scala, Matlab, Delphi, ASP, TeX/LaTeX, Visual Basic, ActionScript, Groovy, Erlang, R, Rust, Verilog, VimL, Perl, QML, Arduino, Assembly, Pascal, Clojure, Emacs Lisp, FORTRAN, CoffeeScript, PowerShell, AutoHotkey, Elixir, VHDL, D, Haskell, M, Julia, Scheme, Common Lisp, XSLT, Logos, DOT, Racket, OCaml, Puppet, Haxe, Coq, LiveScript, Vala, Nemerle, Prolog, Smalltalk, Eiffel, Standard ML, eC, Ada, Slash, Scilab, Awk, ColdFusion, 微信, Crystal, Kotlin, SQL, Lisp, XML, C/C++, HTML/CSS, 易语言, 汇编, 其他, Zephir, Pawn]
 **label** | **String**| 筛选指定标签的 issues | [optional]
 **state** | **String**| 筛选指定状态的 issues, open(开启)、closed(完成)、rejected(拒绝) | [optional] [enum: open, progressing, closed, rejected]
 **author** | **String**| 筛选指定创建者 (username/login) 的 issues | [optional]
 **assignee** | **String**| 筛选指定负责人 (username/login) 的 issues | [optional]
 **sort** | **String**| 排序字段，created_at(创建时间)、last_push_at(更新时间)、notes_count(评论数)，默认为最佳匹配 | [optional] [enum: created_at, updated_at, notes_count]
 **order** | **String**| 排序顺序: desc(default)、asc | [optional] [default to desc] [enum: asc, desc]

### Return type

[**List&lt;Issue&gt;**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5SearchRepositories"></a>
# **getV5SearchRepositories**
> List&lt;Project&gt; getV5SearchRepositories(q, accessToken, page, perPage, owner, fork, language, sort, order)

搜索仓库

搜索仓库

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.SearchApi;


SearchApi apiInstance = new SearchApi();
String q = "q_example"; // String | 搜索关键字
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
String owner = "owner_example"; // String | 筛选指定空间地址(企业、组织或个人的地址 path) 的仓库
Boolean fork = true; // Boolean | 是否搜索含 fork 的仓库，默认：否
String language = "language_example"; // String | 筛选指定语言的仓库
String sort = "sort_example"; // String | 排序字段，created_at(创建时间)、last_push_at(更新时间)、stars_count(收藏数)、forks_count(Fork 数)、watches_count(关注数)，默认为最佳匹配
String order = "desc"; // String | 排序顺序: desc(default)、asc
try {
    List<Project> result = apiInstance.getV5SearchRepositories(q, accessToken, page, perPage, owner, fork, language, sort, order);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SearchApi#getV5SearchRepositories");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **q** | **String**| 搜索关键字 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]
 **owner** | **String**| 筛选指定空间地址(企业、组织或个人的地址 path) 的仓库 | [optional]
 **fork** | **Boolean**| 是否搜索含 fork 的仓库，默认：否 | [optional]
 **language** | **String**| 筛选指定语言的仓库 | [optional] [enum: Java, JavaScript, PHP, Python, Android, Objective-C, C#, Go, C++, HTML, C, NodeJS, Swift, Ruby, TypeScript, Shell, CSS, Docker, Lua, Dart, Scala, Matlab, Delphi, ASP, TeX/LaTeX, Visual Basic, ActionScript, Groovy, Erlang, R, Rust, Verilog, VimL, Perl, QML, Arduino, Assembly, Pascal, Clojure, Emacs Lisp, FORTRAN, CoffeeScript, PowerShell, AutoHotkey, Elixir, VHDL, D, Haskell, M, Julia, Scheme, Common Lisp, XSLT, Logos, DOT, Racket, OCaml, Puppet, Haxe, Coq, LiveScript, Vala, Nemerle, Prolog, Smalltalk, Eiffel, Standard ML, eC, Ada, Slash, Scilab, Awk, ColdFusion, 微信, Crystal, Kotlin, SQL, Lisp, XML, C/C++, HTML/CSS, 易语言, 汇编, 其他, Zephir, Pawn]
 **sort** | **String**| 排序字段，created_at(创建时间)、last_push_at(更新时间)、stars_count(收藏数)、forks_count(Fork 数)、watches_count(关注数)，默认为最佳匹配 | [optional] [enum: created_at, last_push_at, stars_count, forks_count, watches_count]
 **order** | **String**| 排序顺序: desc(default)、asc | [optional] [default to desc] [enum: asc, desc]

### Return type

[**List&lt;Project&gt;**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5SearchUsers"></a>
# **getV5SearchUsers**
> List&lt;User&gt; getV5SearchUsers(q, accessToken, page, perPage, sort, order)

搜索用户

搜索用户

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.SearchApi;


SearchApi apiInstance = new SearchApi();
String q = "q_example"; // String | 搜索关键字
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
String sort = "sort_example"; // String | 排序字段，joined_at(注册时间)，默认为最佳匹配
String order = "desc"; // String | 排序顺序: desc(default)、asc
try {
    List<User> result = apiInstance.getV5SearchUsers(q, accessToken, page, perPage, sort, order);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SearchApi#getV5SearchUsers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **q** | **String**| 搜索关键字 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]
 **sort** | **String**| 排序字段，joined_at(注册时间)，默认为最佳匹配 | [optional] [enum: joined_at]
 **order** | **String**| 排序顺序: desc(default)、asc | [optional] [default to desc] [enum: asc, desc]

### Return type

[**List&lt;User&gt;**](User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

