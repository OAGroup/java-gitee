
# Milestone

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **String** |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**number** | **Integer** |  |  [optional]
**repositoryId** | **Integer** |  |  [optional]
**state** | **String** |  |  [optional]
**title** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**updatedAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**createdAt** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**openIssues** | **Integer** |  |  [optional]
**closedIssues** | **Integer** |  |  [optional]
**dueOn** | **String** |  |  [optional]



