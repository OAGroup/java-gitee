
# PullRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**url** | **String** |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**diffUrl** | **String** |  |  [optional]
**patchUrl** | **String** |  |  [optional]
**issueUrl** | **String** |  |  [optional]
**commitsUrl** | **String** |  |  [optional]
**reviewCommentsUrl** | **String** |  |  [optional]
**reviewCommentUrl** | **String** |  |  [optional]
**commentsUrl** | **String** |  |  [optional]
**statusesUrl** | **String** |  |  [optional]
**number** | **Integer** |  |  [optional]
**state** | **String** |  |  [optional]
**title** | **String** |  |  [optional]
**body** | **String** |  |  [optional]
**bodyHtml** | **String** |  |  [optional]
**assignees** | [**List&lt;UserBasic&gt;**](UserBasic.md) |  |  [optional]
**testers** | [**List&lt;UserBasic&gt;**](UserBasic.md) |  |  [optional]
**milestone** | [**Milestone**](Milestone.md) |  |  [optional]
**labels** | [**List&lt;Label&gt;**](Label.md) |  |  [optional]
**locked** | **String** |  |  [optional]
**createdAt** | **String** |  |  [optional]
**updatedAt** | **String** |  |  [optional]
**closedAt** | **String** |  |  [optional]
**mergedAt** | **String** |  |  [optional]
**mergeable** | **Boolean** |  |  [optional]
**head** | [**BasicInfo**](BasicInfo.md) |  |  [optional]
**base** | [**BasicInfo**](BasicInfo.md) |  |  [optional]
**links** | **String** |  |  [optional]
**user** | [**UserBasic**](UserBasic.md) |  |  [optional]
**comments** | **Integer** |  |  [optional]
**commits** | **Integer** |  |  [optional]
**additions** | **Integer** |  |  [optional]
**deletions** | **Integer** |  |  [optional]
**changedFiles** | **Integer** |  |  [optional]



