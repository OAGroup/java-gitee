
# UserBasic

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**login** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**avatarUrl** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**followersUrl** | **String** |  |  [optional]
**followingUrl** | **String** |  |  [optional]
**gistsUrl** | **String** |  |  [optional]
**starredUrl** | **String** |  |  [optional]
**subscriptionsUrl** | **String** |  |  [optional]
**organizationsUrl** | **String** |  |  [optional]
**reposUrl** | **String** |  |  [optional]
**eventsUrl** | **String** |  |  [optional]
**receivedEventsUrl** | **String** |  |  [optional]
**type** | **String** |  |  [optional]
**siteAdmin** | **Boolean** |  |  [optional]
**email** | **String** |  |  [optional]



