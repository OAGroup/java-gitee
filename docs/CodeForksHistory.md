
# CodeForksHistory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **String** |  |  [optional]
**forksUrl** | **String** |  |  [optional]
**commitsUrl** | **String** |  |  [optional]
**id** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**_public** | **String** |  |  [optional]
**owner** | **String** |  |  [optional]
**user** | **String** |  |  [optional]
**files** | **String** |  |  [optional]
**truncated** | **String** |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**comments** | **String** |  |  [optional]
**commentsUrl** | **String** |  |  [optional]
**gitPullUrl** | **String** |  |  [optional]
**gitPushUrl** | **String** |  |  [optional]
**createdAt** | **String** |  |  [optional]
**updatedAt** | **String** |  |  [optional]
**forks** | **String** |  |  [optional]
**history** | **String** |  |  [optional]



