# ActivityApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteV5UserStarredOwnerRepo**](ActivityApi.md#deleteV5UserStarredOwnerRepo) | **DELETE** /v5/user/starred/{owner}/{repo} | 取消 star 一个仓库
[**deleteV5UserSubscriptionsOwnerRepo**](ActivityApi.md#deleteV5UserSubscriptionsOwnerRepo) | **DELETE** /v5/user/subscriptions/{owner}/{repo} | 取消 watch 一个仓库
[**getV5Events**](ActivityApi.md#getV5Events) | **GET** /v5/events | 获取站内所有公开动态
[**getV5NetworksOwnerRepoEvents**](ActivityApi.md#getV5NetworksOwnerRepoEvents) | **GET** /v5/networks/{owner}/{repo}/events | 列出仓库的所有公开动态
[**getV5NotificationsCount**](ActivityApi.md#getV5NotificationsCount) | **GET** /v5/notifications/count | 获取授权用户的通知数
[**getV5NotificationsMessages**](ActivityApi.md#getV5NotificationsMessages) | **GET** /v5/notifications/messages | 列出授权用户的所有私信
[**getV5NotificationsMessagesId**](ActivityApi.md#getV5NotificationsMessagesId) | **GET** /v5/notifications/messages/{id} | 获取一条私信
[**getV5NotificationsThreads**](ActivityApi.md#getV5NotificationsThreads) | **GET** /v5/notifications/threads | 列出授权用户的所有通知
[**getV5NotificationsThreadsId**](ActivityApi.md#getV5NotificationsThreadsId) | **GET** /v5/notifications/threads/{id} | 获取一条通知
[**getV5OrgsOrgEvents**](ActivityApi.md#getV5OrgsOrgEvents) | **GET** /v5/orgs/{org}/events | 列出组织的公开动态
[**getV5ReposOwnerRepoEvents**](ActivityApi.md#getV5ReposOwnerRepoEvents) | **GET** /v5/repos/{owner}/{repo}/events | 列出仓库的所有动态
[**getV5ReposOwnerRepoNotifications**](ActivityApi.md#getV5ReposOwnerRepoNotifications) | **GET** /v5/repos/{owner}/{repo}/notifications | 列出一个仓库里的通知
[**getV5ReposOwnerRepoStargazers**](ActivityApi.md#getV5ReposOwnerRepoStargazers) | **GET** /v5/repos/{owner}/{repo}/stargazers | 列出 star 了仓库的用户
[**getV5ReposOwnerRepoSubscribers**](ActivityApi.md#getV5ReposOwnerRepoSubscribers) | **GET** /v5/repos/{owner}/{repo}/subscribers | 列出 watch 了仓库的用户
[**getV5UserStarred**](ActivityApi.md#getV5UserStarred) | **GET** /v5/user/starred | 列出授权用户 star 了的仓库
[**getV5UserStarredOwnerRepo**](ActivityApi.md#getV5UserStarredOwnerRepo) | **GET** /v5/user/starred/{owner}/{repo} | 检查授权用户是否 star 了一个仓库
[**getV5UserSubscriptions**](ActivityApi.md#getV5UserSubscriptions) | **GET** /v5/user/subscriptions | 列出授权用户 watch 了的仓库
[**getV5UserSubscriptionsOwnerRepo**](ActivityApi.md#getV5UserSubscriptionsOwnerRepo) | **GET** /v5/user/subscriptions/{owner}/{repo} | 检查授权用户是否 watch 了一个仓库
[**getV5UsersUsernameEvents**](ActivityApi.md#getV5UsersUsernameEvents) | **GET** /v5/users/{username}/events | 列出用户的动态
[**getV5UsersUsernameEventsOrgsOrg**](ActivityApi.md#getV5UsersUsernameEventsOrgsOrg) | **GET** /v5/users/{username}/events/orgs/{org} | 列出用户所属组织的动态
[**getV5UsersUsernameEventsPublic**](ActivityApi.md#getV5UsersUsernameEventsPublic) | **GET** /v5/users/{username}/events/public | 列出用户的公开动态
[**getV5UsersUsernameReceivedEvents**](ActivityApi.md#getV5UsersUsernameReceivedEvents) | **GET** /v5/users/{username}/received_events | 列出一个用户收到的动态
[**getV5UsersUsernameReceivedEventsPublic**](ActivityApi.md#getV5UsersUsernameReceivedEventsPublic) | **GET** /v5/users/{username}/received_events/public | 列出一个用户收到的公开动态
[**getV5UsersUsernameStarred**](ActivityApi.md#getV5UsersUsernameStarred) | **GET** /v5/users/{username}/starred | 列出用户 star 了的仓库
[**getV5UsersUsernameSubscriptions**](ActivityApi.md#getV5UsersUsernameSubscriptions) | **GET** /v5/users/{username}/subscriptions | 列出用户 watch 了的仓库
[**patchV5NotificationsMessagesId**](ActivityApi.md#patchV5NotificationsMessagesId) | **PATCH** /v5/notifications/messages/{id} | 标记一条私信为已读
[**patchV5NotificationsThreadsId**](ActivityApi.md#patchV5NotificationsThreadsId) | **PATCH** /v5/notifications/threads/{id} | 标记一条通知为已读
[**postV5NotificationsMessages**](ActivityApi.md#postV5NotificationsMessages) | **POST** /v5/notifications/messages | 发送私信给指定用户
[**putV5NotificationsMessages**](ActivityApi.md#putV5NotificationsMessages) | **PUT** /v5/notifications/messages | 标记所有私信为已读
[**putV5NotificationsThreads**](ActivityApi.md#putV5NotificationsThreads) | **PUT** /v5/notifications/threads | 标记所有通知为已读
[**putV5ReposOwnerRepoNotifications**](ActivityApi.md#putV5ReposOwnerRepoNotifications) | **PUT** /v5/repos/{owner}/{repo}/notifications | 标记一个仓库里的通知为已读
[**putV5UserStarredOwnerRepo**](ActivityApi.md#putV5UserStarredOwnerRepo) | **PUT** /v5/user/starred/{owner}/{repo} | star 一个仓库
[**putV5UserSubscriptionsOwnerRepo**](ActivityApi.md#putV5UserSubscriptionsOwnerRepo) | **PUT** /v5/user/subscriptions/{owner}/{repo} | watch 一个仓库


<a name="deleteV5UserStarredOwnerRepo"></a>
# **deleteV5UserStarredOwnerRepo**
> deleteV5UserStarredOwnerRepo(owner, repo, accessToken)

取消 star 一个仓库

取消 star 一个仓库

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.ActivityApi;


ActivityApi apiInstance = new ActivityApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.deleteV5UserStarredOwnerRepo(owner, repo, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#deleteV5UserStarredOwnerRepo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="deleteV5UserSubscriptionsOwnerRepo"></a>
# **deleteV5UserSubscriptionsOwnerRepo**
> deleteV5UserSubscriptionsOwnerRepo(owner, repo, accessToken)

取消 watch 一个仓库

取消 watch 一个仓库

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.ActivityApi;


ActivityApi apiInstance = new ActivityApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.deleteV5UserSubscriptionsOwnerRepo(owner, repo, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#deleteV5UserSubscriptionsOwnerRepo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5Events"></a>
# **getV5Events**
> List&lt;Event&gt; getV5Events(accessToken, page, perPage)

获取站内所有公开动态

获取站内所有公开动态

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.ActivityApi;


ActivityApi apiInstance = new ActivityApi();
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Event> result = apiInstance.getV5Events(accessToken, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getV5Events");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;Event&gt;**](Event.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5NetworksOwnerRepoEvents"></a>
# **getV5NetworksOwnerRepoEvents**
> List&lt;Event&gt; getV5NetworksOwnerRepoEvents(owner, repo, accessToken, page, perPage)

列出仓库的所有公开动态

列出仓库的所有公开动态

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.ActivityApi;


ActivityApi apiInstance = new ActivityApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Event> result = apiInstance.getV5NetworksOwnerRepoEvents(owner, repo, accessToken, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getV5NetworksOwnerRepoEvents");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;Event&gt;**](Event.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5NotificationsCount"></a>
# **getV5NotificationsCount**
> UserNotificationCount getV5NotificationsCount(accessToken, unread)

获取授权用户的通知数

获取授权用户的通知数

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.ActivityApi;


ActivityApi apiInstance = new ActivityApi();
String accessToken = "accessToken_example"; // String | 用户授权码
Boolean unread = true; // Boolean | 是否只获取未读消息，默认：否
try {
    UserNotificationCount result = apiInstance.getV5NotificationsCount(accessToken, unread);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getV5NotificationsCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]
 **unread** | **Boolean**| 是否只获取未读消息，默认：否 | [optional]

### Return type

[**UserNotificationCount**](UserNotificationCount.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5NotificationsMessages"></a>
# **getV5NotificationsMessages**
> List&lt;UserMessageList&gt; getV5NotificationsMessages(accessToken, unread, since, before, ids, page, perPage)

列出授权用户的所有私信

列出授权用户的所有私信

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.ActivityApi;


ActivityApi apiInstance = new ActivityApi();
String accessToken = "accessToken_example"; // String | 用户授权码
Boolean unread = true; // Boolean | 是否只显示未读私信，默认：否
String since = "since_example"; // String | 只获取在给定时间后更新的私信，要求时间格式为 ISO 8601
String before = "before_example"; // String | 只获取在给定时间前更新的私信，要求时间格式为 ISO 8601
String ids = "ids_example"; // String | 指定一组私信 ID，以 , 分隔
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<UserMessageList> result = apiInstance.getV5NotificationsMessages(accessToken, unread, since, before, ids, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getV5NotificationsMessages");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]
 **unread** | **Boolean**| 是否只显示未读私信，默认：否 | [optional]
 **since** | **String**| 只获取在给定时间后更新的私信，要求时间格式为 ISO 8601 | [optional]
 **before** | **String**| 只获取在给定时间前更新的私信，要求时间格式为 ISO 8601 | [optional]
 **ids** | **String**| 指定一组私信 ID，以 , 分隔 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;UserMessageList&gt;**](UserMessageList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5NotificationsMessagesId"></a>
# **getV5NotificationsMessagesId**
> UserMessage getV5NotificationsMessagesId(id, accessToken)

获取一条私信

获取一条私信

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.ActivityApi;


ActivityApi apiInstance = new ActivityApi();
String id = "id_example"; // String | 私信的 ID
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    UserMessage result = apiInstance.getV5NotificationsMessagesId(id, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getV5NotificationsMessagesId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| 私信的 ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**UserMessage**](UserMessage.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5NotificationsThreads"></a>
# **getV5NotificationsThreads**
> List&lt;UserNotificationList&gt; getV5NotificationsThreads(accessToken, unread, participating, type, since, before, ids, page, perPage)

列出授权用户的所有通知

列出授权用户的所有通知

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.ActivityApi;


ActivityApi apiInstance = new ActivityApi();
String accessToken = "accessToken_example"; // String | 用户授权码
Boolean unread = true; // Boolean | 是否只获取未读消息，默认：否
Boolean participating = true; // Boolean | 是否只获取自己直接参与的消息，默认：否
String type = "all"; // String | 筛选指定类型的通知，all：所有，event：事件通知，referer：@ 通知
String since = "since_example"; // String | 只获取在给定时间后更新的消息，要求时间格式为 ISO 8601
String before = "before_example"; // String | 只获取在给定时间前更新的消息，要求时间格式为 ISO 8601
String ids = "ids_example"; // String | 指定一组通知 ID，以 , 分隔
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<UserNotificationList> result = apiInstance.getV5NotificationsThreads(accessToken, unread, participating, type, since, before, ids, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getV5NotificationsThreads");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]
 **unread** | **Boolean**| 是否只获取未读消息，默认：否 | [optional]
 **participating** | **Boolean**| 是否只获取自己直接参与的消息，默认：否 | [optional]
 **type** | **String**| 筛选指定类型的通知，all：所有，event：事件通知，referer：@ 通知 | [optional] [default to all] [enum: all, event, referer]
 **since** | **String**| 只获取在给定时间后更新的消息，要求时间格式为 ISO 8601 | [optional]
 **before** | **String**| 只获取在给定时间前更新的消息，要求时间格式为 ISO 8601 | [optional]
 **ids** | **String**| 指定一组通知 ID，以 , 分隔 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;UserNotificationList&gt;**](UserNotificationList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5NotificationsThreadsId"></a>
# **getV5NotificationsThreadsId**
> UserNotification getV5NotificationsThreadsId(id, accessToken)

获取一条通知

获取一条通知

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.ActivityApi;


ActivityApi apiInstance = new ActivityApi();
String id = "id_example"; // String | 通知的 ID
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    UserNotification result = apiInstance.getV5NotificationsThreadsId(id, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getV5NotificationsThreadsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| 通知的 ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**UserNotification**](UserNotification.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5OrgsOrgEvents"></a>
# **getV5OrgsOrgEvents**
> List&lt;Event&gt; getV5OrgsOrgEvents(org, accessToken, page, perPage)

列出组织的公开动态

列出组织的公开动态

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.ActivityApi;


ActivityApi apiInstance = new ActivityApi();
String org = "org_example"; // String | 组织的路径(path/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Event> result = apiInstance.getV5OrgsOrgEvents(org, accessToken, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getV5OrgsOrgEvents");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org** | **String**| 组织的路径(path/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;Event&gt;**](Event.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoEvents"></a>
# **getV5ReposOwnerRepoEvents**
> List&lt;Event&gt; getV5ReposOwnerRepoEvents(owner, repo, accessToken, page, perPage)

列出仓库的所有动态

列出仓库的所有动态

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.ActivityApi;


ActivityApi apiInstance = new ActivityApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Event> result = apiInstance.getV5ReposOwnerRepoEvents(owner, repo, accessToken, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getV5ReposOwnerRepoEvents");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;Event&gt;**](Event.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoNotifications"></a>
# **getV5ReposOwnerRepoNotifications**
> List&lt;UserNotificationList&gt; getV5ReposOwnerRepoNotifications(owner, repo, accessToken, unread, participating, type, since, before, ids, page, perPage)

列出一个仓库里的通知

列出一个仓库里的通知

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.ActivityApi;


ActivityApi apiInstance = new ActivityApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Boolean unread = true; // Boolean | 是否只获取未读消息，默认：否
Boolean participating = true; // Boolean | 是否只获取自己直接参与的消息，默认：否
String type = "all"; // String | 筛选指定类型的通知，all：所有，event：事件通知，referer：@ 通知
String since = "since_example"; // String | 只获取在给定时间后更新的消息，要求时间格式为 ISO 8601
String before = "before_example"; // String | 只获取在给定时间前更新的消息，要求时间格式为 ISO 8601
String ids = "ids_example"; // String | 指定一组通知 ID，以 , 分隔
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<UserNotificationList> result = apiInstance.getV5ReposOwnerRepoNotifications(owner, repo, accessToken, unread, participating, type, since, before, ids, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getV5ReposOwnerRepoNotifications");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **unread** | **Boolean**| 是否只获取未读消息，默认：否 | [optional]
 **participating** | **Boolean**| 是否只获取自己直接参与的消息，默认：否 | [optional]
 **type** | **String**| 筛选指定类型的通知，all：所有，event：事件通知，referer：@ 通知 | [optional] [default to all] [enum: all, event, referer]
 **since** | **String**| 只获取在给定时间后更新的消息，要求时间格式为 ISO 8601 | [optional]
 **before** | **String**| 只获取在给定时间前更新的消息，要求时间格式为 ISO 8601 | [optional]
 **ids** | **String**| 指定一组通知 ID，以 , 分隔 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;UserNotificationList&gt;**](UserNotificationList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoStargazers"></a>
# **getV5ReposOwnerRepoStargazers**
> List&lt;UserBasic&gt; getV5ReposOwnerRepoStargazers(owner, repo, accessToken, page, perPage)

列出 star 了仓库的用户

列出 star 了仓库的用户

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.ActivityApi;


ActivityApi apiInstance = new ActivityApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<UserBasic> result = apiInstance.getV5ReposOwnerRepoStargazers(owner, repo, accessToken, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getV5ReposOwnerRepoStargazers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;UserBasic&gt;**](UserBasic.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoSubscribers"></a>
# **getV5ReposOwnerRepoSubscribers**
> List&lt;UserBasic&gt; getV5ReposOwnerRepoSubscribers(owner, repo, accessToken, page, perPage)

列出 watch 了仓库的用户

列出 watch 了仓库的用户

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.ActivityApi;


ActivityApi apiInstance = new ActivityApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<UserBasic> result = apiInstance.getV5ReposOwnerRepoSubscribers(owner, repo, accessToken, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getV5ReposOwnerRepoSubscribers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;UserBasic&gt;**](UserBasic.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5UserStarred"></a>
# **getV5UserStarred**
> List&lt;Project&gt; getV5UserStarred(accessToken, sort, direction, page, perPage)

列出授权用户 star 了的仓库

列出授权用户 star 了的仓库

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.ActivityApi;


ActivityApi apiInstance = new ActivityApi();
String accessToken = "accessToken_example"; // String | 用户授权码
String sort = "created"; // String | 根据仓库创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间
String direction = "desc"; // String | 按递增(asc)或递减(desc)排序，默认：递减
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Project> result = apiInstance.getV5UserStarred(accessToken, sort, direction, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getV5UserStarred");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]
 **sort** | **String**| 根据仓库创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间 | [optional] [default to created] [enum: created, last_push]
 **direction** | **String**| 按递增(asc)或递减(desc)排序，默认：递减 | [optional] [default to desc] [enum: asc, desc]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;Project&gt;**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5UserStarredOwnerRepo"></a>
# **getV5UserStarredOwnerRepo**
> getV5UserStarredOwnerRepo(owner, repo, accessToken)

检查授权用户是否 star 了一个仓库

检查授权用户是否 star 了一个仓库

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.ActivityApi;


ActivityApi apiInstance = new ActivityApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.getV5UserStarredOwnerRepo(owner, repo, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getV5UserStarredOwnerRepo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5UserSubscriptions"></a>
# **getV5UserSubscriptions**
> List&lt;Project&gt; getV5UserSubscriptions(accessToken, sort, direction, page, perPage)

列出授权用户 watch 了的仓库

列出授权用户 watch 了的仓库

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.ActivityApi;


ActivityApi apiInstance = new ActivityApi();
String accessToken = "accessToken_example"; // String | 用户授权码
String sort = "created"; // String | 根据仓库创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间
String direction = "desc"; // String | 按递增(asc)或递减(desc)排序，默认：递减
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Project> result = apiInstance.getV5UserSubscriptions(accessToken, sort, direction, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getV5UserSubscriptions");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]
 **sort** | **String**| 根据仓库创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间 | [optional] [default to created] [enum: created, last_push]
 **direction** | **String**| 按递增(asc)或递减(desc)排序，默认：递减 | [optional] [default to desc] [enum: asc, desc]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;Project&gt;**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5UserSubscriptionsOwnerRepo"></a>
# **getV5UserSubscriptionsOwnerRepo**
> getV5UserSubscriptionsOwnerRepo(owner, repo, accessToken)

检查授权用户是否 watch 了一个仓库

检查授权用户是否 watch 了一个仓库

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.ActivityApi;


ActivityApi apiInstance = new ActivityApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.getV5UserSubscriptionsOwnerRepo(owner, repo, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getV5UserSubscriptionsOwnerRepo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5UsersUsernameEvents"></a>
# **getV5UsersUsernameEvents**
> List&lt;Event&gt; getV5UsersUsernameEvents(username, accessToken, page, perPage)

列出用户的动态

列出用户的动态

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.ActivityApi;


ActivityApi apiInstance = new ActivityApi();
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Event> result = apiInstance.getV5UsersUsernameEvents(username, accessToken, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getV5UsersUsernameEvents");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;Event&gt;**](Event.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5UsersUsernameEventsOrgsOrg"></a>
# **getV5UsersUsernameEventsOrgsOrg**
> List&lt;Event&gt; getV5UsersUsernameEventsOrgsOrg(username, org, accessToken, page, perPage)

列出用户所属组织的动态

列出用户所属组织的动态

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.ActivityApi;


ActivityApi apiInstance = new ActivityApi();
String username = "username_example"; // String | 用户名(username/login)
String org = "org_example"; // String | 组织的路径(path/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Event> result = apiInstance.getV5UsersUsernameEventsOrgsOrg(username, org, accessToken, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getV5UsersUsernameEventsOrgsOrg");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **org** | **String**| 组织的路径(path/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;Event&gt;**](Event.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5UsersUsernameEventsPublic"></a>
# **getV5UsersUsernameEventsPublic**
> List&lt;Event&gt; getV5UsersUsernameEventsPublic(username, accessToken, page, perPage)

列出用户的公开动态

列出用户的公开动态

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.ActivityApi;


ActivityApi apiInstance = new ActivityApi();
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Event> result = apiInstance.getV5UsersUsernameEventsPublic(username, accessToken, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getV5UsersUsernameEventsPublic");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;Event&gt;**](Event.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5UsersUsernameReceivedEvents"></a>
# **getV5UsersUsernameReceivedEvents**
> List&lt;Event&gt; getV5UsersUsernameReceivedEvents(username, accessToken, page, perPage)

列出一个用户收到的动态

列出一个用户收到的动态

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.ActivityApi;


ActivityApi apiInstance = new ActivityApi();
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Event> result = apiInstance.getV5UsersUsernameReceivedEvents(username, accessToken, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getV5UsersUsernameReceivedEvents");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;Event&gt;**](Event.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5UsersUsernameReceivedEventsPublic"></a>
# **getV5UsersUsernameReceivedEventsPublic**
> List&lt;Event&gt; getV5UsersUsernameReceivedEventsPublic(username, accessToken, page, perPage)

列出一个用户收到的公开动态

列出一个用户收到的公开动态

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.ActivityApi;


ActivityApi apiInstance = new ActivityApi();
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Event> result = apiInstance.getV5UsersUsernameReceivedEventsPublic(username, accessToken, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getV5UsersUsernameReceivedEventsPublic");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;Event&gt;**](Event.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5UsersUsernameStarred"></a>
# **getV5UsersUsernameStarred**
> List&lt;Project&gt; getV5UsersUsernameStarred(username, accessToken, page, perPage, sort, direction)

列出用户 star 了的仓库

列出用户 star 了的仓库

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.ActivityApi;


ActivityApi apiInstance = new ActivityApi();
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
String sort = "created"; // String | 根据仓库创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间
String direction = "desc"; // String | 按递增(asc)或递减(desc)排序，默认：递减
try {
    List<Project> result = apiInstance.getV5UsersUsernameStarred(username, accessToken, page, perPage, sort, direction);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getV5UsersUsernameStarred");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]
 **sort** | **String**| 根据仓库创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间 | [optional] [default to created] [enum: created, last_push]
 **direction** | **String**| 按递增(asc)或递减(desc)排序，默认：递减 | [optional] [default to desc] [enum: asc, desc]

### Return type

[**List&lt;Project&gt;**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5UsersUsernameSubscriptions"></a>
# **getV5UsersUsernameSubscriptions**
> List&lt;Project&gt; getV5UsersUsernameSubscriptions(username, accessToken, page, perPage, sort, direction)

列出用户 watch 了的仓库

列出用户 watch 了的仓库

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.ActivityApi;


ActivityApi apiInstance = new ActivityApi();
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
String sort = "created"; // String | 根据仓库创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间
String direction = "desc"; // String | 按递增(asc)或递减(desc)排序，默认：递减
try {
    List<Project> result = apiInstance.getV5UsersUsernameSubscriptions(username, accessToken, page, perPage, sort, direction);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#getV5UsersUsernameSubscriptions");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]
 **sort** | **String**| 根据仓库创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间 | [optional] [default to created] [enum: created, last_push]
 **direction** | **String**| 按递增(asc)或递减(desc)排序，默认：递减 | [optional] [default to desc] [enum: asc, desc]

### Return type

[**List&lt;Project&gt;**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="patchV5NotificationsMessagesId"></a>
# **patchV5NotificationsMessagesId**
> patchV5NotificationsMessagesId(id, accessToken)

标记一条私信为已读

标记一条私信为已读

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.ActivityApi;


ActivityApi apiInstance = new ActivityApi();
String id = "id_example"; // String | 私信的 ID
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.patchV5NotificationsMessagesId(id, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#patchV5NotificationsMessagesId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| 私信的 ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="patchV5NotificationsThreadsId"></a>
# **patchV5NotificationsThreadsId**
> patchV5NotificationsThreadsId(id, accessToken)

标记一条通知为已读

标记一条通知为已读

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.ActivityApi;


ActivityApi apiInstance = new ActivityApi();
String id = "id_example"; // String | 通知的 ID
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.patchV5NotificationsThreadsId(id, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#patchV5NotificationsThreadsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| 通知的 ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="postV5NotificationsMessages"></a>
# **postV5NotificationsMessages**
> UserMessage postV5NotificationsMessages(username, content, accessToken)

发送私信给指定用户

发送私信给指定用户

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.ActivityApi;


ActivityApi apiInstance = new ActivityApi();
String username = "username_example"; // String | 用户名(username/login)
String content = "content_example"; // String | 私信内容
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    UserMessage result = apiInstance.postV5NotificationsMessages(username, content, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#postV5NotificationsMessages");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **content** | **String**| 私信内容 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**UserMessage**](UserMessage.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="putV5NotificationsMessages"></a>
# **putV5NotificationsMessages**
> putV5NotificationsMessages(accessToken, ids)

标记所有私信为已读

标记所有私信为已读

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.ActivityApi;


ActivityApi apiInstance = new ActivityApi();
String accessToken = "accessToken_example"; // String | 用户授权码
String ids = "ids_example"; // String | 指定一组私信 ID，以 , 分隔
try {
    apiInstance.putV5NotificationsMessages(accessToken, ids);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#putV5NotificationsMessages");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]
 **ids** | **String**| 指定一组私信 ID，以 , 分隔 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="putV5NotificationsThreads"></a>
# **putV5NotificationsThreads**
> putV5NotificationsThreads(accessToken, ids)

标记所有通知为已读

标记所有通知为已读

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.ActivityApi;


ActivityApi apiInstance = new ActivityApi();
String accessToken = "accessToken_example"; // String | 用户授权码
String ids = "ids_example"; // String | 指定一组通知 ID，以 , 分隔
try {
    apiInstance.putV5NotificationsThreads(accessToken, ids);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#putV5NotificationsThreads");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]
 **ids** | **String**| 指定一组通知 ID，以 , 分隔 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="putV5ReposOwnerRepoNotifications"></a>
# **putV5ReposOwnerRepoNotifications**
> putV5ReposOwnerRepoNotifications(owner, repo, accessToken, ids)

标记一个仓库里的通知为已读

标记一个仓库里的通知为已读

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.ActivityApi;


ActivityApi apiInstance = new ActivityApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
String ids = "ids_example"; // String | 指定一组通知 ID，以 , 分隔
try {
    apiInstance.putV5ReposOwnerRepoNotifications(owner, repo, accessToken, ids);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#putV5ReposOwnerRepoNotifications");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **ids** | **String**| 指定一组通知 ID，以 , 分隔 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="putV5UserStarredOwnerRepo"></a>
# **putV5UserStarredOwnerRepo**
> putV5UserStarredOwnerRepo(owner, repo, accessToken)

star 一个仓库

star 一个仓库

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.ActivityApi;


ActivityApi apiInstance = new ActivityApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.putV5UserStarredOwnerRepo(owner, repo, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#putV5UserStarredOwnerRepo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="putV5UserSubscriptionsOwnerRepo"></a>
# **putV5UserSubscriptionsOwnerRepo**
> putV5UserSubscriptionsOwnerRepo(owner, repo, watchType, accessToken)

watch 一个仓库

watch 一个仓库

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.ActivityApi;


ActivityApi apiInstance = new ActivityApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String watchType = "watching"; // String | watch策略, watching: 关注所有动态, releases_only: 仅关注版本发行动态, ignoring: 关注但不提醒动态
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.putV5UserSubscriptionsOwnerRepo(owner, repo, watchType, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling ActivityApi#putV5UserSubscriptionsOwnerRepo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **watchType** | **String**| watch策略, watching: 关注所有动态, releases_only: 仅关注版本发行动态, ignoring: 关注但不提醒动态 | [default to watching] [enum: watching, releases_only, ignoring]
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

