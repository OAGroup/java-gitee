
# Blob

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sha** | **String** |  |  [optional]
**size** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
**content** | **String** |  |  [optional]
**encoding** | **String** |  |  [optional]



