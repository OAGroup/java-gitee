# RepositoriesApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteV5ReposOwnerRepo**](RepositoriesApi.md#deleteV5ReposOwnerRepo) | **DELETE** /v5/repos/{owner}/{repo} | 删除一个仓库
[**deleteV5ReposOwnerRepoBranchesBranchProtection**](RepositoriesApi.md#deleteV5ReposOwnerRepoBranchesBranchProtection) | **DELETE** /v5/repos/{owner}/{repo}/branches/{branch}/protection | 取消保护分支的设置
[**deleteV5ReposOwnerRepoCollaboratorsUsername**](RepositoriesApi.md#deleteV5ReposOwnerRepoCollaboratorsUsername) | **DELETE** /v5/repos/{owner}/{repo}/collaborators/{username} | 移除仓库成员
[**deleteV5ReposOwnerRepoCommentsId**](RepositoriesApi.md#deleteV5ReposOwnerRepoCommentsId) | **DELETE** /v5/repos/{owner}/{repo}/comments/{id} | 删除Commit评论
[**deleteV5ReposOwnerRepoContentsPath**](RepositoriesApi.md#deleteV5ReposOwnerRepoContentsPath) | **DELETE** /v5/repos/{owner}/{repo}/contents/{path} | 删除文件
[**deleteV5ReposOwnerRepoKeysEnableId**](RepositoriesApi.md#deleteV5ReposOwnerRepoKeysEnableId) | **DELETE** /v5/repos/{owner}/{repo}/keys/enable/{id} | 停用仓库公钥
[**deleteV5ReposOwnerRepoKeysId**](RepositoriesApi.md#deleteV5ReposOwnerRepoKeysId) | **DELETE** /v5/repos/{owner}/{repo}/keys/{id} | 删除一个仓库公钥
[**deleteV5ReposOwnerRepoReleasesId**](RepositoriesApi.md#deleteV5ReposOwnerRepoReleasesId) | **DELETE** /v5/repos/{owner}/{repo}/releases/{id} | 删除仓库Release
[**getV5EnterprisesEnterpriseRepos**](RepositoriesApi.md#getV5EnterprisesEnterpriseRepos) | **GET** /v5/enterprises/{enterprise}/repos | 获取企业的所有仓库
[**getV5OrgsOrgRepos**](RepositoriesApi.md#getV5OrgsOrgRepos) | **GET** /v5/orgs/{org}/repos | 获取一个组织的仓库
[**getV5ReposOwnerRepo**](RepositoriesApi.md#getV5ReposOwnerRepo) | **GET** /v5/repos/{owner}/{repo} | 获取用户的某个仓库
[**getV5ReposOwnerRepoBranches**](RepositoriesApi.md#getV5ReposOwnerRepoBranches) | **GET** /v5/repos/{owner}/{repo}/branches | 获取所有分支
[**getV5ReposOwnerRepoBranchesBranch**](RepositoriesApi.md#getV5ReposOwnerRepoBranchesBranch) | **GET** /v5/repos/{owner}/{repo}/branches/{branch} | 获取单个分支
[**getV5ReposOwnerRepoCollaborators**](RepositoriesApi.md#getV5ReposOwnerRepoCollaborators) | **GET** /v5/repos/{owner}/{repo}/collaborators | 获取仓库的所有成员
[**getV5ReposOwnerRepoCollaboratorsUsername**](RepositoriesApi.md#getV5ReposOwnerRepoCollaboratorsUsername) | **GET** /v5/repos/{owner}/{repo}/collaborators/{username} | 判断用户是否为仓库成员
[**getV5ReposOwnerRepoCollaboratorsUsernamePermission**](RepositoriesApi.md#getV5ReposOwnerRepoCollaboratorsUsernamePermission) | **GET** /v5/repos/{owner}/{repo}/collaborators/{username}/permission | 查看仓库成员的权限
[**getV5ReposOwnerRepoComments**](RepositoriesApi.md#getV5ReposOwnerRepoComments) | **GET** /v5/repos/{owner}/{repo}/comments | 获取仓库的Commit评论
[**getV5ReposOwnerRepoCommentsId**](RepositoriesApi.md#getV5ReposOwnerRepoCommentsId) | **GET** /v5/repos/{owner}/{repo}/comments/{id} | 获取仓库的某条Commit评论
[**getV5ReposOwnerRepoCommits**](RepositoriesApi.md#getV5ReposOwnerRepoCommits) | **GET** /v5/repos/{owner}/{repo}/commits | 仓库的所有提交
[**getV5ReposOwnerRepoCommitsRefComments**](RepositoriesApi.md#getV5ReposOwnerRepoCommitsRefComments) | **GET** /v5/repos/{owner}/{repo}/commits/{ref}/comments | 获取单个Commit的评论
[**getV5ReposOwnerRepoCommitsSha**](RepositoriesApi.md#getV5ReposOwnerRepoCommitsSha) | **GET** /v5/repos/{owner}/{repo}/commits/{sha} | 仓库的某个提交
[**getV5ReposOwnerRepoCompareBaseHead**](RepositoriesApi.md#getV5ReposOwnerRepoCompareBaseHead) | **GET** /v5/repos/{owner}/{repo}/compare/{base}...{head} | 两个Commits之间对比的版本差异
[**getV5ReposOwnerRepoContentsPath**](RepositoriesApi.md#getV5ReposOwnerRepoContentsPath) | **GET** /v5/repos/{owner}/{repo}/contents/{path} | 获取仓库具体路径下的内容
[**getV5ReposOwnerRepoContributors**](RepositoriesApi.md#getV5ReposOwnerRepoContributors) | **GET** /v5/repos/{owner}/{repo}/contributors | 获取仓库贡献者
[**getV5ReposOwnerRepoForks**](RepositoriesApi.md#getV5ReposOwnerRepoForks) | **GET** /v5/repos/{owner}/{repo}/forks | 查看仓库的Forks
[**getV5ReposOwnerRepoKeys**](RepositoriesApi.md#getV5ReposOwnerRepoKeys) | **GET** /v5/repos/{owner}/{repo}/keys | 获取仓库已部署的公钥
[**getV5ReposOwnerRepoKeysAvailable**](RepositoriesApi.md#getV5ReposOwnerRepoKeysAvailable) | **GET** /v5/repos/{owner}/{repo}/keys/available | 获取仓库可部署的公钥
[**getV5ReposOwnerRepoKeysId**](RepositoriesApi.md#getV5ReposOwnerRepoKeysId) | **GET** /v5/repos/{owner}/{repo}/keys/{id} | 获取仓库的单个公钥
[**getV5ReposOwnerRepoPages**](RepositoriesApi.md#getV5ReposOwnerRepoPages) | **GET** /v5/repos/{owner}/{repo}/pages | 获取Pages信息
[**getV5ReposOwnerRepoReadme**](RepositoriesApi.md#getV5ReposOwnerRepoReadme) | **GET** /v5/repos/{owner}/{repo}/readme | 获取仓库README
[**getV5ReposOwnerRepoReleases**](RepositoriesApi.md#getV5ReposOwnerRepoReleases) | **GET** /v5/repos/{owner}/{repo}/releases | 获取仓库的所有Releases
[**getV5ReposOwnerRepoReleasesId**](RepositoriesApi.md#getV5ReposOwnerRepoReleasesId) | **GET** /v5/repos/{owner}/{repo}/releases/{id} | 获取仓库的单个Releases
[**getV5ReposOwnerRepoReleasesLatest**](RepositoriesApi.md#getV5ReposOwnerRepoReleasesLatest) | **GET** /v5/repos/{owner}/{repo}/releases/latest | 获取仓库的最后更新的Release
[**getV5ReposOwnerRepoReleasesTagsTag**](RepositoriesApi.md#getV5ReposOwnerRepoReleasesTagsTag) | **GET** /v5/repos/{owner}/{repo}/releases/tags/{tag} | 根据Tag名称获取仓库的Release
[**getV5ReposOwnerRepoTags**](RepositoriesApi.md#getV5ReposOwnerRepoTags) | **GET** /v5/repos/{owner}/{repo}/tags | 列出仓库所有的tags
[**getV5UserRepos**](RepositoriesApi.md#getV5UserRepos) | **GET** /v5/user/repos | 列出授权用户的所有仓库
[**getV5UsersUsernameRepos**](RepositoriesApi.md#getV5UsersUsernameRepos) | **GET** /v5/users/{username}/repos | 获取某个用户的公开仓库
[**patchV5ReposOwnerRepo**](RepositoriesApi.md#patchV5ReposOwnerRepo) | **PATCH** /v5/repos/{owner}/{repo} | 更新仓库设置
[**patchV5ReposOwnerRepoCommentsId**](RepositoriesApi.md#patchV5ReposOwnerRepoCommentsId) | **PATCH** /v5/repos/{owner}/{repo}/comments/{id} | 更新Commit评论
[**patchV5ReposOwnerRepoReleasesId**](RepositoriesApi.md#patchV5ReposOwnerRepoReleasesId) | **PATCH** /v5/repos/{owner}/{repo}/releases/{id} | 更新仓库Release
[**postV5EnterprisesEnterpriseRepos**](RepositoriesApi.md#postV5EnterprisesEnterpriseRepos) | **POST** /v5/enterprises/{enterprise}/repos | 创建企业仓库
[**postV5OrgsOrgRepos**](RepositoriesApi.md#postV5OrgsOrgRepos) | **POST** /v5/orgs/{org}/repos | 创建组织仓库
[**postV5ReposOwnerRepoBranches**](RepositoriesApi.md#postV5ReposOwnerRepoBranches) | **POST** /v5/repos/{owner}/{repo}/branches | 创建分支
[**postV5ReposOwnerRepoCommitsShaComments**](RepositoriesApi.md#postV5ReposOwnerRepoCommitsShaComments) | **POST** /v5/repos/{owner}/{repo}/commits/{sha}/comments | 创建Commit评论
[**postV5ReposOwnerRepoContentsPath**](RepositoriesApi.md#postV5ReposOwnerRepoContentsPath) | **POST** /v5/repos/{owner}/{repo}/contents/{path} | 新建文件
[**postV5ReposOwnerRepoForks**](RepositoriesApi.md#postV5ReposOwnerRepoForks) | **POST** /v5/repos/{owner}/{repo}/forks | Fork一个仓库
[**postV5ReposOwnerRepoKeys**](RepositoriesApi.md#postV5ReposOwnerRepoKeys) | **POST** /v5/repos/{owner}/{repo}/keys | 为仓库添加公钥
[**postV5ReposOwnerRepoPagesBuilds**](RepositoriesApi.md#postV5ReposOwnerRepoPagesBuilds) | **POST** /v5/repos/{owner}/{repo}/pages/builds | 请求建立Pages
[**postV5ReposOwnerRepoReleases**](RepositoriesApi.md#postV5ReposOwnerRepoReleases) | **POST** /v5/repos/{owner}/{repo}/releases | 创建仓库Release
[**postV5UserRepos**](RepositoriesApi.md#postV5UserRepos) | **POST** /v5/user/repos | 创建一个仓库
[**putV5ReposOwnerRepoBranchesBranchProtection**](RepositoriesApi.md#putV5ReposOwnerRepoBranchesBranchProtection) | **PUT** /v5/repos/{owner}/{repo}/branches/{branch}/protection | 设置分支保护
[**putV5ReposOwnerRepoClear**](RepositoriesApi.md#putV5ReposOwnerRepoClear) | **PUT** /v5/repos/{owner}/{repo}/clear | 清空一个仓库
[**putV5ReposOwnerRepoCollaboratorsUsername**](RepositoriesApi.md#putV5ReposOwnerRepoCollaboratorsUsername) | **PUT** /v5/repos/{owner}/{repo}/collaborators/{username} | 添加仓库成员
[**putV5ReposOwnerRepoContentsPath**](RepositoriesApi.md#putV5ReposOwnerRepoContentsPath) | **PUT** /v5/repos/{owner}/{repo}/contents/{path} | 更新文件
[**putV5ReposOwnerRepoKeysEnableId**](RepositoriesApi.md#putV5ReposOwnerRepoKeysEnableId) | **PUT** /v5/repos/{owner}/{repo}/keys/enable/{id} | 启用仓库公钥


<a name="deleteV5ReposOwnerRepo"></a>
# **deleteV5ReposOwnerRepo**
> deleteV5ReposOwnerRepo(owner, repo, accessToken)

删除一个仓库

删除一个仓库

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.deleteV5ReposOwnerRepo(owner, repo, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#deleteV5ReposOwnerRepo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="deleteV5ReposOwnerRepoBranchesBranchProtection"></a>
# **deleteV5ReposOwnerRepoBranchesBranchProtection**
> deleteV5ReposOwnerRepoBranchesBranchProtection(owner, repo, branch, accessToken)

取消保护分支的设置

取消保护分支的设置

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String branch = "branch_example"; // String | 分支名称
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.deleteV5ReposOwnerRepoBranchesBranchProtection(owner, repo, branch, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#deleteV5ReposOwnerRepoBranchesBranchProtection");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **branch** | **String**| 分支名称 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="deleteV5ReposOwnerRepoCollaboratorsUsername"></a>
# **deleteV5ReposOwnerRepoCollaboratorsUsername**
> deleteV5ReposOwnerRepoCollaboratorsUsername(owner, repo, username, accessToken)

移除仓库成员

移除仓库成员

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.deleteV5ReposOwnerRepoCollaboratorsUsername(owner, repo, username, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#deleteV5ReposOwnerRepoCollaboratorsUsername");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="deleteV5ReposOwnerRepoCommentsId"></a>
# **deleteV5ReposOwnerRepoCommentsId**
> deleteV5ReposOwnerRepoCommentsId(owner, repo, id, accessToken)

删除Commit评论

删除Commit评论

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | 评论的ID
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.deleteV5ReposOwnerRepoCommentsId(owner, repo, id, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#deleteV5ReposOwnerRepoCommentsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**| 评论的ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="deleteV5ReposOwnerRepoContentsPath"></a>
# **deleteV5ReposOwnerRepoContentsPath**
> CommitContent deleteV5ReposOwnerRepoContentsPath(owner, repo, path, sha, message, accessToken, branch, committerName, committerEmail, authorName, authorEmail)

删除文件

删除文件

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String path = "path_example"; // String | 文件的路径
String sha = "sha_example"; // String | 文件的 Blob SHA，可通过 [获取仓库具体路径下的内容] API 获取
String message = "message_example"; // String | 提交信息
String accessToken = "accessToken_example"; // String | 用户授权码
String branch = "branch_example"; // String | 分支名称。默认为仓库对默认分支
String committerName = "committerName_example"; // String | Committer的名字，默认为当前用户的名字
String committerEmail = "committerEmail_example"; // String | Committer的邮箱，默认为当前用户的邮箱
String authorName = "authorName_example"; // String | Author的名字，默认为当前用户的名字
String authorEmail = "authorEmail_example"; // String | Author的邮箱，默认为当前用户的邮箱
try {
    CommitContent result = apiInstance.deleteV5ReposOwnerRepoContentsPath(owner, repo, path, sha, message, accessToken, branch, committerName, committerEmail, authorName, authorEmail);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#deleteV5ReposOwnerRepoContentsPath");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **path** | **String**| 文件的路径 |
 **sha** | **String**| 文件的 Blob SHA，可通过 [获取仓库具体路径下的内容] API 获取 |
 **message** | **String**| 提交信息 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **branch** | **String**| 分支名称。默认为仓库对默认分支 | [optional]
 **committerName** | **String**| Committer的名字，默认为当前用户的名字 | [optional]
 **committerEmail** | **String**| Committer的邮箱，默认为当前用户的邮箱 | [optional]
 **authorName** | **String**| Author的名字，默认为当前用户的名字 | [optional]
 **authorEmail** | **String**| Author的邮箱，默认为当前用户的邮箱 | [optional]

### Return type

[**CommitContent**](CommitContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="deleteV5ReposOwnerRepoKeysEnableId"></a>
# **deleteV5ReposOwnerRepoKeysEnableId**
> deleteV5ReposOwnerRepoKeysEnableId(owner, repo, id, accessToken)

停用仓库公钥

停用仓库公钥

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | 公钥 ID
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.deleteV5ReposOwnerRepoKeysEnableId(owner, repo, id, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#deleteV5ReposOwnerRepoKeysEnableId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**| 公钥 ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="deleteV5ReposOwnerRepoKeysId"></a>
# **deleteV5ReposOwnerRepoKeysId**
> deleteV5ReposOwnerRepoKeysId(owner, repo, id, accessToken)

删除一个仓库公钥

删除一个仓库公钥

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | 公钥 ID
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.deleteV5ReposOwnerRepoKeysId(owner, repo, id, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#deleteV5ReposOwnerRepoKeysId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**| 公钥 ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="deleteV5ReposOwnerRepoReleasesId"></a>
# **deleteV5ReposOwnerRepoReleasesId**
> deleteV5ReposOwnerRepoReleasesId(owner, repo, id, accessToken)

删除仓库Release

删除仓库Release

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | 
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.deleteV5ReposOwnerRepoReleasesId(owner, repo, id, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#deleteV5ReposOwnerRepoReleasesId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**|  |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5EnterprisesEnterpriseRepos"></a>
# **getV5EnterprisesEnterpriseRepos**
> Project getV5EnterprisesEnterpriseRepos(enterprise, accessToken, type, direct, page, perPage)

获取企业的所有仓库

获取企业的所有仓库

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
String accessToken = "accessToken_example"; // String | 用户授权码
String type = "all"; // String | 筛选仓库的类型，可以是 all, public, internal, private。默认: all
Boolean direct = true; // Boolean | 只获取直属仓库，默认: false
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    Project result = apiInstance.getV5EnterprisesEnterpriseRepos(enterprise, accessToken, type, direct, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getV5EnterprisesEnterpriseRepos");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **type** | **String**| 筛选仓库的类型，可以是 all, public, internal, private。默认: all | [optional] [default to all] [enum: all, public, internal, private]
 **direct** | **Boolean**| 只获取直属仓库，默认: false | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**Project**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5OrgsOrgRepos"></a>
# **getV5OrgsOrgRepos**
> List&lt;Project&gt; getV5OrgsOrgRepos(org, accessToken, type, page, perPage)

获取一个组织的仓库

获取一个组织的仓库

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String org = "org_example"; // String | 组织的路径(path/login)
String accessToken = "accessToken_example"; // String | 用户授权码
String type = "all"; // String | 筛选仓库的类型，可以是 all, public, private。默认: all
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Project> result = apiInstance.getV5OrgsOrgRepos(org, accessToken, type, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getV5OrgsOrgRepos");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org** | **String**| 组织的路径(path/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **type** | **String**| 筛选仓库的类型，可以是 all, public, private。默认: all | [optional] [default to all] [enum: all, public, private]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;Project&gt;**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepo"></a>
# **getV5ReposOwnerRepo**
> Project getV5ReposOwnerRepo(owner, repo, accessToken)

获取用户的某个仓库

获取用户的某个仓库

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    Project result = apiInstance.getV5ReposOwnerRepo(owner, repo, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getV5ReposOwnerRepo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Project**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoBranches"></a>
# **getV5ReposOwnerRepoBranches**
> List&lt;Branch&gt; getV5ReposOwnerRepoBranches(owner, repo, accessToken)

获取所有分支

获取所有分支

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    List<Branch> result = apiInstance.getV5ReposOwnerRepoBranches(owner, repo, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getV5ReposOwnerRepoBranches");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**List&lt;Branch&gt;**](Branch.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoBranchesBranch"></a>
# **getV5ReposOwnerRepoBranchesBranch**
> CompleteBranch getV5ReposOwnerRepoBranchesBranch(owner, repo, branch, accessToken)

获取单个分支

获取单个分支

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String branch = "branch_example"; // String | 分支名称
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    CompleteBranch result = apiInstance.getV5ReposOwnerRepoBranchesBranch(owner, repo, branch, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getV5ReposOwnerRepoBranchesBranch");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **branch** | **String**| 分支名称 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**CompleteBranch**](CompleteBranch.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoCollaborators"></a>
# **getV5ReposOwnerRepoCollaborators**
> ProjectMember getV5ReposOwnerRepoCollaborators(owner, repo, accessToken)

获取仓库的所有成员

获取仓库的所有成员

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    ProjectMember result = apiInstance.getV5ReposOwnerRepoCollaborators(owner, repo, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getV5ReposOwnerRepoCollaborators");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**ProjectMember**](ProjectMember.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoCollaboratorsUsername"></a>
# **getV5ReposOwnerRepoCollaboratorsUsername**
> getV5ReposOwnerRepoCollaboratorsUsername(owner, repo, username, accessToken)

判断用户是否为仓库成员

判断用户是否为仓库成员

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.getV5ReposOwnerRepoCollaboratorsUsername(owner, repo, username, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getV5ReposOwnerRepoCollaboratorsUsername");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoCollaboratorsUsernamePermission"></a>
# **getV5ReposOwnerRepoCollaboratorsUsernamePermission**
> ProjectMemberPermission getV5ReposOwnerRepoCollaboratorsUsernamePermission(owner, repo, username, accessToken)

查看仓库成员的权限

查看仓库成员的权限

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    ProjectMemberPermission result = apiInstance.getV5ReposOwnerRepoCollaboratorsUsernamePermission(owner, repo, username, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getV5ReposOwnerRepoCollaboratorsUsernamePermission");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**ProjectMemberPermission**](ProjectMemberPermission.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoComments"></a>
# **getV5ReposOwnerRepoComments**
> Note getV5ReposOwnerRepoComments(owner, repo, accessToken, page, perPage)

获取仓库的Commit评论

获取仓库的Commit评论

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    Note result = apiInstance.getV5ReposOwnerRepoComments(owner, repo, accessToken, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getV5ReposOwnerRepoComments");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**Note**](Note.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoCommentsId"></a>
# **getV5ReposOwnerRepoCommentsId**
> Note getV5ReposOwnerRepoCommentsId(owner, repo, id, accessToken)

获取仓库的某条Commit评论

获取仓库的某条Commit评论

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | 评论的ID
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    Note result = apiInstance.getV5ReposOwnerRepoCommentsId(owner, repo, id, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getV5ReposOwnerRepoCommentsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**| 评论的ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Note**](Note.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoCommits"></a>
# **getV5ReposOwnerRepoCommits**
> List&lt;RepoCommit&gt; getV5ReposOwnerRepoCommits(owner, repo, accessToken, sha, path, author, since, until, page, perPage)

仓库的所有提交

仓库的所有提交

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
String sha = "sha_example"; // String | 提交起始的SHA值或者分支名. 默认: 仓库的默认分支
String path = "path_example"; // String | 包含该文件的提交
String author = "author_example"; // String | 提交作者的邮箱或个人空间地址(username/login)
String since = "since_example"; // String | 提交的起始时间，时间格式为 ISO 8601
String until = "until_example"; // String | 提交的最后时间，时间格式为 ISO 8601
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<RepoCommit> result = apiInstance.getV5ReposOwnerRepoCommits(owner, repo, accessToken, sha, path, author, since, until, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getV5ReposOwnerRepoCommits");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **sha** | **String**| 提交起始的SHA值或者分支名. 默认: 仓库的默认分支 | [optional]
 **path** | **String**| 包含该文件的提交 | [optional]
 **author** | **String**| 提交作者的邮箱或个人空间地址(username/login) | [optional]
 **since** | **String**| 提交的起始时间，时间格式为 ISO 8601 | [optional]
 **until** | **String**| 提交的最后时间，时间格式为 ISO 8601 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;RepoCommit&gt;**](RepoCommit.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoCommitsRefComments"></a>
# **getV5ReposOwnerRepoCommitsRefComments**
> Note getV5ReposOwnerRepoCommitsRefComments(owner, repo, ref, accessToken, page, perPage)

获取单个Commit的评论

获取单个Commit的评论

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String ref = "ref_example"; // String | Commit的Reference
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    Note result = apiInstance.getV5ReposOwnerRepoCommitsRefComments(owner, repo, ref, accessToken, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getV5ReposOwnerRepoCommitsRefComments");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **ref** | **String**| Commit的Reference |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**Note**](Note.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoCommitsSha"></a>
# **getV5ReposOwnerRepoCommitsSha**
> RepoCommit getV5ReposOwnerRepoCommitsSha(owner, repo, sha, accessToken)

仓库的某个提交

仓库的某个提交

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String sha = "sha_example"; // String | 提交的SHA值或者分支名
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    RepoCommit result = apiInstance.getV5ReposOwnerRepoCommitsSha(owner, repo, sha, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getV5ReposOwnerRepoCommitsSha");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **sha** | **String**| 提交的SHA值或者分支名 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**RepoCommit**](RepoCommit.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoCompareBaseHead"></a>
# **getV5ReposOwnerRepoCompareBaseHead**
> Compare getV5ReposOwnerRepoCompareBaseHead(owner, repo, base, head, accessToken)

两个Commits之间对比的版本差异

两个Commits之间对比的版本差异

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String base = "base_example"; // String | Commit提交的SHA值或者分支名作为对比起点
String head = "head_example"; // String | Commit提交的SHA值或者分支名作为对比终点
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    Compare result = apiInstance.getV5ReposOwnerRepoCompareBaseHead(owner, repo, base, head, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getV5ReposOwnerRepoCompareBaseHead");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **base** | **String**| Commit提交的SHA值或者分支名作为对比起点 |
 **head** | **String**| Commit提交的SHA值或者分支名作为对比终点 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Compare**](Compare.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoContentsPath"></a>
# **getV5ReposOwnerRepoContentsPath**
> Content getV5ReposOwnerRepoContentsPath(owner, repo, path, accessToken, ref)

获取仓库具体路径下的内容

获取仓库具体路径下的内容

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String path = "path_example"; // String | 文件的路径
String accessToken = "accessToken_example"; // String | 用户授权码
String ref = "ref_example"; // String | 分支、tag或commit。默认: 仓库的默认分支(通常是master)
try {
    Content result = apiInstance.getV5ReposOwnerRepoContentsPath(owner, repo, path, accessToken, ref);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getV5ReposOwnerRepoContentsPath");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **path** | **String**| 文件的路径 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **ref** | **String**| 分支、tag或commit。默认: 仓库的默认分支(通常是master) | [optional]

### Return type

[**Content**](Content.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoContributors"></a>
# **getV5ReposOwnerRepoContributors**
> Contributor getV5ReposOwnerRepoContributors(owner, repo, accessToken)

获取仓库贡献者

获取仓库贡献者

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    Contributor result = apiInstance.getV5ReposOwnerRepoContributors(owner, repo, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getV5ReposOwnerRepoContributors");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Contributor**](Contributor.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoForks"></a>
# **getV5ReposOwnerRepoForks**
> Project getV5ReposOwnerRepoForks(owner, repo, accessToken, sort, page, perPage)

查看仓库的Forks

查看仓库的Forks

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
String sort = "newest"; // String | 排序方式: fork的时间(newest, oldest)，star的人数(stargazers)
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    Project result = apiInstance.getV5ReposOwnerRepoForks(owner, repo, accessToken, sort, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getV5ReposOwnerRepoForks");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **sort** | **String**| 排序方式: fork的时间(newest, oldest)，star的人数(stargazers) | [optional] [default to newest] [enum: newest, oldest, stargazers]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**Project**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoKeys"></a>
# **getV5ReposOwnerRepoKeys**
> List&lt;SSHKey&gt; getV5ReposOwnerRepoKeys(owner, repo, accessToken, page, perPage)

获取仓库已部署的公钥

获取仓库已部署的公钥

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<SSHKey> result = apiInstance.getV5ReposOwnerRepoKeys(owner, repo, accessToken, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getV5ReposOwnerRepoKeys");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;SSHKey&gt;**](SSHKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoKeysAvailable"></a>
# **getV5ReposOwnerRepoKeysAvailable**
> List&lt;SSHKeyBasic&gt; getV5ReposOwnerRepoKeysAvailable(owner, repo, accessToken, page, perPage)

获取仓库可部署的公钥

获取仓库可部署的公钥

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<SSHKeyBasic> result = apiInstance.getV5ReposOwnerRepoKeysAvailable(owner, repo, accessToken, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getV5ReposOwnerRepoKeysAvailable");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;SSHKeyBasic&gt;**](SSHKeyBasic.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoKeysId"></a>
# **getV5ReposOwnerRepoKeysId**
> SSHKey getV5ReposOwnerRepoKeysId(owner, repo, id, accessToken)

获取仓库的单个公钥

获取仓库的单个公钥

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | 公钥 ID
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    SSHKey result = apiInstance.getV5ReposOwnerRepoKeysId(owner, repo, id, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getV5ReposOwnerRepoKeysId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**| 公钥 ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**SSHKey**](SSHKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoPages"></a>
# **getV5ReposOwnerRepoPages**
> getV5ReposOwnerRepoPages(owner, repo, accessToken)

获取Pages信息

获取Pages信息

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.getV5ReposOwnerRepoPages(owner, repo, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getV5ReposOwnerRepoPages");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoReadme"></a>
# **getV5ReposOwnerRepoReadme**
> Content getV5ReposOwnerRepoReadme(owner, repo, accessToken, ref)

获取仓库README

获取仓库README

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
String ref = "ref_example"; // String | 分支、tag或commit。默认: 仓库的默认分支(通常是master)
try {
    Content result = apiInstance.getV5ReposOwnerRepoReadme(owner, repo, accessToken, ref);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getV5ReposOwnerRepoReadme");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **ref** | **String**| 分支、tag或commit。默认: 仓库的默认分支(通常是master) | [optional]

### Return type

[**Content**](Content.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoReleases"></a>
# **getV5ReposOwnerRepoReleases**
> List&lt;Release&gt; getV5ReposOwnerRepoReleases(owner, repo, accessToken, page, perPage)

获取仓库的所有Releases

获取仓库的所有Releases

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Release> result = apiInstance.getV5ReposOwnerRepoReleases(owner, repo, accessToken, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getV5ReposOwnerRepoReleases");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;Release&gt;**](Release.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoReleasesId"></a>
# **getV5ReposOwnerRepoReleasesId**
> Release getV5ReposOwnerRepoReleasesId(owner, repo, id, accessToken)

获取仓库的单个Releases

获取仓库的单个Releases

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | 发行版本的ID
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    Release result = apiInstance.getV5ReposOwnerRepoReleasesId(owner, repo, id, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getV5ReposOwnerRepoReleasesId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**| 发行版本的ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Release**](Release.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoReleasesLatest"></a>
# **getV5ReposOwnerRepoReleasesLatest**
> Release getV5ReposOwnerRepoReleasesLatest(owner, repo, accessToken)

获取仓库的最后更新的Release

获取仓库的最后更新的Release

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    Release result = apiInstance.getV5ReposOwnerRepoReleasesLatest(owner, repo, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getV5ReposOwnerRepoReleasesLatest");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Release**](Release.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoReleasesTagsTag"></a>
# **getV5ReposOwnerRepoReleasesTagsTag**
> Release getV5ReposOwnerRepoReleasesTagsTag(owner, repo, tag, accessToken)

根据Tag名称获取仓库的Release

根据Tag名称获取仓库的Release

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String tag = "tag_example"; // String | Tag 名称
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    Release result = apiInstance.getV5ReposOwnerRepoReleasesTagsTag(owner, repo, tag, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getV5ReposOwnerRepoReleasesTagsTag");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **tag** | **String**| Tag 名称 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Release**](Release.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoTags"></a>
# **getV5ReposOwnerRepoTags**
> Tag getV5ReposOwnerRepoTags(owner, repo, accessToken)

列出仓库所有的tags

列出仓库所有的tags

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    Tag result = apiInstance.getV5ReposOwnerRepoTags(owner, repo, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getV5ReposOwnerRepoTags");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Tag**](Tag.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5UserRepos"></a>
# **getV5UserRepos**
> Project getV5UserRepos(accessToken, visibility, affiliation, type, sort, direction, page, perPage)

列出授权用户的所有仓库

列出授权用户的所有仓库

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String accessToken = "accessToken_example"; // String | 用户授权码
String visibility = "visibility_example"; // String | 公开(public)、私有(private)或者所有(all)，默认: 所有(all)
String affiliation = "affiliation_example"; // String | owner(授权用户拥有的仓库)、collaborator(授权用户为仓库成员)、organization_member(授权用户为仓库所在组织并有访问仓库权限)、enterprise_member(授权用户所在企业并有访问仓库权限)、admin(所有有权限的，包括所管理的组织中所有仓库、所管理的企业的所有仓库)。                    可以用逗号分隔符组合。如: owner, organization_member 或 owner, collaborator, organization_member
String type = "type_example"; // String | 筛选用户仓库: 其创建(owner)、个人(personal)、其为成员(member)、公开(public)、私有(private)，不能与 visibility 或 affiliation 参数一并使用，否则会报 422 错误
String sort = "full_name"; // String | 排序方式: 创建时间(created)，更新时间(updated)，最后推送时间(pushed)，仓库所属与名称(full_name)。默认: full_name
String direction = "direction_example"; // String | 如果sort参数为full_name，用升序(asc)。否则降序(desc)
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    Project result = apiInstance.getV5UserRepos(accessToken, visibility, affiliation, type, sort, direction, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getV5UserRepos");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]
 **visibility** | **String**| 公开(public)、私有(private)或者所有(all)，默认: 所有(all) | [optional] [enum: private, public, all]
 **affiliation** | **String**| owner(授权用户拥有的仓库)、collaborator(授权用户为仓库成员)、organization_member(授权用户为仓库所在组织并有访问仓库权限)、enterprise_member(授权用户所在企业并有访问仓库权限)、admin(所有有权限的，包括所管理的组织中所有仓库、所管理的企业的所有仓库)。                    可以用逗号分隔符组合。如: owner, organization_member 或 owner, collaborator, organization_member | [optional]
 **type** | **String**| 筛选用户仓库: 其创建(owner)、个人(personal)、其为成员(member)、公开(public)、私有(private)，不能与 visibility 或 affiliation 参数一并使用，否则会报 422 错误 | [optional] [enum: all, owner, personal, member, public, private]
 **sort** | **String**| 排序方式: 创建时间(created)，更新时间(updated)，最后推送时间(pushed)，仓库所属与名称(full_name)。默认: full_name | [optional] [default to full_name] [enum: created, updated, pushed, full_name]
 **direction** | **String**| 如果sort参数为full_name，用升序(asc)。否则降序(desc) | [optional] [enum: asc, desc]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**Project**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5UsersUsernameRepos"></a>
# **getV5UsersUsernameRepos**
> Project getV5UsersUsernameRepos(username, accessToken, type, sort, direction, page, perPage)

获取某个用户的公开仓库

获取某个用户的公开仓库

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String username = "username_example"; // String | 用户名(username/login)
String accessToken = "accessToken_example"; // String | 用户授权码
String type = "all"; // String | 用户创建的仓库(owner)，用户个人仓库(personal)，用户为仓库成员(member)，所有(all)。默认: 所有(all)
String sort = "full_name"; // String | 排序方式: 创建时间(created)，更新时间(updated)，最后推送时间(pushed)，仓库所属与名称(full_name)。默认: full_name
String direction = "direction_example"; // String | 如果sort参数为full_name，用升序(asc)。否则降序(desc)
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    Project result = apiInstance.getV5UsersUsernameRepos(username, accessToken, type, sort, direction, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#getV5UsersUsernameRepos");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **String**| 用户名(username/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **type** | **String**| 用户创建的仓库(owner)，用户个人仓库(personal)，用户为仓库成员(member)，所有(all)。默认: 所有(all) | [optional] [default to all] [enum: all, owner, personal, member]
 **sort** | **String**| 排序方式: 创建时间(created)，更新时间(updated)，最后推送时间(pushed)，仓库所属与名称(full_name)。默认: full_name | [optional] [default to full_name] [enum: created, updated, pushed, full_name]
 **direction** | **String**| 如果sort参数为full_name，用升序(asc)。否则降序(desc) | [optional] [enum: asc, desc]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**Project**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="patchV5ReposOwnerRepo"></a>
# **patchV5ReposOwnerRepo**
> Project patchV5ReposOwnerRepo(owner, repo, name, accessToken, description, homepage, hasIssues, hasWiki, _private, defaultBranch)

更新仓库设置

更新仓库设置

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String name = "name_example"; // String | 仓库名称
String accessToken = "accessToken_example"; // String | 用户授权码
String description = "description_example"; // String | 仓库描述
String homepage = "homepage_example"; // String | 主页(eg: https://gitee.com)
Boolean hasIssues = true; // Boolean | 允许提Issue与否。默认: 允许(true)
Boolean hasWiki = true; // Boolean | 提供Wiki与否。默认: 提供(true)
Boolean _private = true; // Boolean | 仓库公开或私有。
String defaultBranch = "defaultBranch_example"; // String | 更新默认分支
try {
    Project result = apiInstance.patchV5ReposOwnerRepo(owner, repo, name, accessToken, description, homepage, hasIssues, hasWiki, _private, defaultBranch);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#patchV5ReposOwnerRepo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **name** | **String**| 仓库名称 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **description** | **String**| 仓库描述 | [optional]
 **homepage** | **String**| 主页(eg: https://gitee.com) | [optional]
 **hasIssues** | **Boolean**| 允许提Issue与否。默认: 允许(true) | [optional] [default to true]
 **hasWiki** | **Boolean**| 提供Wiki与否。默认: 提供(true) | [optional] [default to true]
 **_private** | **Boolean**| 仓库公开或私有。 | [optional]
 **defaultBranch** | **String**| 更新默认分支 | [optional]

### Return type

[**Project**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="patchV5ReposOwnerRepoCommentsId"></a>
# **patchV5ReposOwnerRepoCommentsId**
> Note patchV5ReposOwnerRepoCommentsId(owner, repo, id, body, accessToken)

更新Commit评论

更新Commit评论

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | 评论的ID
String body = "body_example"; // String | 评论的内容
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    Note result = apiInstance.patchV5ReposOwnerRepoCommentsId(owner, repo, id, body, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#patchV5ReposOwnerRepoCommentsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**| 评论的ID |
 **body** | **String**| 评论的内容 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Note**](Note.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="patchV5ReposOwnerRepoReleasesId"></a>
# **patchV5ReposOwnerRepoReleasesId**
> Release patchV5ReposOwnerRepoReleasesId(owner, repo, tagName, name, body, id, accessToken, prerelease)

更新仓库Release

更新仓库Release

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String tagName = "tagName_example"; // String | Tag 名称, 提倡以v字母为前缀做为Release名称，例如v1.0或者v2.3.4
String name = "name_example"; // String | Release 名称
String body = "body_example"; // String | Release 描述
Integer id = 56; // Integer | 
String accessToken = "accessToken_example"; // String | 用户授权码
Boolean prerelease = true; // Boolean | 是否为预览版本。默认: false（非预览版本）
try {
    Release result = apiInstance.patchV5ReposOwnerRepoReleasesId(owner, repo, tagName, name, body, id, accessToken, prerelease);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#patchV5ReposOwnerRepoReleasesId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **tagName** | **String**| Tag 名称, 提倡以v字母为前缀做为Release名称，例如v1.0或者v2.3.4 |
 **name** | **String**| Release 名称 |
 **body** | **String**| Release 描述 |
 **id** | **Integer**|  |
 **accessToken** | **String**| 用户授权码 | [optional]
 **prerelease** | **Boolean**| 是否为预览版本。默认: false（非预览版本） | [optional]

### Return type

[**Release**](Release.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="postV5EnterprisesEnterpriseRepos"></a>
# **postV5EnterprisesEnterpriseRepos**
> Project postV5EnterprisesEnterpriseRepos(name, enterprise, accessToken, description, homepage, hasIssues, hasWiki, autoInit, gitignoreTemplate, licenseTemplate, _private, outsourced, projectCreator, members)

创建企业仓库

创建企业仓库

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String name = "name_example"; // String | 仓库名称
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
String accessToken = "accessToken_example"; // String | 用户授权码
String description = "description_example"; // String | 仓库描述
String homepage = "homepage_example"; // String | 主页(eg: https://gitee.com)
Boolean hasIssues = true; // Boolean | 允许提Issue与否。默认: 允许(true)
Boolean hasWiki = true; // Boolean | 提供Wiki与否。默认: 提供(true)
Boolean autoInit = true; // Boolean | 值为true时则会用README初始化仓库。默认: 不初始化(false)
String gitignoreTemplate = "gitignoreTemplate_example"; // String | Git Ingore模版
String licenseTemplate = "licenseTemplate_example"; // String | License模版
Integer _private = 0; // Integer | 仓库开源类型。0(私有), 1(外部开源), 2(内部开源)。默认: 0
Boolean outsourced = true; // Boolean | 值为true值为外包仓库, false值为内部仓库。默认: 内部仓库(false)
String projectCreator = "projectCreator_example"; // String | 负责人的username
String members = "members_example"; // String | 用逗号分开的仓库成员。如: member1,member2
try {
    Project result = apiInstance.postV5EnterprisesEnterpriseRepos(name, enterprise, accessToken, description, homepage, hasIssues, hasWiki, autoInit, gitignoreTemplate, licenseTemplate, _private, outsourced, projectCreator, members);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#postV5EnterprisesEnterpriseRepos");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| 仓库名称 |
 **enterprise** | **String**| 企业的路径(path/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **description** | **String**| 仓库描述 | [optional]
 **homepage** | **String**| 主页(eg: https://gitee.com) | [optional]
 **hasIssues** | **Boolean**| 允许提Issue与否。默认: 允许(true) | [optional] [default to true]
 **hasWiki** | **Boolean**| 提供Wiki与否。默认: 提供(true) | [optional] [default to true]
 **autoInit** | **Boolean**| 值为true时则会用README初始化仓库。默认: 不初始化(false) | [optional]
 **gitignoreTemplate** | **String**| Git Ingore模版 | [optional] [enum: Actionscript, Ada, Agda, Android, AppEngine, AppceleratorTitanium, ArchLinuxPackages, Autotools, C, C++, CFWheels, CMake, CUDA, CakePHP, ChefCookbook, Clojure, CodeIgniter, CommonLisp, Composer, Concrete5, Coq, CraftCMS, D, DM, Dart, Delphi, Drupal, EPiServer, Eagle, Elisp, Elixir, Elm, Erlang, ExpressionEngine, ExtJs, Fancy, Finale, Flutter, ForceDotCom, Fortran, FuelPHP, GWT, Gcov, GitBook, Global/Anjuta, Global/Ansible, Global/Archives, Global/Backup, Global/Bazaar, Global/BricxCC, Global/CVS, Global/Calabash, Global/Cloud9, Global/CodeKit, Global/DartEditor, Global/Diff, Global/Dreamweaver, Global/Dropbox, Global/Eclipse, Global/EiffelStudio, Global/Emacs, Global/Ensime, Global/Espresso, Global/FlexBuilder, Global/GPG, Global/Images, Global/JDeveloper, Global/JEnv, Global/JetBrains, Global/KDevelop4, Global/Kate, Global/Lazarus, Global/LibreOffice, Global/Linux, Global/LyX, Global/MATLAB, Global/Mercurial, Global/MicrosoftOffice, Global/ModelSim, Global/Momentics, Global/MonoDevelop, Global/NetBeans, Global/Ninja, Global/NotepadPP, Global/Octave, Global/Otto, Global/PSoCCreator, Global/Patch, Global/PuTTY, Global/Redcar, Global/Redis, Global/SBT, Global/SVN, Global/SlickEdit, Global/Stata, Global/SublimeText, Global/SynopsysVCS, Global/Tags, Global/TextMate, Global/TortoiseGit, Global/Vagrant, Global/Vim, Global/VirtualEnv, Global/Virtuoso, Global/VisualStudioCode, Global/WebMethods, Global/Windows, Global/Xcode, Global/XilinxISE, Global/macOS, Go, Godot, Gradle, Grails, Haskell, IGORPro, Idris, JBoss, Java, Jekyll, Joomla, Julia, KiCad, Kohana, Kotlin, LabVIEW, Laravel, Leiningen, LemonStand, Lilypond, Lithium, Lua, Magento, Maven, Mercury, MetaProgrammingSystem, MiniProgram, Nanoc, Nim, Node, OCaml, Objective-C, Opa, OpenCart, OracleForms, Packer, Perl, Perl6, Phalcon, PlayFramework, Plone, Prestashop, Processing, PureScript, Python, Qooxdoo, Qt, R, ROS, Rails, RhodesRhomobile, Ruby, Rust, SCons, Sass, Scala, Scheme, Scrivener, Sdcc, SeamGen, SketchUp, Smalltalk, Stella, SugarCRM, Swift, Symfony, SymphonyCMS, TeX, Terraform, Textpattern, TurboGears2, Typo3, Umbraco, Unity, UnrealEngine, VVVV, VisualStudio, Waf, WordPress, Xojo, Yeoman, Yii, ZendFramework, Zephir]
 **licenseTemplate** | **String**| License模版 | [optional] [enum: MulanPSL-1.0, AFL-3.0, AGPL-3.0, Apache-2.0, Artistic-2.0, BSD-2-Clause, BSD-3-Clause, BSD-3-Clause-Clear, BSL-1.0, CC-BY-4.0, CC-BY-SA-4.0, CC0-1.0, ECL-2.0, EPL-1.0, EUPL-1.1, GPL-2.0, GPL-3.0, ISC, LGPL-2.1, LGPL-3.0, LPPL-1.3c, MIT, MPL-2.0, MS-PL, MS-RL, NCSA, OFL-1.1, OSL-3.0, PostgreSQL, Unlicense, WTFPL, Zlib]
 **_private** | **Integer**| 仓库开源类型。0(私有), 1(外部开源), 2(内部开源)。默认: 0 | [optional] [default to 0] [enum: 0, 1, 2]
 **outsourced** | **Boolean**| 值为true值为外包仓库, false值为内部仓库。默认: 内部仓库(false) | [optional]
 **projectCreator** | **String**| 负责人的username | [optional]
 **members** | **String**| 用逗号分开的仓库成员。如: member1,member2 | [optional]

### Return type

[**Project**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="postV5OrgsOrgRepos"></a>
# **postV5OrgsOrgRepos**
> Project postV5OrgsOrgRepos(org, body)

创建组织仓库

创建组织仓库

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String org = "org_example"; // String | 组织的路径(path/login)
RepositoryPostParam body = new RepositoryPostParam(); // RepositoryPostParam | Repositorie 内容
try {
    Project result = apiInstance.postV5OrgsOrgRepos(org, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#postV5OrgsOrgRepos");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org** | **String**| 组织的路径(path/login) |
 **body** | [**RepositoryPostParam**](RepositoryPostParam.md)| Repositorie 内容 |

### Return type

[**Project**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="postV5ReposOwnerRepoBranches"></a>
# **postV5ReposOwnerRepoBranches**
> CompleteBranch postV5ReposOwnerRepoBranches(owner, repo, refs, branchName, accessToken)

创建分支

创建分支

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String refs = "master"; // String | 起点名称, 默认：master
String branchName = "branchName_example"; // String | 新创建的分支名称
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    CompleteBranch result = apiInstance.postV5ReposOwnerRepoBranches(owner, repo, refs, branchName, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#postV5ReposOwnerRepoBranches");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **refs** | **String**| 起点名称, 默认：master | [default to master]
 **branchName** | **String**| 新创建的分支名称 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**CompleteBranch**](CompleteBranch.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="postV5ReposOwnerRepoCommitsShaComments"></a>
# **postV5ReposOwnerRepoCommitsShaComments**
> Note postV5ReposOwnerRepoCommitsShaComments(owner, repo, sha, body, accessToken, path, position)

创建Commit评论

创建Commit评论

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String sha = "sha_example"; // String | 评论的sha值
String body = "body_example"; // String | 评论的内容
String accessToken = "accessToken_example"; // String | 用户授权码
String path = "path_example"; // String | 文件的相对路径
Integer position = 56; // Integer | Diff的相对行数
try {
    Note result = apiInstance.postV5ReposOwnerRepoCommitsShaComments(owner, repo, sha, body, accessToken, path, position);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#postV5ReposOwnerRepoCommitsShaComments");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **sha** | **String**| 评论的sha值 |
 **body** | **String**| 评论的内容 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **path** | **String**| 文件的相对路径 | [optional]
 **position** | **Integer**| Diff的相对行数 | [optional]

### Return type

[**Note**](Note.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="postV5ReposOwnerRepoContentsPath"></a>
# **postV5ReposOwnerRepoContentsPath**
> CommitContent postV5ReposOwnerRepoContentsPath(owner, repo, path, content, message, accessToken, branch, committerName, committerEmail, authorName, authorEmail)

新建文件

新建文件

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String path = "path_example"; // String | 文件的路径
String content = "content_example"; // String | 文件内容, 要用 base64 编码
String message = "message_example"; // String | 提交信息
String accessToken = "accessToken_example"; // String | 用户授权码
String branch = "branch_example"; // String | 分支名称。默认为仓库对默认分支
String committerName = "committerName_example"; // String | Committer的名字，默认为当前用户的名字
String committerEmail = "committerEmail_example"; // String | Committer的邮箱，默认为当前用户的邮箱
String authorName = "authorName_example"; // String | Author的名字，默认为当前用户的名字
String authorEmail = "authorEmail_example"; // String | Author的邮箱，默认为当前用户的邮箱
try {
    CommitContent result = apiInstance.postV5ReposOwnerRepoContentsPath(owner, repo, path, content, message, accessToken, branch, committerName, committerEmail, authorName, authorEmail);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#postV5ReposOwnerRepoContentsPath");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **path** | **String**| 文件的路径 |
 **content** | **String**| 文件内容, 要用 base64 编码 |
 **message** | **String**| 提交信息 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **branch** | **String**| 分支名称。默认为仓库对默认分支 | [optional]
 **committerName** | **String**| Committer的名字，默认为当前用户的名字 | [optional]
 **committerEmail** | **String**| Committer的邮箱，默认为当前用户的邮箱 | [optional]
 **authorName** | **String**| Author的名字，默认为当前用户的名字 | [optional]
 **authorEmail** | **String**| Author的邮箱，默认为当前用户的邮箱 | [optional]

### Return type

[**CommitContent**](CommitContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="postV5ReposOwnerRepoForks"></a>
# **postV5ReposOwnerRepoForks**
> Project postV5ReposOwnerRepoForks(owner, repo, accessToken, organization)

Fork一个仓库

Fork一个仓库

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
String organization = "organization_example"; // String | 组织空间地址，不填写默认Fork到用户个人空间地址
try {
    Project result = apiInstance.postV5ReposOwnerRepoForks(owner, repo, accessToken, organization);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#postV5ReposOwnerRepoForks");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **organization** | **String**| 组织空间地址，不填写默认Fork到用户个人空间地址 | [optional]

### Return type

[**Project**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="postV5ReposOwnerRepoKeys"></a>
# **postV5ReposOwnerRepoKeys**
> SSHKey postV5ReposOwnerRepoKeys(owner, repo, key, title, accessToken)

为仓库添加公钥

为仓库添加公钥

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String key = "key_example"; // String | 公钥内容
String title = "title_example"; // String | 公钥名称
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    SSHKey result = apiInstance.postV5ReposOwnerRepoKeys(owner, repo, key, title, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#postV5ReposOwnerRepoKeys");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **key** | **String**| 公钥内容 |
 **title** | **String**| 公钥名称 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**SSHKey**](SSHKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="postV5ReposOwnerRepoPagesBuilds"></a>
# **postV5ReposOwnerRepoPagesBuilds**
> postV5ReposOwnerRepoPagesBuilds(owner, repo, accessToken)

请求建立Pages

请求建立Pages

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.postV5ReposOwnerRepoPagesBuilds(owner, repo, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#postV5ReposOwnerRepoPagesBuilds");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="postV5ReposOwnerRepoReleases"></a>
# **postV5ReposOwnerRepoReleases**
> Release postV5ReposOwnerRepoReleases(owner, repo, tagName, name, body, targetCommitish, accessToken, prerelease)

创建仓库Release

创建仓库Release

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String tagName = "tagName_example"; // String | Tag 名称, 提倡以v字母为前缀做为Release名称，例如v1.0或者v2.3.4
String name = "name_example"; // String | Release 名称
String body = "body_example"; // String | Release 描述
String targetCommitish = "targetCommitish_example"; // String | 分支名称或者commit SHA, 默认是当前默认分支
String accessToken = "accessToken_example"; // String | 用户授权码
Boolean prerelease = true; // Boolean | 是否为预览版本。默认: false（非预览版本）
try {
    Release result = apiInstance.postV5ReposOwnerRepoReleases(owner, repo, tagName, name, body, targetCommitish, accessToken, prerelease);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#postV5ReposOwnerRepoReleases");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **tagName** | **String**| Tag 名称, 提倡以v字母为前缀做为Release名称，例如v1.0或者v2.3.4 |
 **name** | **String**| Release 名称 |
 **body** | **String**| Release 描述 |
 **targetCommitish** | **String**| 分支名称或者commit SHA, 默认是当前默认分支 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **prerelease** | **Boolean**| 是否为预览版本。默认: false（非预览版本） | [optional]

### Return type

[**Release**](Release.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="postV5UserRepos"></a>
# **postV5UserRepos**
> Project postV5UserRepos(name, accessToken, description, homepage, hasIssues, hasWiki, autoInit, gitignoreTemplate, licenseTemplate, _private)

创建一个仓库

创建一个仓库

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String name = "name_example"; // String | 仓库名称
String accessToken = "accessToken_example"; // String | 用户授权码
String description = "description_example"; // String | 仓库描述
String homepage = "homepage_example"; // String | 主页(eg: https://gitee.com)
Boolean hasIssues = true; // Boolean | 允许提Issue与否。默认: 允许(true)
Boolean hasWiki = true; // Boolean | 提供Wiki与否。默认: 提供(true)
Boolean autoInit = true; // Boolean | 值为true时则会用README初始化仓库。默认: 不初始化(false)
String gitignoreTemplate = "gitignoreTemplate_example"; // String | Git Ingore模版
String licenseTemplate = "licenseTemplate_example"; // String | License模版
Boolean _private = true; // Boolean | 仓库公开或私有。默认: 公开(false)
try {
    Project result = apiInstance.postV5UserRepos(name, accessToken, description, homepage, hasIssues, hasWiki, autoInit, gitignoreTemplate, licenseTemplate, _private);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#postV5UserRepos");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| 仓库名称 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **description** | **String**| 仓库描述 | [optional]
 **homepage** | **String**| 主页(eg: https://gitee.com) | [optional]
 **hasIssues** | **Boolean**| 允许提Issue与否。默认: 允许(true) | [optional] [default to true]
 **hasWiki** | **Boolean**| 提供Wiki与否。默认: 提供(true) | [optional] [default to true]
 **autoInit** | **Boolean**| 值为true时则会用README初始化仓库。默认: 不初始化(false) | [optional]
 **gitignoreTemplate** | **String**| Git Ingore模版 | [optional] [enum: Actionscript, Ada, Agda, Android, AppEngine, AppceleratorTitanium, ArchLinuxPackages, Autotools, C, C++, CFWheels, CMake, CUDA, CakePHP, ChefCookbook, Clojure, CodeIgniter, CommonLisp, Composer, Concrete5, Coq, CraftCMS, D, DM, Dart, Delphi, Drupal, EPiServer, Eagle, Elisp, Elixir, Elm, Erlang, ExpressionEngine, ExtJs, Fancy, Finale, Flutter, ForceDotCom, Fortran, FuelPHP, GWT, Gcov, GitBook, Global/Anjuta, Global/Ansible, Global/Archives, Global/Backup, Global/Bazaar, Global/BricxCC, Global/CVS, Global/Calabash, Global/Cloud9, Global/CodeKit, Global/DartEditor, Global/Diff, Global/Dreamweaver, Global/Dropbox, Global/Eclipse, Global/EiffelStudio, Global/Emacs, Global/Ensime, Global/Espresso, Global/FlexBuilder, Global/GPG, Global/Images, Global/JDeveloper, Global/JEnv, Global/JetBrains, Global/KDevelop4, Global/Kate, Global/Lazarus, Global/LibreOffice, Global/Linux, Global/LyX, Global/MATLAB, Global/Mercurial, Global/MicrosoftOffice, Global/ModelSim, Global/Momentics, Global/MonoDevelop, Global/NetBeans, Global/Ninja, Global/NotepadPP, Global/Octave, Global/Otto, Global/PSoCCreator, Global/Patch, Global/PuTTY, Global/Redcar, Global/Redis, Global/SBT, Global/SVN, Global/SlickEdit, Global/Stata, Global/SublimeText, Global/SynopsysVCS, Global/Tags, Global/TextMate, Global/TortoiseGit, Global/Vagrant, Global/Vim, Global/VirtualEnv, Global/Virtuoso, Global/VisualStudioCode, Global/WebMethods, Global/Windows, Global/Xcode, Global/XilinxISE, Global/macOS, Go, Godot, Gradle, Grails, Haskell, IGORPro, Idris, JBoss, Java, Jekyll, Joomla, Julia, KiCad, Kohana, Kotlin, LabVIEW, Laravel, Leiningen, LemonStand, Lilypond, Lithium, Lua, Magento, Maven, Mercury, MetaProgrammingSystem, MiniProgram, Nanoc, Nim, Node, OCaml, Objective-C, Opa, OpenCart, OracleForms, Packer, Perl, Perl6, Phalcon, PlayFramework, Plone, Prestashop, Processing, PureScript, Python, Qooxdoo, Qt, R, ROS, Rails, RhodesRhomobile, Ruby, Rust, SCons, Sass, Scala, Scheme, Scrivener, Sdcc, SeamGen, SketchUp, Smalltalk, Stella, SugarCRM, Swift, Symfony, SymphonyCMS, TeX, Terraform, Textpattern, TurboGears2, Typo3, Umbraco, Unity, UnrealEngine, VVVV, VisualStudio, Waf, WordPress, Xojo, Yeoman, Yii, ZendFramework, Zephir]
 **licenseTemplate** | **String**| License模版 | [optional] [enum: MulanPSL-1.0, AFL-3.0, AGPL-3.0, Apache-2.0, Artistic-2.0, BSD-2-Clause, BSD-3-Clause, BSD-3-Clause-Clear, BSL-1.0, CC-BY-4.0, CC-BY-SA-4.0, CC0-1.0, ECL-2.0, EPL-1.0, EUPL-1.1, GPL-2.0, GPL-3.0, ISC, LGPL-2.1, LGPL-3.0, LPPL-1.3c, MIT, MPL-2.0, MS-PL, MS-RL, NCSA, OFL-1.1, OSL-3.0, PostgreSQL, Unlicense, WTFPL, Zlib]
 **_private** | **Boolean**| 仓库公开或私有。默认: 公开(false) | [optional]

### Return type

[**Project**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="putV5ReposOwnerRepoBranchesBranchProtection"></a>
# **putV5ReposOwnerRepoBranchesBranchProtection**
> CompleteBranch putV5ReposOwnerRepoBranchesBranchProtection(owner, repo, branch, accessToken)

设置分支保护

设置分支保护

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String branch = "branch_example"; // String | 分支名称
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    CompleteBranch result = apiInstance.putV5ReposOwnerRepoBranchesBranchProtection(owner, repo, branch, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#putV5ReposOwnerRepoBranchesBranchProtection");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **branch** | **String**| 分支名称 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**CompleteBranch**](CompleteBranch.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="putV5ReposOwnerRepoClear"></a>
# **putV5ReposOwnerRepoClear**
> putV5ReposOwnerRepoClear(owner, repo, accessToken)

清空一个仓库

清空一个仓库

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.putV5ReposOwnerRepoClear(owner, repo, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#putV5ReposOwnerRepoClear");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="putV5ReposOwnerRepoCollaboratorsUsername"></a>
# **putV5ReposOwnerRepoCollaboratorsUsername**
> ProjectMember putV5ReposOwnerRepoCollaboratorsUsername(owner, repo, username, body)

添加仓库成员

添加仓库成员

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String username = "username_example"; // String | 用户名(username/login)
ProjectMemberPutParam body = new ProjectMemberPutParam(); // ProjectMemberPutParam | 仓库成员内容
try {
    ProjectMember result = apiInstance.putV5ReposOwnerRepoCollaboratorsUsername(owner, repo, username, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#putV5ReposOwnerRepoCollaboratorsUsername");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **username** | **String**| 用户名(username/login) |
 **body** | [**ProjectMemberPutParam**](ProjectMemberPutParam.md)| 仓库成员内容 |

### Return type

[**ProjectMember**](ProjectMember.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="putV5ReposOwnerRepoContentsPath"></a>
# **putV5ReposOwnerRepoContentsPath**
> CommitContent putV5ReposOwnerRepoContentsPath(owner, repo, path, content, sha, message, accessToken, branch, committerName, committerEmail, authorName, authorEmail)

更新文件

更新文件

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String path = "path_example"; // String | 文件的路径
String content = "content_example"; // String | 文件内容, 要用 base64 编码
String sha = "sha_example"; // String | 文件的 Blob SHA，可通过 [获取仓库具体路径下的内容] API 获取
String message = "message_example"; // String | 提交信息
String accessToken = "accessToken_example"; // String | 用户授权码
String branch = "branch_example"; // String | 分支名称。默认为仓库对默认分支
String committerName = "committerName_example"; // String | Committer的名字，默认为当前用户的名字
String committerEmail = "committerEmail_example"; // String | Committer的邮箱，默认为当前用户的邮箱
String authorName = "authorName_example"; // String | Author的名字，默认为当前用户的名字
String authorEmail = "authorEmail_example"; // String | Author的邮箱，默认为当前用户的邮箱
try {
    CommitContent result = apiInstance.putV5ReposOwnerRepoContentsPath(owner, repo, path, content, sha, message, accessToken, branch, committerName, committerEmail, authorName, authorEmail);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#putV5ReposOwnerRepoContentsPath");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **path** | **String**| 文件的路径 |
 **content** | **String**| 文件内容, 要用 base64 编码 |
 **sha** | **String**| 文件的 Blob SHA，可通过 [获取仓库具体路径下的内容] API 获取 |
 **message** | **String**| 提交信息 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **branch** | **String**| 分支名称。默认为仓库对默认分支 | [optional]
 **committerName** | **String**| Committer的名字，默认为当前用户的名字 | [optional]
 **committerEmail** | **String**| Committer的邮箱，默认为当前用户的邮箱 | [optional]
 **authorName** | **String**| Author的名字，默认为当前用户的名字 | [optional]
 **authorEmail** | **String**| Author的邮箱，默认为当前用户的邮箱 | [optional]

### Return type

[**CommitContent**](CommitContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="putV5ReposOwnerRepoKeysEnableId"></a>
# **putV5ReposOwnerRepoKeysEnableId**
> putV5ReposOwnerRepoKeysEnableId(owner, repo, id, accessToken)

启用仓库公钥

启用仓库公钥

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.RepositoriesApi;


RepositoriesApi apiInstance = new RepositoriesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | 公钥 ID
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.putV5ReposOwnerRepoKeysEnableId(owner, repo, id, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling RepositoriesApi#putV5ReposOwnerRepoKeysEnableId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**| 公钥 ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

