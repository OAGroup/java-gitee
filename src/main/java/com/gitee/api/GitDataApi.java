/*
 * 码云 Open API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 5.3.2
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.gitee.api;

import com.gitee.ApiCallback;
import com.gitee.ApiClient;
import com.gitee.ApiException;
import com.gitee.ApiResponse;
import com.gitee.Configuration;
import com.gitee.Pair;
import com.gitee.ProgressRequestBody;
import com.gitee.ProgressResponseBody;

import com.google.gson.reflect.TypeToken;

import java.io.IOException;


import com.gitee.model.Blob;
import com.gitee.model.Tree;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GitDataApi {
    private ApiClient apiClient;

    public GitDataApi() {
        this(Configuration.getDefaultApiClient());
    }

    public GitDataApi(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Build call for getV5ReposOwnerRepoGitBlobsSha
     * @param owner 仓库所属空间地址(企业、组织或个人的地址path) (required)
     * @param repo 仓库路径(path) (required)
     * @param sha 文件的 Blob SHA，可通过 [获取仓库具体路径下的内容] API 获取 (required)
     * @param accessToken 用户授权码 (optional)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getV5ReposOwnerRepoGitBlobsShaCall(String owner, String repo, String sha, String accessToken, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/v5/repos/{owner}/{repo}/git/blobs/{sha}"
            .replaceAll("\\{" + "owner" + "\\}", apiClient.escapeString(owner.toString()))
            .replaceAll("\\{" + "repo" + "\\}", apiClient.escapeString(repo.toString()))
            .replaceAll("\\{" + "sha" + "\\}", apiClient.escapeString(sha.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();
        if (accessToken != null)
        localVarQueryParams.addAll(apiClient.parameterToPair("access_token", accessToken));

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json", "multipart/form-data"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getV5ReposOwnerRepoGitBlobsShaValidateBeforeCall(String owner, String repo, String sha, String accessToken, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'owner' is set
        if (owner == null) {
            throw new ApiException("Missing the required parameter 'owner' when calling getV5ReposOwnerRepoGitBlobsSha(Async)");
        }
        
        // verify the required parameter 'repo' is set
        if (repo == null) {
            throw new ApiException("Missing the required parameter 'repo' when calling getV5ReposOwnerRepoGitBlobsSha(Async)");
        }
        
        // verify the required parameter 'sha' is set
        if (sha == null) {
            throw new ApiException("Missing the required parameter 'sha' when calling getV5ReposOwnerRepoGitBlobsSha(Async)");
        }
        

        com.squareup.okhttp.Call call = getV5ReposOwnerRepoGitBlobsShaCall(owner, repo, sha, accessToken, progressListener, progressRequestListener);
        return call;

    }

    /**
     * 获取文件Blob
     * 获取文件Blob
     * @param owner 仓库所属空间地址(企业、组织或个人的地址path) (required)
     * @param repo 仓库路径(path) (required)
     * @param sha 文件的 Blob SHA，可通过 [获取仓库具体路径下的内容] API 获取 (required)
     * @param accessToken 用户授权码 (optional)
     * @return Blob
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public Blob getV5ReposOwnerRepoGitBlobsSha(String owner, String repo, String sha, String accessToken) throws ApiException {
        ApiResponse<Blob> resp = getV5ReposOwnerRepoGitBlobsShaWithHttpInfo(owner, repo, sha, accessToken);
        return resp.getData();
    }

    /**
     * 获取文件Blob
     * 获取文件Blob
     * @param owner 仓库所属空间地址(企业、组织或个人的地址path) (required)
     * @param repo 仓库路径(path) (required)
     * @param sha 文件的 Blob SHA，可通过 [获取仓库具体路径下的内容] API 获取 (required)
     * @param accessToken 用户授权码 (optional)
     * @return ApiResponse&lt;Blob&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<Blob> getV5ReposOwnerRepoGitBlobsShaWithHttpInfo(String owner, String repo, String sha, String accessToken) throws ApiException {
        com.squareup.okhttp.Call call = getV5ReposOwnerRepoGitBlobsShaValidateBeforeCall(owner, repo, sha, accessToken, null, null);
        Type localVarReturnType = new TypeToken<Blob>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * 获取文件Blob (asynchronously)
     * 获取文件Blob
     * @param owner 仓库所属空间地址(企业、组织或个人的地址path) (required)
     * @param repo 仓库路径(path) (required)
     * @param sha 文件的 Blob SHA，可通过 [获取仓库具体路径下的内容] API 获取 (required)
     * @param accessToken 用户授权码 (optional)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getV5ReposOwnerRepoGitBlobsShaAsync(String owner, String repo, String sha, String accessToken, final ApiCallback<Blob> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = getV5ReposOwnerRepoGitBlobsShaValidateBeforeCall(owner, repo, sha, accessToken, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<Blob>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getV5ReposOwnerRepoGitTreesSha
     * @param owner 仓库所属空间地址(企业、组织或个人的地址path) (required)
     * @param repo 仓库路径(path) (required)
     * @param sha 可以是分支名(如master)、Commit或者目录Tree的SHA值 (required)
     * @param accessToken 用户授权码 (optional)
     * @param recursive 赋值为1递归获取目录 (optional)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getV5ReposOwnerRepoGitTreesShaCall(String owner, String repo, String sha, String accessToken, Integer recursive, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/v5/repos/{owner}/{repo}/git/trees/{sha}"
            .replaceAll("\\{" + "owner" + "\\}", apiClient.escapeString(owner.toString()))
            .replaceAll("\\{" + "repo" + "\\}", apiClient.escapeString(repo.toString()))
            .replaceAll("\\{" + "sha" + "\\}", apiClient.escapeString(sha.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();
        if (accessToken != null)
        localVarQueryParams.addAll(apiClient.parameterToPair("access_token", accessToken));
        if (recursive != null)
        localVarQueryParams.addAll(apiClient.parameterToPair("recursive", recursive));

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json", "multipart/form-data"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getV5ReposOwnerRepoGitTreesShaValidateBeforeCall(String owner, String repo, String sha, String accessToken, Integer recursive, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'owner' is set
        if (owner == null) {
            throw new ApiException("Missing the required parameter 'owner' when calling getV5ReposOwnerRepoGitTreesSha(Async)");
        }
        
        // verify the required parameter 'repo' is set
        if (repo == null) {
            throw new ApiException("Missing the required parameter 'repo' when calling getV5ReposOwnerRepoGitTreesSha(Async)");
        }
        
        // verify the required parameter 'sha' is set
        if (sha == null) {
            throw new ApiException("Missing the required parameter 'sha' when calling getV5ReposOwnerRepoGitTreesSha(Async)");
        }
        

        com.squareup.okhttp.Call call = getV5ReposOwnerRepoGitTreesShaCall(owner, repo, sha, accessToken, recursive, progressListener, progressRequestListener);
        return call;

    }

    /**
     * 获取目录Tree
     * 获取目录Tree
     * @param owner 仓库所属空间地址(企业、组织或个人的地址path) (required)
     * @param repo 仓库路径(path) (required)
     * @param sha 可以是分支名(如master)、Commit或者目录Tree的SHA值 (required)
     * @param accessToken 用户授权码 (optional)
     * @param recursive 赋值为1递归获取目录 (optional)
     * @return Tree
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public Tree getV5ReposOwnerRepoGitTreesSha(String owner, String repo, String sha, String accessToken, Integer recursive) throws ApiException {
        ApiResponse<Tree> resp = getV5ReposOwnerRepoGitTreesShaWithHttpInfo(owner, repo, sha, accessToken, recursive);
        return resp.getData();
    }

    /**
     * 获取目录Tree
     * 获取目录Tree
     * @param owner 仓库所属空间地址(企业、组织或个人的地址path) (required)
     * @param repo 仓库路径(path) (required)
     * @param sha 可以是分支名(如master)、Commit或者目录Tree的SHA值 (required)
     * @param accessToken 用户授权码 (optional)
     * @param recursive 赋值为1递归获取目录 (optional)
     * @return ApiResponse&lt;Tree&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<Tree> getV5ReposOwnerRepoGitTreesShaWithHttpInfo(String owner, String repo, String sha, String accessToken, Integer recursive) throws ApiException {
        com.squareup.okhttp.Call call = getV5ReposOwnerRepoGitTreesShaValidateBeforeCall(owner, repo, sha, accessToken, recursive, null, null);
        Type localVarReturnType = new TypeToken<Tree>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * 获取目录Tree (asynchronously)
     * 获取目录Tree
     * @param owner 仓库所属空间地址(企业、组织或个人的地址path) (required)
     * @param repo 仓库路径(path) (required)
     * @param sha 可以是分支名(如master)、Commit或者目录Tree的SHA值 (required)
     * @param accessToken 用户授权码 (optional)
     * @param recursive 赋值为1递归获取目录 (optional)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getV5ReposOwnerRepoGitTreesShaAsync(String owner, String repo, String sha, String accessToken, Integer recursive, final ApiCallback<Tree> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = getV5ReposOwnerRepoGitTreesShaValidateBeforeCall(owner, repo, sha, accessToken, recursive, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<Tree>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
}
