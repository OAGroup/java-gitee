/*
 * 码云 Open API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 5.3.2
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.gitee.model;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * update pull request information
 */
@ApiModel(description = "update pull request information")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-01-07T23:29:15.250+08:00")
public class PullRequestUpdateParam {
  @SerializedName("access_token")
  private String accessToken = null;

  @SerializedName("title")
  private String title = null;

  @SerializedName("body")
  private String body = null;

  /**
   * 可选。Pull Request 状态
   */
  @JsonAdapter(StateEnum.Adapter.class)
  public enum StateEnum {
    OPEN("open"),
    
    CLOSED("closed");

    private String value;

    StateEnum(String value) {
      this.value = value;
    }

    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    public static StateEnum fromValue(String text) {
      for (StateEnum b : StateEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    public static class Adapter extends TypeAdapter<StateEnum> {
      @Override
      public void write(final JsonWriter jsonWriter, final StateEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      @Override
      public StateEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return StateEnum.fromValue(String.valueOf(value));
      }
    }
  }

  @SerializedName("state")
  private StateEnum state = null;

  @SerializedName("milestone_number")
  private Integer milestoneNumber = null;

  @SerializedName("labels")
  private String labels = null;

  public PullRequestUpdateParam accessToken(String accessToken) {
    this.accessToken = accessToken;
    return this;
  }

   /**
   * 用户授权码
   * @return accessToken
  **/
  @ApiModelProperty(value = "用户授权码")
  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public PullRequestUpdateParam title(String title) {
    this.title = title;
    return this;
  }

   /**
   * 可选。Pull Request 标题
   * @return title
  **/
  @ApiModelProperty(value = "可选。Pull Request 标题")
  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public PullRequestUpdateParam body(String body) {
    this.body = body;
    return this;
  }

   /**
   * 可选。Pull Request 内容
   * @return body
  **/
  @ApiModelProperty(value = "可选。Pull Request 内容")
  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public PullRequestUpdateParam state(StateEnum state) {
    this.state = state;
    return this;
  }

   /**
   * 可选。Pull Request 状态
   * @return state
  **/
  @ApiModelProperty(value = "可选。Pull Request 状态")
  public StateEnum getState() {
    return state;
  }

  public void setState(StateEnum state) {
    this.state = state;
  }

  public PullRequestUpdateParam milestoneNumber(Integer milestoneNumber) {
    this.milestoneNumber = milestoneNumber;
    return this;
  }

   /**
   * 可选。里程碑序号(id)
   * @return milestoneNumber
  **/
  @ApiModelProperty(value = "可选。里程碑序号(id)")
  public Integer getMilestoneNumber() {
    return milestoneNumber;
  }

  public void setMilestoneNumber(Integer milestoneNumber) {
    this.milestoneNumber = milestoneNumber;
  }

  public PullRequestUpdateParam labels(String labels) {
    this.labels = labels;
    return this;
  }

   /**
   * 用逗号分开的标签，名称要求长度在 2-20 之间且非特殊字符。如: bug,performance
   * @return labels
  **/
  @ApiModelProperty(value = "用逗号分开的标签，名称要求长度在 2-20 之间且非特殊字符。如: bug,performance")
  public String getLabels() {
    return labels;
  }

  public void setLabels(String labels) {
    this.labels = labels;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PullRequestUpdateParam pullRequestUpdateParam = (PullRequestUpdateParam) o;
    return Objects.equals(this.accessToken, pullRequestUpdateParam.accessToken) &&
        Objects.equals(this.title, pullRequestUpdateParam.title) &&
        Objects.equals(this.body, pullRequestUpdateParam.body) &&
        Objects.equals(this.state, pullRequestUpdateParam.state) &&
        Objects.equals(this.milestoneNumber, pullRequestUpdateParam.milestoneNumber) &&
        Objects.equals(this.labels, pullRequestUpdateParam.labels);
  }

  @Override
  public int hashCode() {
    return Objects.hash(accessToken, title, body, state, milestoneNumber, labels);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PullRequestUpdateParam {\n");
    
    sb.append("    accessToken: ").append(toIndentedString(accessToken)).append("\n");
    sb.append("    title: ").append(toIndentedString(title)).append("\n");
    sb.append("    body: ").append(toIndentedString(body)).append("\n");
    sb.append("    state: ").append(toIndentedString(state)).append("\n");
    sb.append("    milestoneNumber: ").append(toIndentedString(milestoneNumber)).append("\n");
    sb.append("    labels: ").append(toIndentedString(labels)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

