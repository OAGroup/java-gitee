/*
 * 码云 Open API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 5.3.2
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.gitee.model;

import java.util.Objects;
import com.gitee.model.UserBasic;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * ProjectBasic
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-01-07T23:29:15.250+08:00")
public class ProjectBasic {
  @SerializedName("id")
  private Integer id = null;

  @SerializedName("full_name")
  private String fullName = null;

  @SerializedName("human_name")
  private String humanName = null;

  @SerializedName("url")
  private String url = null;

  @SerializedName("namespace")
  private Object namespace = null;

  @SerializedName("path")
  private String path = null;

  @SerializedName("name")
  private String name = null;

  @SerializedName("owner")
  private UserBasic owner = null;

  @SerializedName("description")
  private String description = null;

  @SerializedName("private")
  private Boolean _private = null;

  @SerializedName("public")
  private Boolean _public = null;

  @SerializedName("internal")
  private Boolean internal = null;

  @SerializedName("fork")
  private Boolean fork = null;

  @SerializedName("html_url")
  private String htmlUrl = null;

  @SerializedName("ssh_url")
  private String sshUrl = null;

  public ProjectBasic id(Integer id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public ProjectBasic fullName(String fullName) {
    this.fullName = fullName;
    return this;
  }

   /**
   * Get fullName
   * @return fullName
  **/
  @ApiModelProperty(value = "")
  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  public ProjectBasic humanName(String humanName) {
    this.humanName = humanName;
    return this;
  }

   /**
   * Get humanName
   * @return humanName
  **/
  @ApiModelProperty(value = "")
  public String getHumanName() {
    return humanName;
  }

  public void setHumanName(String humanName) {
    this.humanName = humanName;
  }

  public ProjectBasic url(String url) {
    this.url = url;
    return this;
  }

   /**
   * Get url
   * @return url
  **/
  @ApiModelProperty(value = "")
  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public ProjectBasic namespace(Object namespace) {
    this.namespace = namespace;
    return this;
  }

   /**
   * Get namespace
   * @return namespace
  **/
  @ApiModelProperty(value = "")
  public Object getNamespace() {
    return namespace;
  }

  public void setNamespace(Object namespace) {
    this.namespace = namespace;
  }

  public ProjectBasic path(String path) {
    this.path = path;
    return this;
  }

   /**
   * Get path
   * @return path
  **/
  @ApiModelProperty(value = "")
  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public ProjectBasic name(String name) {
    this.name = name;
    return this;
  }

   /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(value = "")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ProjectBasic owner(UserBasic owner) {
    this.owner = owner;
    return this;
  }

   /**
   * Get owner
   * @return owner
  **/
  @ApiModelProperty(value = "")
  public UserBasic getOwner() {
    return owner;
  }

  public void setOwner(UserBasic owner) {
    this.owner = owner;
  }

  public ProjectBasic description(String description) {
    this.description = description;
    return this;
  }

   /**
   * Get description
   * @return description
  **/
  @ApiModelProperty(value = "")
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public ProjectBasic _private(Boolean _private) {
    this._private = _private;
    return this;
  }

   /**
   * Get _private
   * @return _private
  **/
  @ApiModelProperty(value = "")
  public Boolean isPrivate() {
    return _private;
  }

  public void setPrivate(Boolean _private) {
    this._private = _private;
  }

  public ProjectBasic _public(Boolean _public) {
    this._public = _public;
    return this;
  }

   /**
   * Get _public
   * @return _public
  **/
  @ApiModelProperty(value = "")
  public Boolean isPublic() {
    return _public;
  }

  public void setPublic(Boolean _public) {
    this._public = _public;
  }

  public ProjectBasic internal(Boolean internal) {
    this.internal = internal;
    return this;
  }

   /**
   * Get internal
   * @return internal
  **/
  @ApiModelProperty(value = "")
  public Boolean isInternal() {
    return internal;
  }

  public void setInternal(Boolean internal) {
    this.internal = internal;
  }

  public ProjectBasic fork(Boolean fork) {
    this.fork = fork;
    return this;
  }

   /**
   * Get fork
   * @return fork
  **/
  @ApiModelProperty(value = "")
  public Boolean isFork() {
    return fork;
  }

  public void setFork(Boolean fork) {
    this.fork = fork;
  }

  public ProjectBasic htmlUrl(String htmlUrl) {
    this.htmlUrl = htmlUrl;
    return this;
  }

   /**
   * Get htmlUrl
   * @return htmlUrl
  **/
  @ApiModelProperty(value = "")
  public String getHtmlUrl() {
    return htmlUrl;
  }

  public void setHtmlUrl(String htmlUrl) {
    this.htmlUrl = htmlUrl;
  }

  public ProjectBasic sshUrl(String sshUrl) {
    this.sshUrl = sshUrl;
    return this;
  }

   /**
   * Get sshUrl
   * @return sshUrl
  **/
  @ApiModelProperty(value = "")
  public String getSshUrl() {
    return sshUrl;
  }

  public void setSshUrl(String sshUrl) {
    this.sshUrl = sshUrl;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ProjectBasic projectBasic = (ProjectBasic) o;
    return Objects.equals(this.id, projectBasic.id) &&
        Objects.equals(this.fullName, projectBasic.fullName) &&
        Objects.equals(this.humanName, projectBasic.humanName) &&
        Objects.equals(this.url, projectBasic.url) &&
        Objects.equals(this.namespace, projectBasic.namespace) &&
        Objects.equals(this.path, projectBasic.path) &&
        Objects.equals(this.name, projectBasic.name) &&
        Objects.equals(this.owner, projectBasic.owner) &&
        Objects.equals(this.description, projectBasic.description) &&
        Objects.equals(this._private, projectBasic._private) &&
        Objects.equals(this._public, projectBasic._public) &&
        Objects.equals(this.internal, projectBasic.internal) &&
        Objects.equals(this.fork, projectBasic.fork) &&
        Objects.equals(this.htmlUrl, projectBasic.htmlUrl) &&
        Objects.equals(this.sshUrl, projectBasic.sshUrl);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, fullName, humanName, url, namespace, path, name, owner, description, _private, _public, internal, fork, htmlUrl, sshUrl);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ProjectBasic {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    fullName: ").append(toIndentedString(fullName)).append("\n");
    sb.append("    humanName: ").append(toIndentedString(humanName)).append("\n");
    sb.append("    url: ").append(toIndentedString(url)).append("\n");
    sb.append("    namespace: ").append(toIndentedString(namespace)).append("\n");
    sb.append("    path: ").append(toIndentedString(path)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    owner: ").append(toIndentedString(owner)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    _private: ").append(toIndentedString(_private)).append("\n");
    sb.append("    _public: ").append(toIndentedString(_public)).append("\n");
    sb.append("    internal: ").append(toIndentedString(internal)).append("\n");
    sb.append("    fork: ").append(toIndentedString(fork)).append("\n");
    sb.append("    htmlUrl: ").append(toIndentedString(htmlUrl)).append("\n");
    sb.append("    sshUrl: ").append(toIndentedString(sshUrl)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

