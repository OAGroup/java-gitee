/*
 * 码云 Open API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 5.3.2
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.gitee.model;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * create project member
 */
@ApiModel(description = "create project member")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-01-07T23:29:15.250+08:00")
public class ProjectMemberPutParam {
  @SerializedName("access_token")
  private String accessToken = null;

  /**
   * 成员权限: 拉代码(pull)，推代码(push)，管理员(admin)。默认: push
   */
  @JsonAdapter(PermissionEnum.Adapter.class)
  public enum PermissionEnum {
    PULL("pull"),
    
    PUSH("push"),
    
    ADMIN("admin");

    private String value;

    PermissionEnum(String value) {
      this.value = value;
    }

    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    public static PermissionEnum fromValue(String text) {
      for (PermissionEnum b : PermissionEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }

    public static class Adapter extends TypeAdapter<PermissionEnum> {
      @Override
      public void write(final JsonWriter jsonWriter, final PermissionEnum enumeration) throws IOException {
        jsonWriter.value(enumeration.getValue());
      }

      @Override
      public PermissionEnum read(final JsonReader jsonReader) throws IOException {
        String value = jsonReader.nextString();
        return PermissionEnum.fromValue(String.valueOf(value));
      }
    }
  }

  @SerializedName("permission")
  private PermissionEnum permission = PermissionEnum.PUSH;

  public ProjectMemberPutParam accessToken(String accessToken) {
    this.accessToken = accessToken;
    return this;
  }

   /**
   * 用户授权码
   * @return accessToken
  **/
  @ApiModelProperty(value = "用户授权码")
  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public ProjectMemberPutParam permission(PermissionEnum permission) {
    this.permission = permission;
    return this;
  }

   /**
   * 成员权限: 拉代码(pull)，推代码(push)，管理员(admin)。默认: push
   * @return permission
  **/
  @ApiModelProperty(value = "成员权限: 拉代码(pull)，推代码(push)，管理员(admin)。默认: push")
  public PermissionEnum getPermission() {
    return permission;
  }

  public void setPermission(PermissionEnum permission) {
    this.permission = permission;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ProjectMemberPutParam projectMemberPutParam = (ProjectMemberPutParam) o;
    return Objects.equals(this.accessToken, projectMemberPutParam.accessToken) &&
        Objects.equals(this.permission, projectMemberPutParam.permission);
  }

  @Override
  public int hashCode() {
    return Objects.hash(accessToken, permission);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ProjectMemberPutParam {\n");
    
    sb.append("    accessToken: ").append(toIndentedString(accessToken)).append("\n");
    sb.append("    permission: ").append(toIndentedString(permission)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

