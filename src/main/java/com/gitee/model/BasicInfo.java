/*
 * 码云 Open API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 5.3.2
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.gitee.model;

import java.util.Objects;
import com.gitee.model.Project;
import com.gitee.model.UserBasic;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * basic information
 */
@ApiModel(description = "basic information")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-01-07T23:29:15.250+08:00")
public class BasicInfo {
  @SerializedName("label")
  private String label = null;

  @SerializedName("ref")
  private String ref = null;

  @SerializedName("sha")
  private String sha = null;

  @SerializedName("user")
  private UserBasic user = null;

  @SerializedName("repo")
  private Project repo = null;

  public BasicInfo label(String label) {
    this.label = label;
    return this;
  }

   /**
   * Get label
   * @return label
  **/
  @ApiModelProperty(value = "")
  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public BasicInfo ref(String ref) {
    this.ref = ref;
    return this;
  }

   /**
   * Get ref
   * @return ref
  **/
  @ApiModelProperty(value = "")
  public String getRef() {
    return ref;
  }

  public void setRef(String ref) {
    this.ref = ref;
  }

  public BasicInfo sha(String sha) {
    this.sha = sha;
    return this;
  }

   /**
   * Get sha
   * @return sha
  **/
  @ApiModelProperty(value = "")
  public String getSha() {
    return sha;
  }

  public void setSha(String sha) {
    this.sha = sha;
  }

  public BasicInfo user(UserBasic user) {
    this.user = user;
    return this;
  }

   /**
   * Get user
   * @return user
  **/
  @ApiModelProperty(value = "")
  public UserBasic getUser() {
    return user;
  }

  public void setUser(UserBasic user) {
    this.user = user;
  }

  public BasicInfo repo(Project repo) {
    this.repo = repo;
    return this;
  }

   /**
   * Get repo
   * @return repo
  **/
  @ApiModelProperty(value = "")
  public Project getRepo() {
    return repo;
  }

  public void setRepo(Project repo) {
    this.repo = repo;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BasicInfo basicInfo = (BasicInfo) o;
    return Objects.equals(this.label, basicInfo.label) &&
        Objects.equals(this.ref, basicInfo.ref) &&
        Objects.equals(this.sha, basicInfo.sha) &&
        Objects.equals(this.user, basicInfo.user) &&
        Objects.equals(this.repo, basicInfo.repo);
  }

  @Override
  public int hashCode() {
    return Objects.hash(label, ref, sha, user, repo);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class BasicInfo {\n");
    
    sb.append("    label: ").append(toIndentedString(label)).append("\n");
    sb.append("    ref: ").append(toIndentedString(ref)).append("\n");
    sb.append("    sha: ").append(toIndentedString(sha)).append("\n");
    sb.append("    user: ").append(toIndentedString(user)).append("\n");
    sb.append("    repo: ").append(toIndentedString(repo)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

