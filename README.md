# gitee-java-sdk

## Requirements

Building the API client library requires [Maven](https://maven.apache.org/) to be installed.

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn deploy
```

Refer to the [official documentation](https://maven.apache.org/plugins/maven-deploy-plugin/usage.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<repositories>
		<repository>
		    <id>jitpack.io</id>
		    <url>https://jitpack.io</url>
		</repository>
	</repositories>
<dependency>
	    <groupId>com.gitee.openkylin</groupId>
	    <artifactId>java-gitee</artifactId>
	    <version>master-SNAPSHOT</version>
	</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "com.gitee.openkylin:gitee-java-sdk:1.0.0"
```

### Others

At first generate the JAR by executing:

    mvn package

Then manually install the following JARs:

* target/gitee-java-sdk-1.0.0.jar
* target/lib/*.jar

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

import com.gitee.*;
import com.gitee.auth.*;
import com.gitee.model.*;
import com.gitee.api.ActivityApi;

import java.io.File;
import java.util.*;

public class ActivityApiExample {

    public static void main(String[] args) {
        
        ActivityApi apiInstance = new ActivityApi();
        String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
        String repo = "repo_example"; // String | 仓库路径(path)
        String accessToken = "accessToken_example"; // String | 用户授权码
        try {
            apiInstance.deleteV5UserStarredOwnerRepo(owner, repo, accessToken);
        } catch (ApiException e) {
            System.err.println("Exception when calling ActivityApi#deleteV5UserStarredOwnerRepo");
            e.printStackTrace();
        }
    }
}

```

## Documentation for API Endpoints

All URIs are relative to *https://gitee.com/api*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*ActivityApi* | [**deleteV5UserStarredOwnerRepo**](docs/ActivityApi.md#deleteV5UserStarredOwnerRepo) | **DELETE** /v5/user/starred/{owner}/{repo} | 取消 star 一个仓库
*ActivityApi* | [**deleteV5UserSubscriptionsOwnerRepo**](docs/ActivityApi.md#deleteV5UserSubscriptionsOwnerRepo) | **DELETE** /v5/user/subscriptions/{owner}/{repo} | 取消 watch 一个仓库
*ActivityApi* | [**getV5Events**](docs/ActivityApi.md#getV5Events) | **GET** /v5/events | 获取站内所有公开动态
*ActivityApi* | [**getV5NetworksOwnerRepoEvents**](docs/ActivityApi.md#getV5NetworksOwnerRepoEvents) | **GET** /v5/networks/{owner}/{repo}/events | 列出仓库的所有公开动态
*ActivityApi* | [**getV5NotificationsCount**](docs/ActivityApi.md#getV5NotificationsCount) | **GET** /v5/notifications/count | 获取授权用户的通知数
*ActivityApi* | [**getV5NotificationsMessages**](docs/ActivityApi.md#getV5NotificationsMessages) | **GET** /v5/notifications/messages | 列出授权用户的所有私信
*ActivityApi* | [**getV5NotificationsMessagesId**](docs/ActivityApi.md#getV5NotificationsMessagesId) | **GET** /v5/notifications/messages/{id} | 获取一条私信
*ActivityApi* | [**getV5NotificationsThreads**](docs/ActivityApi.md#getV5NotificationsThreads) | **GET** /v5/notifications/threads | 列出授权用户的所有通知
*ActivityApi* | [**getV5NotificationsThreadsId**](docs/ActivityApi.md#getV5NotificationsThreadsId) | **GET** /v5/notifications/threads/{id} | 获取一条通知
*ActivityApi* | [**getV5OrgsOrgEvents**](docs/ActivityApi.md#getV5OrgsOrgEvents) | **GET** /v5/orgs/{org}/events | 列出组织的公开动态
*ActivityApi* | [**getV5ReposOwnerRepoEvents**](docs/ActivityApi.md#getV5ReposOwnerRepoEvents) | **GET** /v5/repos/{owner}/{repo}/events | 列出仓库的所有动态
*ActivityApi* | [**getV5ReposOwnerRepoNotifications**](docs/ActivityApi.md#getV5ReposOwnerRepoNotifications) | **GET** /v5/repos/{owner}/{repo}/notifications | 列出一个仓库里的通知
*ActivityApi* | [**getV5ReposOwnerRepoStargazers**](docs/ActivityApi.md#getV5ReposOwnerRepoStargazers) | **GET** /v5/repos/{owner}/{repo}/stargazers | 列出 star 了仓库的用户
*ActivityApi* | [**getV5ReposOwnerRepoSubscribers**](docs/ActivityApi.md#getV5ReposOwnerRepoSubscribers) | **GET** /v5/repos/{owner}/{repo}/subscribers | 列出 watch 了仓库的用户
*ActivityApi* | [**getV5UserStarred**](docs/ActivityApi.md#getV5UserStarred) | **GET** /v5/user/starred | 列出授权用户 star 了的仓库
*ActivityApi* | [**getV5UserStarredOwnerRepo**](docs/ActivityApi.md#getV5UserStarredOwnerRepo) | **GET** /v5/user/starred/{owner}/{repo} | 检查授权用户是否 star 了一个仓库
*ActivityApi* | [**getV5UserSubscriptions**](docs/ActivityApi.md#getV5UserSubscriptions) | **GET** /v5/user/subscriptions | 列出授权用户 watch 了的仓库
*ActivityApi* | [**getV5UserSubscriptionsOwnerRepo**](docs/ActivityApi.md#getV5UserSubscriptionsOwnerRepo) | **GET** /v5/user/subscriptions/{owner}/{repo} | 检查授权用户是否 watch 了一个仓库
*ActivityApi* | [**getV5UsersUsernameEvents**](docs/ActivityApi.md#getV5UsersUsernameEvents) | **GET** /v5/users/{username}/events | 列出用户的动态
*ActivityApi* | [**getV5UsersUsernameEventsOrgsOrg**](docs/ActivityApi.md#getV5UsersUsernameEventsOrgsOrg) | **GET** /v5/users/{username}/events/orgs/{org} | 列出用户所属组织的动态
*ActivityApi* | [**getV5UsersUsernameEventsPublic**](docs/ActivityApi.md#getV5UsersUsernameEventsPublic) | **GET** /v5/users/{username}/events/public | 列出用户的公开动态
*ActivityApi* | [**getV5UsersUsernameReceivedEvents**](docs/ActivityApi.md#getV5UsersUsernameReceivedEvents) | **GET** /v5/users/{username}/received_events | 列出一个用户收到的动态
*ActivityApi* | [**getV5UsersUsernameReceivedEventsPublic**](docs/ActivityApi.md#getV5UsersUsernameReceivedEventsPublic) | **GET** /v5/users/{username}/received_events/public | 列出一个用户收到的公开动态
*ActivityApi* | [**getV5UsersUsernameStarred**](docs/ActivityApi.md#getV5UsersUsernameStarred) | **GET** /v5/users/{username}/starred | 列出用户 star 了的仓库
*ActivityApi* | [**getV5UsersUsernameSubscriptions**](docs/ActivityApi.md#getV5UsersUsernameSubscriptions) | **GET** /v5/users/{username}/subscriptions | 列出用户 watch 了的仓库
*ActivityApi* | [**patchV5NotificationsMessagesId**](docs/ActivityApi.md#patchV5NotificationsMessagesId) | **PATCH** /v5/notifications/messages/{id} | 标记一条私信为已读
*ActivityApi* | [**patchV5NotificationsThreadsId**](docs/ActivityApi.md#patchV5NotificationsThreadsId) | **PATCH** /v5/notifications/threads/{id} | 标记一条通知为已读
*ActivityApi* | [**postV5NotificationsMessages**](docs/ActivityApi.md#postV5NotificationsMessages) | **POST** /v5/notifications/messages | 发送私信给指定用户
*ActivityApi* | [**putV5NotificationsMessages**](docs/ActivityApi.md#putV5NotificationsMessages) | **PUT** /v5/notifications/messages | 标记所有私信为已读
*ActivityApi* | [**putV5NotificationsThreads**](docs/ActivityApi.md#putV5NotificationsThreads) | **PUT** /v5/notifications/threads | 标记所有通知为已读
*ActivityApi* | [**putV5ReposOwnerRepoNotifications**](docs/ActivityApi.md#putV5ReposOwnerRepoNotifications) | **PUT** /v5/repos/{owner}/{repo}/notifications | 标记一个仓库里的通知为已读
*ActivityApi* | [**putV5UserStarredOwnerRepo**](docs/ActivityApi.md#putV5UserStarredOwnerRepo) | **PUT** /v5/user/starred/{owner}/{repo} | star 一个仓库
*ActivityApi* | [**putV5UserSubscriptionsOwnerRepo**](docs/ActivityApi.md#putV5UserSubscriptionsOwnerRepo) | **PUT** /v5/user/subscriptions/{owner}/{repo} | watch 一个仓库
*EnterprisesApi* | [**deleteV5EnterprisesEnterpriseMembersUsername**](docs/EnterprisesApi.md#deleteV5EnterprisesEnterpriseMembersUsername) | **DELETE** /v5/enterprises/{enterprise}/members/{username} | 移除企业成员
*EnterprisesApi* | [**deleteV5EnterprisesEnterpriseWeekReportsReportIdCommentsId**](docs/EnterprisesApi.md#deleteV5EnterprisesEnterpriseWeekReportsReportIdCommentsId) | **DELETE** /v5/enterprises/{enterprise}/week_reports/{report_id}/comments/{id} | 删除周报某个评论
*EnterprisesApi* | [**getV5EnterprisesEnterprise**](docs/EnterprisesApi.md#getV5EnterprisesEnterprise) | **GET** /v5/enterprises/{enterprise} | 获取一个企业
*EnterprisesApi* | [**getV5EnterprisesEnterpriseMembers**](docs/EnterprisesApi.md#getV5EnterprisesEnterpriseMembers) | **GET** /v5/enterprises/{enterprise}/members | 列出企业的所有成员
*EnterprisesApi* | [**getV5EnterprisesEnterpriseMembersUsername**](docs/EnterprisesApi.md#getV5EnterprisesEnterpriseMembersUsername) | **GET** /v5/enterprises/{enterprise}/members/{username} | 获取企业的一个成员
*EnterprisesApi* | [**getV5EnterprisesEnterpriseUsersUsernameWeekReports**](docs/EnterprisesApi.md#getV5EnterprisesEnterpriseUsersUsernameWeekReports) | **GET** /v5/enterprises/{enterprise}/users/{username}/week_reports | 个人周报列表
*EnterprisesApi* | [**getV5EnterprisesEnterpriseWeekReports**](docs/EnterprisesApi.md#getV5EnterprisesEnterpriseWeekReports) | **GET** /v5/enterprises/{enterprise}/week_reports | 企业成员周报列表
*EnterprisesApi* | [**getV5EnterprisesEnterpriseWeekReportsId**](docs/EnterprisesApi.md#getV5EnterprisesEnterpriseWeekReportsId) | **GET** /v5/enterprises/{enterprise}/week_reports/{id} | 周报详情
*EnterprisesApi* | [**getV5EnterprisesEnterpriseWeekReportsIdComments**](docs/EnterprisesApi.md#getV5EnterprisesEnterpriseWeekReportsIdComments) | **GET** /v5/enterprises/{enterprise}/week_reports/{id}/comments | 某个周报评论列表
*EnterprisesApi* | [**getV5UserEnterprises**](docs/EnterprisesApi.md#getV5UserEnterprises) | **GET** /v5/user/enterprises | 列出授权用户所属的企业
*EnterprisesApi* | [**patchV5EnterprisesEnterpriseWeekReportId**](docs/EnterprisesApi.md#patchV5EnterprisesEnterpriseWeekReportId) | **PATCH** /v5/enterprises/{enterprise}/week_report/{id} | 编辑周报
*EnterprisesApi* | [**postV5EnterprisesEnterpriseMembers**](docs/EnterprisesApi.md#postV5EnterprisesEnterpriseMembers) | **POST** /v5/enterprises/{enterprise}/members | 添加或邀请企业成员
*EnterprisesApi* | [**postV5EnterprisesEnterpriseWeekReport**](docs/EnterprisesApi.md#postV5EnterprisesEnterpriseWeekReport) | **POST** /v5/enterprises/{enterprise}/week_report | 新建周报
*EnterprisesApi* | [**postV5EnterprisesEnterpriseWeekReportsIdComment**](docs/EnterprisesApi.md#postV5EnterprisesEnterpriseWeekReportsIdComment) | **POST** /v5/enterprises/{enterprise}/week_reports/{id}/comment | 评论周报
*EnterprisesApi* | [**putV5EnterprisesEnterpriseMembersUsername**](docs/EnterprisesApi.md#putV5EnterprisesEnterpriseMembersUsername) | **PUT** /v5/enterprises/{enterprise}/members/{username} | 修改企业成员权限或备注
*GistsApi* | [**deleteV5GistsGistIdCommentsId**](docs/GistsApi.md#deleteV5GistsGistIdCommentsId) | **DELETE** /v5/gists/{gist_id}/comments/{id} | 删除代码片段的评论
*GistsApi* | [**deleteV5GistsId**](docs/GistsApi.md#deleteV5GistsId) | **DELETE** /v5/gists/{id} | 删除指定代码片段
*GistsApi* | [**deleteV5GistsIdStar**](docs/GistsApi.md#deleteV5GistsIdStar) | **DELETE** /v5/gists/{id}/star | 取消Star代码片段
*GistsApi* | [**getV5Gists**](docs/GistsApi.md#getV5Gists) | **GET** /v5/gists | 获取代码片段
*GistsApi* | [**getV5GistsGistIdComments**](docs/GistsApi.md#getV5GistsGistIdComments) | **GET** /v5/gists/{gist_id}/comments | 获取代码片段的评论
*GistsApi* | [**getV5GistsGistIdCommentsId**](docs/GistsApi.md#getV5GistsGistIdCommentsId) | **GET** /v5/gists/{gist_id}/comments/{id} | 获取单条代码片段的评论
*GistsApi* | [**getV5GistsId**](docs/GistsApi.md#getV5GistsId) | **GET** /v5/gists/{id} | 获取单条代码片段
*GistsApi* | [**getV5GistsIdCommits**](docs/GistsApi.md#getV5GistsIdCommits) | **GET** /v5/gists/{id}/commits | 获取代码片段的commit
*GistsApi* | [**getV5GistsIdForks**](docs/GistsApi.md#getV5GistsIdForks) | **GET** /v5/gists/{id}/forks | 获取 Fork 了指定代码片段的列表
*GistsApi* | [**getV5GistsIdStar**](docs/GistsApi.md#getV5GistsIdStar) | **GET** /v5/gists/{id}/star | 判断代码片段是否已Star
*GistsApi* | [**getV5GistsPublic**](docs/GistsApi.md#getV5GistsPublic) | **GET** /v5/gists/public | 获取公开的代码片段
*GistsApi* | [**getV5GistsStarred**](docs/GistsApi.md#getV5GistsStarred) | **GET** /v5/gists/starred | 获取用户Star的代码片段
*GistsApi* | [**getV5UsersUsernameGists**](docs/GistsApi.md#getV5UsersUsernameGists) | **GET** /v5/users/{username}/gists | 获取指定用户的公开代码片段
*GistsApi* | [**patchV5GistsGistIdCommentsId**](docs/GistsApi.md#patchV5GistsGistIdCommentsId) | **PATCH** /v5/gists/{gist_id}/comments/{id} | 修改代码片段的评论
*GistsApi* | [**patchV5GistsId**](docs/GistsApi.md#patchV5GistsId) | **PATCH** /v5/gists/{id} | 修改代码片段
*GistsApi* | [**postV5Gists**](docs/GistsApi.md#postV5Gists) | **POST** /v5/gists | 创建代码片段
*GistsApi* | [**postV5GistsGistIdComments**](docs/GistsApi.md#postV5GistsGistIdComments) | **POST** /v5/gists/{gist_id}/comments | 增加代码片段的评论
*GistsApi* | [**postV5GistsIdForks**](docs/GistsApi.md#postV5GistsIdForks) | **POST** /v5/gists/{id}/forks | Fork代码片段
*GistsApi* | [**putV5GistsIdStar**](docs/GistsApi.md#putV5GistsIdStar) | **PUT** /v5/gists/{id}/star | Star代码片段
*GitDataApi* | [**getV5ReposOwnerRepoGitBlobsSha**](docs/GitDataApi.md#getV5ReposOwnerRepoGitBlobsSha) | **GET** /v5/repos/{owner}/{repo}/git/blobs/{sha} | 获取文件Blob
*GitDataApi* | [**getV5ReposOwnerRepoGitTreesSha**](docs/GitDataApi.md#getV5ReposOwnerRepoGitTreesSha) | **GET** /v5/repos/{owner}/{repo}/git/trees/{sha} | 获取目录Tree
*IssuesApi* | [**deleteV5ReposOwnerRepoIssuesCommentsId**](docs/IssuesApi.md#deleteV5ReposOwnerRepoIssuesCommentsId) | **DELETE** /v5/repos/{owner}/{repo}/issues/comments/{id} | 删除Issue某条评论
*IssuesApi* | [**getV5EnterprisesEnterpriseIssues**](docs/IssuesApi.md#getV5EnterprisesEnterpriseIssues) | **GET** /v5/enterprises/{enterprise}/issues | 获取某个企业的所有Issues
*IssuesApi* | [**getV5EnterprisesEnterpriseIssuesNumber**](docs/IssuesApi.md#getV5EnterprisesEnterpriseIssuesNumber) | **GET** /v5/enterprises/{enterprise}/issues/{number} | 获取企业的某个Issue
*IssuesApi* | [**getV5EnterprisesEnterpriseIssuesNumberComments**](docs/IssuesApi.md#getV5EnterprisesEnterpriseIssuesNumberComments) | **GET** /v5/enterprises/{enterprise}/issues/{number}/comments | 获取企业某个Issue所有评论
*IssuesApi* | [**getV5EnterprisesEnterpriseIssuesNumberLabels**](docs/IssuesApi.md#getV5EnterprisesEnterpriseIssuesNumberLabels) | **GET** /v5/enterprises/{enterprise}/issues/{number}/labels | 获取企业某个Issue所有标签
*IssuesApi* | [**getV5Issues**](docs/IssuesApi.md#getV5Issues) | **GET** /v5/issues | 获取当前授权用户的所有Issues
*IssuesApi* | [**getV5OrgsOrgIssues**](docs/IssuesApi.md#getV5OrgsOrgIssues) | **GET** /v5/orgs/{org}/issues | 获取当前用户某个组织的Issues
*IssuesApi* | [**getV5ReposOwnerIssuesNumberOperateLogs**](docs/IssuesApi.md#getV5ReposOwnerIssuesNumberOperateLogs) | **GET** /v5/repos/{owner}/issues/{number}/operate_logs | 获取某个Issue下的操作日志
*IssuesApi* | [**getV5ReposOwnerRepoIssues**](docs/IssuesApi.md#getV5ReposOwnerRepoIssues) | **GET** /v5/repos/{owner}/{repo}/issues | 仓库的所有Issues
*IssuesApi* | [**getV5ReposOwnerRepoIssuesComments**](docs/IssuesApi.md#getV5ReposOwnerRepoIssuesComments) | **GET** /v5/repos/{owner}/{repo}/issues/comments | 获取仓库所有Issue的评论
*IssuesApi* | [**getV5ReposOwnerRepoIssuesCommentsId**](docs/IssuesApi.md#getV5ReposOwnerRepoIssuesCommentsId) | **GET** /v5/repos/{owner}/{repo}/issues/comments/{id} | 获取仓库Issue某条评论
*IssuesApi* | [**getV5ReposOwnerRepoIssuesNumber**](docs/IssuesApi.md#getV5ReposOwnerRepoIssuesNumber) | **GET** /v5/repos/{owner}/{repo}/issues/{number} | 仓库的某个Issue
*IssuesApi* | [**getV5ReposOwnerRepoIssuesNumberComments**](docs/IssuesApi.md#getV5ReposOwnerRepoIssuesNumberComments) | **GET** /v5/repos/{owner}/{repo}/issues/{number}/comments | 获取仓库某个Issue所有的评论
*IssuesApi* | [**getV5UserIssues**](docs/IssuesApi.md#getV5UserIssues) | **GET** /v5/user/issues | 获取授权用户的所有Issues
*IssuesApi* | [**patchV5ReposOwnerIssuesNumber**](docs/IssuesApi.md#patchV5ReposOwnerIssuesNumber) | **PATCH** /v5/repos/{owner}/issues/{number} | 更新Issue
*IssuesApi* | [**patchV5ReposOwnerRepoIssuesCommentsId**](docs/IssuesApi.md#patchV5ReposOwnerRepoIssuesCommentsId) | **PATCH** /v5/repos/{owner}/{repo}/issues/comments/{id} | 更新Issue某条评论
*IssuesApi* | [**postV5ReposOwnerIssues**](docs/IssuesApi.md#postV5ReposOwnerIssues) | **POST** /v5/repos/{owner}/issues | 创建Issue
*IssuesApi* | [**postV5ReposOwnerRepoIssuesNumberComments**](docs/IssuesApi.md#postV5ReposOwnerRepoIssuesNumberComments) | **POST** /v5/repos/{owner}/{repo}/issues/{number}/comments | 创建某个Issue评论
*LabelsApi* | [**deleteV5ReposOwnerRepoIssuesNumberLabels**](docs/LabelsApi.md#deleteV5ReposOwnerRepoIssuesNumberLabels) | **DELETE** /v5/repos/{owner}/{repo}/issues/{number}/labels | 删除Issue所有标签
*LabelsApi* | [**deleteV5ReposOwnerRepoIssuesNumberLabelsName**](docs/LabelsApi.md#deleteV5ReposOwnerRepoIssuesNumberLabelsName) | **DELETE** /v5/repos/{owner}/{repo}/issues/{number}/labels/{name} | 删除Issue标签
*LabelsApi* | [**deleteV5ReposOwnerRepoLabelsName**](docs/LabelsApi.md#deleteV5ReposOwnerRepoLabelsName) | **DELETE** /v5/repos/{owner}/{repo}/labels/{name} | 删除一个仓库任务标签
*LabelsApi* | [**getV5EnterprisesEnterpriseLabels**](docs/LabelsApi.md#getV5EnterprisesEnterpriseLabels) | **GET** /v5/enterprises/{enterprise}/labels | 获取企业所有标签
*LabelsApi* | [**getV5EnterprisesEnterpriseLabelsName**](docs/LabelsApi.md#getV5EnterprisesEnterpriseLabelsName) | **GET** /v5/enterprises/{enterprise}/labels/{name} | 获取企业某个标签
*LabelsApi* | [**getV5ReposOwnerRepoIssuesNumberLabels**](docs/LabelsApi.md#getV5ReposOwnerRepoIssuesNumberLabels) | **GET** /v5/repos/{owner}/{repo}/issues/{number}/labels | 获取仓库任务的所有标签
*LabelsApi* | [**getV5ReposOwnerRepoLabels**](docs/LabelsApi.md#getV5ReposOwnerRepoLabels) | **GET** /v5/repos/{owner}/{repo}/labels | 获取仓库所有任务标签
*LabelsApi* | [**getV5ReposOwnerRepoLabelsName**](docs/LabelsApi.md#getV5ReposOwnerRepoLabelsName) | **GET** /v5/repos/{owner}/{repo}/labels/{name} | 根据标签名称获取单个标签
*LabelsApi* | [**patchV5ReposOwnerRepoLabelsOriginalName**](docs/LabelsApi.md#patchV5ReposOwnerRepoLabelsOriginalName) | **PATCH** /v5/repos/{owner}/{repo}/labels/{original_name} | 更新一个仓库任务标签
*LabelsApi* | [**postV5ReposOwnerRepoIssuesNumberLabels**](docs/LabelsApi.md#postV5ReposOwnerRepoIssuesNumberLabels) | **POST** /v5/repos/{owner}/{repo}/issues/{number}/labels | 创建Issue标签
*LabelsApi* | [**postV5ReposOwnerRepoLabels**](docs/LabelsApi.md#postV5ReposOwnerRepoLabels) | **POST** /v5/repos/{owner}/{repo}/labels | 创建仓库任务标签
*LabelsApi* | [**putV5ReposOwnerRepoIssuesNumberLabels**](docs/LabelsApi.md#putV5ReposOwnerRepoIssuesNumberLabels) | **PUT** /v5/repos/{owner}/{repo}/issues/{number}/labels | 替换Issue所有标签
*MilestonesApi* | [**deleteV5ReposOwnerRepoMilestonesNumber**](docs/MilestonesApi.md#deleteV5ReposOwnerRepoMilestonesNumber) | **DELETE** /v5/repos/{owner}/{repo}/milestones/{number} | 删除仓库单个里程碑
*MilestonesApi* | [**getV5ReposOwnerRepoMilestones**](docs/MilestonesApi.md#getV5ReposOwnerRepoMilestones) | **GET** /v5/repos/{owner}/{repo}/milestones | 获取仓库所有里程碑
*MilestonesApi* | [**getV5ReposOwnerRepoMilestonesNumber**](docs/MilestonesApi.md#getV5ReposOwnerRepoMilestonesNumber) | **GET** /v5/repos/{owner}/{repo}/milestones/{number} | 获取仓库单个里程碑
*MilestonesApi* | [**patchV5ReposOwnerRepoMilestonesNumber**](docs/MilestonesApi.md#patchV5ReposOwnerRepoMilestonesNumber) | **PATCH** /v5/repos/{owner}/{repo}/milestones/{number} | 更新仓库里程碑
*MilestonesApi* | [**postV5ReposOwnerRepoMilestones**](docs/MilestonesApi.md#postV5ReposOwnerRepoMilestones) | **POST** /v5/repos/{owner}/{repo}/milestones | 创建仓库里程碑
*MiscellaneousApi* | [**getV5Emojis**](docs/MiscellaneousApi.md#getV5Emojis) | **GET** /v5/emojis | 列出可使用的 Emoji
*MiscellaneousApi* | [**getV5GitignoreTemplates**](docs/MiscellaneousApi.md#getV5GitignoreTemplates) | **GET** /v5/gitignore/templates | 列出可使用的 .gitignore 模板
*MiscellaneousApi* | [**getV5GitignoreTemplatesName**](docs/MiscellaneousApi.md#getV5GitignoreTemplatesName) | **GET** /v5/gitignore/templates/{name} | 获取一个 .gitignore 模板
*MiscellaneousApi* | [**getV5GitignoreTemplatesNameRaw**](docs/MiscellaneousApi.md#getV5GitignoreTemplatesNameRaw) | **GET** /v5/gitignore/templates/{name}/raw | 获取一个 .gitignore 模板原始文件
*MiscellaneousApi* | [**getV5Licenses**](docs/MiscellaneousApi.md#getV5Licenses) | **GET** /v5/licenses | 列出可使用的开源许可协议
*MiscellaneousApi* | [**getV5LicensesLicense**](docs/MiscellaneousApi.md#getV5LicensesLicense) | **GET** /v5/licenses/{license} | 获取一个开源许可协议
*MiscellaneousApi* | [**getV5LicensesLicenseRaw**](docs/MiscellaneousApi.md#getV5LicensesLicenseRaw) | **GET** /v5/licenses/{license}/raw | 获取一个开源许可协议原始文件
*MiscellaneousApi* | [**getV5ReposOwnerRepoLicense**](docs/MiscellaneousApi.md#getV5ReposOwnerRepoLicense) | **GET** /v5/repos/{owner}/{repo}/license | 获取一个仓库使用的开源许可协议
*MiscellaneousApi* | [**postV5Markdown**](docs/MiscellaneousApi.md#postV5Markdown) | **POST** /v5/markdown | 渲染 Markdown 文本
*OrganizationsApi* | [**deleteV5OrgsOrgMembershipsUsername**](docs/OrganizationsApi.md#deleteV5OrgsOrgMembershipsUsername) | **DELETE** /v5/orgs/{org}/memberships/{username} | 移除授权用户所管理组织中的成员
*OrganizationsApi* | [**deleteV5UserMembershipsOrgsOrg**](docs/OrganizationsApi.md#deleteV5UserMembershipsOrgsOrg) | **DELETE** /v5/user/memberships/orgs/{org} | 退出一个组织
*OrganizationsApi* | [**getV5OrgsOrg**](docs/OrganizationsApi.md#getV5OrgsOrg) | **GET** /v5/orgs/{org} | 获取一个组织
*OrganizationsApi* | [**getV5OrgsOrgMembers**](docs/OrganizationsApi.md#getV5OrgsOrgMembers) | **GET** /v5/orgs/{org}/members | 列出一个组织的所有成员
*OrganizationsApi* | [**getV5OrgsOrgMembershipsUsername**](docs/OrganizationsApi.md#getV5OrgsOrgMembershipsUsername) | **GET** /v5/orgs/{org}/memberships/{username} | 获取授权用户所属组织的一个成员
*OrganizationsApi* | [**getV5UserMembershipsOrgs**](docs/OrganizationsApi.md#getV5UserMembershipsOrgs) | **GET** /v5/user/memberships/orgs | 列出授权用户在所属组织的成员资料
*OrganizationsApi* | [**getV5UserMembershipsOrgsOrg**](docs/OrganizationsApi.md#getV5UserMembershipsOrgsOrg) | **GET** /v5/user/memberships/orgs/{org} | 获取授权用户在一个组织的成员资料
*OrganizationsApi* | [**getV5UserOrgs**](docs/OrganizationsApi.md#getV5UserOrgs) | **GET** /v5/user/orgs | 列出授权用户所属的组织
*OrganizationsApi* | [**getV5UsersUsernameOrgs**](docs/OrganizationsApi.md#getV5UsersUsernameOrgs) | **GET** /v5/users/{username}/orgs | 列出用户所属的组织
*OrganizationsApi* | [**patchV5OrgsOrg**](docs/OrganizationsApi.md#patchV5OrgsOrg) | **PATCH** /v5/orgs/{org} | 更新授权用户所管理的组织资料
*OrganizationsApi* | [**patchV5UserMembershipsOrgsOrg**](docs/OrganizationsApi.md#patchV5UserMembershipsOrgsOrg) | **PATCH** /v5/user/memberships/orgs/{org} | 更新授权用户在一个组织的成员资料
*OrganizationsApi* | [**postV5UsersOrganization**](docs/OrganizationsApi.md#postV5UsersOrganization) | **POST** /v5/users/organization | 创建组织
*OrganizationsApi* | [**putV5OrgsOrgMembershipsUsername**](docs/OrganizationsApi.md#putV5OrgsOrgMembershipsUsername) | **PUT** /v5/orgs/{org}/memberships/{username} | 增加或更新授权用户所管理组织的成员
*PullRequestsApi* | [**deleteV5ReposOwnerRepoPullsCommentsId**](docs/PullRequestsApi.md#deleteV5ReposOwnerRepoPullsCommentsId) | **DELETE** /v5/repos/{owner}/{repo}/pulls/comments/{id} | 删除评论
*PullRequestsApi* | [**deleteV5ReposOwnerRepoPullsNumberAssignees**](docs/PullRequestsApi.md#deleteV5ReposOwnerRepoPullsNumberAssignees) | **DELETE** /v5/repos/{owner}/{repo}/pulls/{number}/assignees | 取消用户审查 Pull Request
*PullRequestsApi* | [**deleteV5ReposOwnerRepoPullsNumberTesters**](docs/PullRequestsApi.md#deleteV5ReposOwnerRepoPullsNumberTesters) | **DELETE** /v5/repos/{owner}/{repo}/pulls/{number}/testers | 取消用户测试 Pull Request
*PullRequestsApi* | [**getV5ReposOwnerRepoPulls**](docs/PullRequestsApi.md#getV5ReposOwnerRepoPulls) | **GET** /v5/repos/{owner}/{repo}/pulls | 获取Pull Request列表
*PullRequestsApi* | [**getV5ReposOwnerRepoPullsComments**](docs/PullRequestsApi.md#getV5ReposOwnerRepoPullsComments) | **GET** /v5/repos/{owner}/{repo}/pulls/comments | 获取该仓库下的所有Pull Request评论
*PullRequestsApi* | [**getV5ReposOwnerRepoPullsCommentsId**](docs/PullRequestsApi.md#getV5ReposOwnerRepoPullsCommentsId) | **GET** /v5/repos/{owner}/{repo}/pulls/comments/{id} | 获取Pull Request的某个评论
*PullRequestsApi* | [**getV5ReposOwnerRepoPullsNumber**](docs/PullRequestsApi.md#getV5ReposOwnerRepoPullsNumber) | **GET** /v5/repos/{owner}/{repo}/pulls/{number} | 获取单个Pull Request
*PullRequestsApi* | [**getV5ReposOwnerRepoPullsNumberComments**](docs/PullRequestsApi.md#getV5ReposOwnerRepoPullsNumberComments) | **GET** /v5/repos/{owner}/{repo}/pulls/{number}/comments | 获取某个Pull Request的所有评论
*PullRequestsApi* | [**getV5ReposOwnerRepoPullsNumberCommits**](docs/PullRequestsApi.md#getV5ReposOwnerRepoPullsNumberCommits) | **GET** /v5/repos/{owner}/{repo}/pulls/{number}/commits | 获取某Pull Request的所有Commit信息。最多显示250条Commit
*PullRequestsApi* | [**getV5ReposOwnerRepoPullsNumberFiles**](docs/PullRequestsApi.md#getV5ReposOwnerRepoPullsNumberFiles) | **GET** /v5/repos/{owner}/{repo}/pulls/{number}/files | Pull Request Commit文件列表。最多显示300条diff
*PullRequestsApi* | [**getV5ReposOwnerRepoPullsNumberMerge**](docs/PullRequestsApi.md#getV5ReposOwnerRepoPullsNumberMerge) | **GET** /v5/repos/{owner}/{repo}/pulls/{number}/merge | 判断Pull Request是否已经合并
*PullRequestsApi* | [**getV5ReposOwnerRepoPullsNumberOperateLogs**](docs/PullRequestsApi.md#getV5ReposOwnerRepoPullsNumberOperateLogs) | **GET** /v5/repos/{owner}/{repo}/pulls/{number}/operate_logs | 获取某个Pull Request的操作日志
*PullRequestsApi* | [**patchV5ReposOwnerRepoPullsCommentsId**](docs/PullRequestsApi.md#patchV5ReposOwnerRepoPullsCommentsId) | **PATCH** /v5/repos/{owner}/{repo}/pulls/comments/{id} | 编辑评论
*PullRequestsApi* | [**patchV5ReposOwnerRepoPullsNumber**](docs/PullRequestsApi.md#patchV5ReposOwnerRepoPullsNumber) | **PATCH** /v5/repos/{owner}/{repo}/pulls/{number} | 更新Pull Request信息
*PullRequestsApi* | [**postV5ReposOwnerRepoPulls**](docs/PullRequestsApi.md#postV5ReposOwnerRepoPulls) | **POST** /v5/repos/{owner}/{repo}/pulls | 创建Pull Request
*PullRequestsApi* | [**postV5ReposOwnerRepoPullsNumberAssignees**](docs/PullRequestsApi.md#postV5ReposOwnerRepoPullsNumberAssignees) | **POST** /v5/repos/{owner}/{repo}/pulls/{number}/assignees | 指派用户审查 Pull Request
*PullRequestsApi* | [**postV5ReposOwnerRepoPullsNumberComments**](docs/PullRequestsApi.md#postV5ReposOwnerRepoPullsNumberComments) | **POST** /v5/repos/{owner}/{repo}/pulls/{number}/comments | 提交Pull Request评论
*PullRequestsApi* | [**postV5ReposOwnerRepoPullsNumberTesters**](docs/PullRequestsApi.md#postV5ReposOwnerRepoPullsNumberTesters) | **POST** /v5/repos/{owner}/{repo}/pulls/{number}/testers | 指派用户测试 Pull Request
*PullRequestsApi* | [**putV5ReposOwnerRepoPullsNumberMerge**](docs/PullRequestsApi.md#putV5ReposOwnerRepoPullsNumberMerge) | **PUT** /v5/repos/{owner}/{repo}/pulls/{number}/merge | 合并Pull Request
*RepositoriesApi* | [**deleteV5ReposOwnerRepo**](docs/RepositoriesApi.md#deleteV5ReposOwnerRepo) | **DELETE** /v5/repos/{owner}/{repo} | 删除一个仓库
*RepositoriesApi* | [**deleteV5ReposOwnerRepoBranchesBranchProtection**](docs/RepositoriesApi.md#deleteV5ReposOwnerRepoBranchesBranchProtection) | **DELETE** /v5/repos/{owner}/{repo}/branches/{branch}/protection | 取消保护分支的设置
*RepositoriesApi* | [**deleteV5ReposOwnerRepoCollaboratorsUsername**](docs/RepositoriesApi.md#deleteV5ReposOwnerRepoCollaboratorsUsername) | **DELETE** /v5/repos/{owner}/{repo}/collaborators/{username} | 移除仓库成员
*RepositoriesApi* | [**deleteV5ReposOwnerRepoCommentsId**](docs/RepositoriesApi.md#deleteV5ReposOwnerRepoCommentsId) | **DELETE** /v5/repos/{owner}/{repo}/comments/{id} | 删除Commit评论
*RepositoriesApi* | [**deleteV5ReposOwnerRepoContentsPath**](docs/RepositoriesApi.md#deleteV5ReposOwnerRepoContentsPath) | **DELETE** /v5/repos/{owner}/{repo}/contents/{path} | 删除文件
*RepositoriesApi* | [**deleteV5ReposOwnerRepoKeysEnableId**](docs/RepositoriesApi.md#deleteV5ReposOwnerRepoKeysEnableId) | **DELETE** /v5/repos/{owner}/{repo}/keys/enable/{id} | 停用仓库公钥
*RepositoriesApi* | [**deleteV5ReposOwnerRepoKeysId**](docs/RepositoriesApi.md#deleteV5ReposOwnerRepoKeysId) | **DELETE** /v5/repos/{owner}/{repo}/keys/{id} | 删除一个仓库公钥
*RepositoriesApi* | [**deleteV5ReposOwnerRepoReleasesId**](docs/RepositoriesApi.md#deleteV5ReposOwnerRepoReleasesId) | **DELETE** /v5/repos/{owner}/{repo}/releases/{id} | 删除仓库Release
*RepositoriesApi* | [**getV5EnterprisesEnterpriseRepos**](docs/RepositoriesApi.md#getV5EnterprisesEnterpriseRepos) | **GET** /v5/enterprises/{enterprise}/repos | 获取企业的所有仓库
*RepositoriesApi* | [**getV5OrgsOrgRepos**](docs/RepositoriesApi.md#getV5OrgsOrgRepos) | **GET** /v5/orgs/{org}/repos | 获取一个组织的仓库
*RepositoriesApi* | [**getV5ReposOwnerRepo**](docs/RepositoriesApi.md#getV5ReposOwnerRepo) | **GET** /v5/repos/{owner}/{repo} | 获取用户的某个仓库
*RepositoriesApi* | [**getV5ReposOwnerRepoBranches**](docs/RepositoriesApi.md#getV5ReposOwnerRepoBranches) | **GET** /v5/repos/{owner}/{repo}/branches | 获取所有分支
*RepositoriesApi* | [**getV5ReposOwnerRepoBranchesBranch**](docs/RepositoriesApi.md#getV5ReposOwnerRepoBranchesBranch) | **GET** /v5/repos/{owner}/{repo}/branches/{branch} | 获取单个分支
*RepositoriesApi* | [**getV5ReposOwnerRepoCollaborators**](docs/RepositoriesApi.md#getV5ReposOwnerRepoCollaborators) | **GET** /v5/repos/{owner}/{repo}/collaborators | 获取仓库的所有成员
*RepositoriesApi* | [**getV5ReposOwnerRepoCollaboratorsUsername**](docs/RepositoriesApi.md#getV5ReposOwnerRepoCollaboratorsUsername) | **GET** /v5/repos/{owner}/{repo}/collaborators/{username} | 判断用户是否为仓库成员
*RepositoriesApi* | [**getV5ReposOwnerRepoCollaboratorsUsernamePermission**](docs/RepositoriesApi.md#getV5ReposOwnerRepoCollaboratorsUsernamePermission) | **GET** /v5/repos/{owner}/{repo}/collaborators/{username}/permission | 查看仓库成员的权限
*RepositoriesApi* | [**getV5ReposOwnerRepoComments**](docs/RepositoriesApi.md#getV5ReposOwnerRepoComments) | **GET** /v5/repos/{owner}/{repo}/comments | 获取仓库的Commit评论
*RepositoriesApi* | [**getV5ReposOwnerRepoCommentsId**](docs/RepositoriesApi.md#getV5ReposOwnerRepoCommentsId) | **GET** /v5/repos/{owner}/{repo}/comments/{id} | 获取仓库的某条Commit评论
*RepositoriesApi* | [**getV5ReposOwnerRepoCommits**](docs/RepositoriesApi.md#getV5ReposOwnerRepoCommits) | **GET** /v5/repos/{owner}/{repo}/commits | 仓库的所有提交
*RepositoriesApi* | [**getV5ReposOwnerRepoCommitsRefComments**](docs/RepositoriesApi.md#getV5ReposOwnerRepoCommitsRefComments) | **GET** /v5/repos/{owner}/{repo}/commits/{ref}/comments | 获取单个Commit的评论
*RepositoriesApi* | [**getV5ReposOwnerRepoCommitsSha**](docs/RepositoriesApi.md#getV5ReposOwnerRepoCommitsSha) | **GET** /v5/repos/{owner}/{repo}/commits/{sha} | 仓库的某个提交
*RepositoriesApi* | [**getV5ReposOwnerRepoCompareBaseHead**](docs/RepositoriesApi.md#getV5ReposOwnerRepoCompareBaseHead) | **GET** /v5/repos/{owner}/{repo}/compare/{base}...{head} | 两个Commits之间对比的版本差异
*RepositoriesApi* | [**getV5ReposOwnerRepoContentsPath**](docs/RepositoriesApi.md#getV5ReposOwnerRepoContentsPath) | **GET** /v5/repos/{owner}/{repo}/contents/{path} | 获取仓库具体路径下的内容
*RepositoriesApi* | [**getV5ReposOwnerRepoContributors**](docs/RepositoriesApi.md#getV5ReposOwnerRepoContributors) | **GET** /v5/repos/{owner}/{repo}/contributors | 获取仓库贡献者
*RepositoriesApi* | [**getV5ReposOwnerRepoForks**](docs/RepositoriesApi.md#getV5ReposOwnerRepoForks) | **GET** /v5/repos/{owner}/{repo}/forks | 查看仓库的Forks
*RepositoriesApi* | [**getV5ReposOwnerRepoKeys**](docs/RepositoriesApi.md#getV5ReposOwnerRepoKeys) | **GET** /v5/repos/{owner}/{repo}/keys | 获取仓库已部署的公钥
*RepositoriesApi* | [**getV5ReposOwnerRepoKeysAvailable**](docs/RepositoriesApi.md#getV5ReposOwnerRepoKeysAvailable) | **GET** /v5/repos/{owner}/{repo}/keys/available | 获取仓库可部署的公钥
*RepositoriesApi* | [**getV5ReposOwnerRepoKeysId**](docs/RepositoriesApi.md#getV5ReposOwnerRepoKeysId) | **GET** /v5/repos/{owner}/{repo}/keys/{id} | 获取仓库的单个公钥
*RepositoriesApi* | [**getV5ReposOwnerRepoPages**](docs/RepositoriesApi.md#getV5ReposOwnerRepoPages) | **GET** /v5/repos/{owner}/{repo}/pages | 获取Pages信息
*RepositoriesApi* | [**getV5ReposOwnerRepoReadme**](docs/RepositoriesApi.md#getV5ReposOwnerRepoReadme) | **GET** /v5/repos/{owner}/{repo}/readme | 获取仓库README
*RepositoriesApi* | [**getV5ReposOwnerRepoReleases**](docs/RepositoriesApi.md#getV5ReposOwnerRepoReleases) | **GET** /v5/repos/{owner}/{repo}/releases | 获取仓库的所有Releases
*RepositoriesApi* | [**getV5ReposOwnerRepoReleasesId**](docs/RepositoriesApi.md#getV5ReposOwnerRepoReleasesId) | **GET** /v5/repos/{owner}/{repo}/releases/{id} | 获取仓库的单个Releases
*RepositoriesApi* | [**getV5ReposOwnerRepoReleasesLatest**](docs/RepositoriesApi.md#getV5ReposOwnerRepoReleasesLatest) | **GET** /v5/repos/{owner}/{repo}/releases/latest | 获取仓库的最后更新的Release
*RepositoriesApi* | [**getV5ReposOwnerRepoReleasesTagsTag**](docs/RepositoriesApi.md#getV5ReposOwnerRepoReleasesTagsTag) | **GET** /v5/repos/{owner}/{repo}/releases/tags/{tag} | 根据Tag名称获取仓库的Release
*RepositoriesApi* | [**getV5ReposOwnerRepoTags**](docs/RepositoriesApi.md#getV5ReposOwnerRepoTags) | **GET** /v5/repos/{owner}/{repo}/tags | 列出仓库所有的tags
*RepositoriesApi* | [**getV5UserRepos**](docs/RepositoriesApi.md#getV5UserRepos) | **GET** /v5/user/repos | 列出授权用户的所有仓库
*RepositoriesApi* | [**getV5UsersUsernameRepos**](docs/RepositoriesApi.md#getV5UsersUsernameRepos) | **GET** /v5/users/{username}/repos | 获取某个用户的公开仓库
*RepositoriesApi* | [**patchV5ReposOwnerRepo**](docs/RepositoriesApi.md#patchV5ReposOwnerRepo) | **PATCH** /v5/repos/{owner}/{repo} | 更新仓库设置
*RepositoriesApi* | [**patchV5ReposOwnerRepoCommentsId**](docs/RepositoriesApi.md#patchV5ReposOwnerRepoCommentsId) | **PATCH** /v5/repos/{owner}/{repo}/comments/{id} | 更新Commit评论
*RepositoriesApi* | [**patchV5ReposOwnerRepoReleasesId**](docs/RepositoriesApi.md#patchV5ReposOwnerRepoReleasesId) | **PATCH** /v5/repos/{owner}/{repo}/releases/{id} | 更新仓库Release
*RepositoriesApi* | [**postV5EnterprisesEnterpriseRepos**](docs/RepositoriesApi.md#postV5EnterprisesEnterpriseRepos) | **POST** /v5/enterprises/{enterprise}/repos | 创建企业仓库
*RepositoriesApi* | [**postV5OrgsOrgRepos**](docs/RepositoriesApi.md#postV5OrgsOrgRepos) | **POST** /v5/orgs/{org}/repos | 创建组织仓库
*RepositoriesApi* | [**postV5ReposOwnerRepoBranches**](docs/RepositoriesApi.md#postV5ReposOwnerRepoBranches) | **POST** /v5/repos/{owner}/{repo}/branches | 创建分支
*RepositoriesApi* | [**postV5ReposOwnerRepoCommitsShaComments**](docs/RepositoriesApi.md#postV5ReposOwnerRepoCommitsShaComments) | **POST** /v5/repos/{owner}/{repo}/commits/{sha}/comments | 创建Commit评论
*RepositoriesApi* | [**postV5ReposOwnerRepoContentsPath**](docs/RepositoriesApi.md#postV5ReposOwnerRepoContentsPath) | **POST** /v5/repos/{owner}/{repo}/contents/{path} | 新建文件
*RepositoriesApi* | [**postV5ReposOwnerRepoForks**](docs/RepositoriesApi.md#postV5ReposOwnerRepoForks) | **POST** /v5/repos/{owner}/{repo}/forks | Fork一个仓库
*RepositoriesApi* | [**postV5ReposOwnerRepoKeys**](docs/RepositoriesApi.md#postV5ReposOwnerRepoKeys) | **POST** /v5/repos/{owner}/{repo}/keys | 为仓库添加公钥
*RepositoriesApi* | [**postV5ReposOwnerRepoPagesBuilds**](docs/RepositoriesApi.md#postV5ReposOwnerRepoPagesBuilds) | **POST** /v5/repos/{owner}/{repo}/pages/builds | 请求建立Pages
*RepositoriesApi* | [**postV5ReposOwnerRepoReleases**](docs/RepositoriesApi.md#postV5ReposOwnerRepoReleases) | **POST** /v5/repos/{owner}/{repo}/releases | 创建仓库Release
*RepositoriesApi* | [**postV5UserRepos**](docs/RepositoriesApi.md#postV5UserRepos) | **POST** /v5/user/repos | 创建一个仓库
*RepositoriesApi* | [**putV5ReposOwnerRepoBranchesBranchProtection**](docs/RepositoriesApi.md#putV5ReposOwnerRepoBranchesBranchProtection) | **PUT** /v5/repos/{owner}/{repo}/branches/{branch}/protection | 设置分支保护
*RepositoriesApi* | [**putV5ReposOwnerRepoClear**](docs/RepositoriesApi.md#putV5ReposOwnerRepoClear) | **PUT** /v5/repos/{owner}/{repo}/clear | 清空一个仓库
*RepositoriesApi* | [**putV5ReposOwnerRepoCollaboratorsUsername**](docs/RepositoriesApi.md#putV5ReposOwnerRepoCollaboratorsUsername) | **PUT** /v5/repos/{owner}/{repo}/collaborators/{username} | 添加仓库成员
*RepositoriesApi* | [**putV5ReposOwnerRepoContentsPath**](docs/RepositoriesApi.md#putV5ReposOwnerRepoContentsPath) | **PUT** /v5/repos/{owner}/{repo}/contents/{path} | 更新文件
*RepositoriesApi* | [**putV5ReposOwnerRepoKeysEnableId**](docs/RepositoriesApi.md#putV5ReposOwnerRepoKeysEnableId) | **PUT** /v5/repos/{owner}/{repo}/keys/enable/{id} | 启用仓库公钥
*SearchApi* | [**getV5SearchGists**](docs/SearchApi.md#getV5SearchGists) | **GET** /v5/search/gists | 搜索代码片段
*SearchApi* | [**getV5SearchIssues**](docs/SearchApi.md#getV5SearchIssues) | **GET** /v5/search/issues | 搜索 Issues
*SearchApi* | [**getV5SearchRepositories**](docs/SearchApi.md#getV5SearchRepositories) | **GET** /v5/search/repositories | 搜索仓库
*SearchApi* | [**getV5SearchUsers**](docs/SearchApi.md#getV5SearchUsers) | **GET** /v5/search/users | 搜索用户
*UsersApi* | [**deleteV5UserFollowingUsername**](docs/UsersApi.md#deleteV5UserFollowingUsername) | **DELETE** /v5/user/following/{username} | 取消关注一个用户
*UsersApi* | [**deleteV5UserKeysId**](docs/UsersApi.md#deleteV5UserKeysId) | **DELETE** /v5/user/keys/{id} | 删除一个公钥
*UsersApi* | [**getV5User**](docs/UsersApi.md#getV5User) | **GET** /v5/user | 获取授权用户的资料
*UsersApi* | [**getV5UserFollowers**](docs/UsersApi.md#getV5UserFollowers) | **GET** /v5/user/followers | 列出授权用户的关注者
*UsersApi* | [**getV5UserFollowing**](docs/UsersApi.md#getV5UserFollowing) | **GET** /v5/user/following | 列出授权用户正关注的用户
*UsersApi* | [**getV5UserFollowingUsername**](docs/UsersApi.md#getV5UserFollowingUsername) | **GET** /v5/user/following/{username} | 检查授权用户是否关注了一个用户
*UsersApi* | [**getV5UserKeys**](docs/UsersApi.md#getV5UserKeys) | **GET** /v5/user/keys | 列出授权用户的所有公钥
*UsersApi* | [**getV5UserKeysId**](docs/UsersApi.md#getV5UserKeysId) | **GET** /v5/user/keys/{id} | 获取一个公钥
*UsersApi* | [**getV5UserNamespace**](docs/UsersApi.md#getV5UserNamespace) | **GET** /v5/user/namespace | 获取授权用户的一个 Namespace
*UsersApi* | [**getV5UserNamespaces**](docs/UsersApi.md#getV5UserNamespaces) | **GET** /v5/user/namespaces | 列出授权用户所有的 Namespace
*UsersApi* | [**getV5UsersUsername**](docs/UsersApi.md#getV5UsersUsername) | **GET** /v5/users/{username} | 获取一个用户
*UsersApi* | [**getV5UsersUsernameFollowers**](docs/UsersApi.md#getV5UsersUsernameFollowers) | **GET** /v5/users/{username}/followers | 列出指定用户的关注者
*UsersApi* | [**getV5UsersUsernameFollowing**](docs/UsersApi.md#getV5UsersUsernameFollowing) | **GET** /v5/users/{username}/following | 列出指定用户正在关注的用户
*UsersApi* | [**getV5UsersUsernameFollowingTargetUser**](docs/UsersApi.md#getV5UsersUsernameFollowingTargetUser) | **GET** /v5/users/{username}/following/{target_user} | 检查指定用户是否关注目标用户
*UsersApi* | [**getV5UsersUsernameKeys**](docs/UsersApi.md#getV5UsersUsernameKeys) | **GET** /v5/users/{username}/keys | 列出指定用户的所有公钥
*UsersApi* | [**patchV5User**](docs/UsersApi.md#patchV5User) | **PATCH** /v5/user | 更新授权用户的资料
*UsersApi* | [**postV5UserKeys**](docs/UsersApi.md#postV5UserKeys) | **POST** /v5/user/keys | 添加一个公钥
*UsersApi* | [**putV5UserFollowingUsername**](docs/UsersApi.md#putV5UserFollowingUsername) | **PUT** /v5/user/following/{username} | 关注一个用户
*WebhooksApi* | [**deleteV5ReposOwnerRepoHooksId**](docs/WebhooksApi.md#deleteV5ReposOwnerRepoHooksId) | **DELETE** /v5/repos/{owner}/{repo}/hooks/{id} | 删除一个仓库WebHook
*WebhooksApi* | [**getV5ReposOwnerRepoHooks**](docs/WebhooksApi.md#getV5ReposOwnerRepoHooks) | **GET** /v5/repos/{owner}/{repo}/hooks | 列出仓库的WebHooks
*WebhooksApi* | [**getV5ReposOwnerRepoHooksId**](docs/WebhooksApi.md#getV5ReposOwnerRepoHooksId) | **GET** /v5/repos/{owner}/{repo}/hooks/{id} | 获取仓库单个WebHook
*WebhooksApi* | [**patchV5ReposOwnerRepoHooksId**](docs/WebhooksApi.md#patchV5ReposOwnerRepoHooksId) | **PATCH** /v5/repos/{owner}/{repo}/hooks/{id} | 更新一个仓库WebHook
*WebhooksApi* | [**postV5ReposOwnerRepoHooks**](docs/WebhooksApi.md#postV5ReposOwnerRepoHooks) | **POST** /v5/repos/{owner}/{repo}/hooks | 创建一个仓库WebHook
*WebhooksApi* | [**postV5ReposOwnerRepoHooksIdTests**](docs/WebhooksApi.md#postV5ReposOwnerRepoHooksIdTests) | **POST** /v5/repos/{owner}/{repo}/hooks/{id}/tests | 测试WebHook是否发送成功


## Documentation for Models

 - [BasicInfo](docs/BasicInfo.md)
 - [Blob](docs/Blob.md)
 - [Branch](docs/Branch.md)
 - [Code](docs/Code.md)
 - [CodeComment](docs/CodeComment.md)
 - [CodeForks](docs/CodeForks.md)
 - [CodeForksHistory](docs/CodeForksHistory.md)
 - [Commit](docs/Commit.md)
 - [CommitContent](docs/CommitContent.md)
 - [Compare](docs/Compare.md)
 - [CompleteBranch](docs/CompleteBranch.md)
 - [Content](docs/Content.md)
 - [ContentBasic](docs/ContentBasic.md)
 - [Contributor](docs/Contributor.md)
 - [EnterpriseBasic](docs/EnterpriseBasic.md)
 - [EnterpriseMember](docs/EnterpriseMember.md)
 - [Event](docs/Event.md)
 - [Group](docs/Group.md)
 - [GroupDetail](docs/GroupDetail.md)
 - [GroupMember](docs/GroupMember.md)
 - [Hook](docs/Hook.md)
 - [Issue](docs/Issue.md)
 - [IssueCommentPostParam](docs/IssueCommentPostParam.md)
 - [IssueUpdateParam](docs/IssueUpdateParam.md)
 - [Label](docs/Label.md)
 - [Milestone](docs/Milestone.md)
 - [Namespace](docs/Namespace.md)
 - [NamespaceMini](docs/NamespaceMini.md)
 - [Note](docs/Note.md)
 - [OperateLog](docs/OperateLog.md)
 - [ProgramBasic](docs/ProgramBasic.md)
 - [Project](docs/Project.md)
 - [ProjectBasic](docs/ProjectBasic.md)
 - [ProjectMember](docs/ProjectMember.md)
 - [ProjectMemberPermission](docs/ProjectMemberPermission.md)
 - [ProjectMemberPutParam](docs/ProjectMemberPutParam.md)
 - [PullRequest](docs/PullRequest.md)
 - [PullRequestCommentPostParam](docs/PullRequestCommentPostParam.md)
 - [PullRequestComments](docs/PullRequestComments.md)
 - [PullRequestCommits](docs/PullRequestCommits.md)
 - [PullRequestFiles](docs/PullRequestFiles.md)
 - [PullRequestMergePutParam](docs/PullRequestMergePutParam.md)
 - [PullRequestUpdateParam](docs/PullRequestUpdateParam.md)
 - [Release](docs/Release.md)
 - [RepoCommit](docs/RepoCommit.md)
 - [RepositoryPostParam](docs/RepositoryPostParam.md)
 - [SSHKey](docs/SSHKey.md)
 - [SSHKeyBasic](docs/SSHKeyBasic.md)
 - [Tag](docs/Tag.md)
 - [Tree](docs/Tree.md)
 - [User](docs/User.md)
 - [UserBasic](docs/UserBasic.md)
 - [UserMessage](docs/UserMessage.md)
 - [UserMessageList](docs/UserMessageList.md)
 - [UserMini](docs/UserMini.md)
 - [UserNotification](docs/UserNotification.md)
 - [UserNotificationCount](docs/UserNotificationCount.md)
 - [UserNotificationList](docs/UserNotificationList.md)
 - [UserNotificationNamespace](docs/UserNotificationNamespace.md)
 - [UserNotificationSubject](docs/UserNotificationSubject.md)
 - [WeekReport](docs/WeekReport.md)


## Documentation for Authorization

All endpoints do not require authorization.
Authentication schemes defined for the API:

## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issues.

## Author



